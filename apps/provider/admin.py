from django.contrib import admin

from models import *
# Register your models here.
admin.site.register(AccountHotelBeds)
admin.site.register(ProviderHotelBeds)
admin.site.register(AccountServivuelo)
admin.site.register(ProviderServivuelo)