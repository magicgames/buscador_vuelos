# -*- coding: utf-8 -*-

from django.conf import settings
from elasticsearch import Elasticsearch
from elasticsearch.helpers import streaming_bulk
from django.db.models import Count
from django.core.management.base import BaseCommand

from provider.hotelbeds.models import Hotel, DestinationId


def setup_elastic(client, index_name):
    client.indices.create(index=index_name,
                          body={"analysis": {
                              "filter": {
                                  "nGram_filter": {
                                      "type": "edgeNGram", "min_gram": 2, "max_gram": 20,
                                      "token_chars": ["letter", "digit", "punctuation", "symbol"]
                                  }
                              },
                              "analyzer": {
                                  "nGram_analyzer": {
                                      "type": "custom",
                                      "tokenizer": "whitespace",
                                      "filter": ["lowercase", "asciifolding", "nGram_filter"]
                                  },
                                  "whitespace_analyzer": {"type": "custom", "tokenizer": "whitespace",
                                                          "filter": ["lowercase", "asciifolding"]
                                                          }
                              }
                          }
                          },  # ignore already existing index
                          ignore=400
                          )


def create_destination_index(client, index_name):
    client.indices.put_mapping(
        index=index_name,
        doc_type='hotels-destination',
        body={
            'hotels-destination': {
                '_all': {
                    "index_analyzer": "nGram_analyzer",
                    "search_analyzer": "whitespace_analyzer"
                },

                'properties': {
                    'code': {
                        'type': 'string',
                        'include_in_all': 'false'
                    },
                    'num_hotels': {
                        'type': 'integer',
                        'include_in_all': 'false'
                    },
                    'destination': {
                        'type': 'string',
                        'include_in_all': 'true',
                    }}}
        },  # ignore already existing index
        ignore=400

    )


def bulk_create_destination():
    hotel_count = Hotel.objects.values('destination_code').annotate(Count("id")).order_by()
    no_lang = []
    hotel_a = {}
    destination_a = {}
    destinations = DestinationId.objects.filter(language__pk='CAS')
    for hotel in hotel_count:
        x = {hotel['destination_code']: hotel['id__count']}
        hotel_a.update(x)

    for destination in destinations:
        destination_dict = {}
        country_dict = {}

        try:
            num = hotel_a[destination.code]
        except:
            num = 0

        x = {
            '_id': destination.destination_code.pk,
            'code': destination.destination_code.pk,
            'num_hotels': num,
            'destination': destination.name
        }

        yield x


def load_destination(client, index):
    for ok, result in streaming_bulk(
            client,
            bulk_create_destination(),
            index=index,
            doc_type='hotels-destination',
            chunk_size=500  # keep the batch sizes small for appearances only
    ):
        action, result = result.popitem()
        doc_id = '/%s/hotel-destination/%s' % (index, result['_id'])
        # process the information from ES whether the document has been
        # successfully indexed
        if not ok:
            print('Failed to %s document %s: %r' % (action, doc_id, result))
        else:
            pass

            # instantiate es client, connects to localhost:9200 by default


class Command(BaseCommand):
    help = 'Insertar datos en el Buscador'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        es = Elasticsearch()

        index = settings.ELASTIC_SEARCH_IDEX + '-cas'
        setup_elastic(es, index)
        create_destination_index(es, index)
        load_destination(es, index)
        pass
