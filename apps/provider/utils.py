# -*- coding: utf-8 -*-
import hashlib
import unicodedata
import csv, os
import sys


def strip_accents(name, accents=('COMBINING ACUTE ACCENT', 'COMBINING GRAVE ACCENT', 'COMBINING TILDE')):


    accents = set(map(unicodedata.lookup, accents))
    chars = [c for c in unicodedata.normalize('NFD', name) if c not in accents]
    return unicodedata.normalize('NFC', ''.join(chars))


def hashfile(afile, hasher, blocksize=65536):
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.hexdigest()


def add_id(process_folder, file_name, replace=False, add_pk=True):
    file_input = os.path.join(process_folder, file_name)
    file_output = os.path.join(process_folder, 'id_' + file_name)
    csv.field_size_limit(sys.maxsize)
    with open(file_input, 'rb') as f_input, open(file_output, 'wb') as output:
        reader = csv.reader(f_input, delimiter='|', quotechar=None, quoting=csv.QUOTE_NONE)
        writer = csv.writer(output, delimiter='|', quotechar=None, quoting=csv.QUOTE_NONE)

        all = []


        for k, row in enumerate(reader):
            new_row = []
            if replace:
                for col in row:

                    col = col.replace('"', '\"')
                    col = col.replace('\\', '\\ ')
                    new_row.append(col)
            else:
                new_row = row
            if add_pk:
                all.append([str(k + 1)] + new_row)
            else:
                all.append(new_row)
        writer.writerows(all)
    return file_output