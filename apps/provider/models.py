from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator


# Create your models here.

@python_2_unicode_compatible
class ProviderHotelBeds(models.Model):
    name = models.CharField(max_length=100)
    user_test = models.CharField(max_length=100)
    pass_test = models.CharField(max_length=100)
    user_production = models.CharField(max_length=100)
    pass_production = models.CharField(max_length=100)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class AccountHotelBeds(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    provider = models.ForeignKey(ProviderHotelBeds)

    def __str__(self):
        return "%s : %s" % (self.user.email, self.provider.name)


@python_2_unicode_compatible
class ProviderServivuelo(models.Model):
    name = models.CharField(max_length=100)
    agencia_test = models.CharField(max_length=100)
    user_test = models.CharField(max_length=100)
    pass_test = models.CharField(max_length=100)
    agencia_production = models.CharField(max_length=100)
    user_production = models.CharField(max_length=100)
    pass_production = models.CharField(max_length=100)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class AccountServivuelo(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    provider = models.ForeignKey(ProviderServivuelo)

    def __str__(self):
        return "%s : %s" % (self.user.email, self.provider.name)


@python_2_unicode_compatible
class AirLines(models.Model):
    carrier = models.CharField(max_length=20, primary_key=True)
    name = models.CharField(max_length=50, null=True, blank=True)
    alias = models.CharField(max_length=100, null=True, blank=True)
    preferent_card = models.CharField(max_length=200, null=True, blank=True)
    logo = models.ImageField(upload_to="logos_airline/", null=True, blank=True)

    def __str__(self):
        return self.name

    def image_tag(self):
        try:
            url = self.logo.url
        except:
            print self.logo
            url = ''

        return u'<img src="%s" />' % url

    image_tag.short_description = 'Image'
    image_tag.allow_tags = True


# Create your models here.
@python_2_unicode_compatible
class IataCode(models.Model):
    code = models.CharField(max_length=5, primary_key=True)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class MunicipalIata(models.Model):
    iata = models.ForeignKey(IataCode)
    code = models.IntegerField(db_index=True)
    name = models.CharField(max_length=300, null=True, blank=True)

    class Meta:
        ordering = ['name']
