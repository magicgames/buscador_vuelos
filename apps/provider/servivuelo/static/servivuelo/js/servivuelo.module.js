(function () {
    'use strict';


    angular
        .module('preferentBooking.servivuelo.services', ['ngCookies', 'underScore','slugifier']);
    angular
        .module('preferentBooking.servivuelo.config', []);
    angular
        .module('preferentBooking.servivuelo.directives', []);
    angular
        .module('preferentBooking.servivuelo.controllers', ['ngSanitize','mgcrea.ngStrap', 'ui.select','preferentBooking.authentication']);
    angular
        .module('preferentBooking.servivuelo', [
            'preferentBooking.servivuelo.controllers',
            'preferentBooking.servivuelo.services',
            'preferentBooking.servivuelo.config',
            'preferentBooking.servivuelo.directives'
        ]);

})();