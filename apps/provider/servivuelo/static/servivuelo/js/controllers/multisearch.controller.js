/**
 * FlightSearchController
 * @namespace preferentBooking.search.controllers
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.servivuelo.controllers')
        .controller('FlightMultiSearchController', FlightMultiSearchController);

    FlightMultiSearchController.$inject = ['$scope', 'Authentication', 'FlightSearch', 'FlightShared', '$modal', '$filter'];

    /**
     * @namespace FlightMultiSearchController
     */
    function FlightMultiSearchController($scope, Authentication, FlightSearch, FlightShared, $modal, $filter) {
        var vm = this;
        var dateFilter = $filter('date');

        vm.search = search;
        vm.range = range;
        vm.rangeAddition = rangeAddition;
        vm.checkAdult = checkAdult;
        vm.resident = false;
        vm.oneWay = true;

        vm.flights = {Status: ''};
        vm.token=null;
        vm.flight1 = {};
        vm.flight2 = {};
        vm.flight3 = {};
        vm.flight4 = {};
        vm.flight5 = {};
        vm.flight6 = {};
        vm.dateFlight1 = null;
        vm.dateFlight2 = null;
        vm.dateFlight3 = null;
        vm.dateFlight4 = null;
        vm.dateFlight5 = null;
        vm.dateFlight6 = null;


        vm.numAdult = "1";
        vm.numChild = "0";
        vm.numInfant = "0";
        vm.numAdultResArr = {};
        vm.numChildResArr = {};
        vm.numInfantResArr = {};


        var myModal = $modal({
            title: 'Buscando vuelos',
            content: 'Hola estamos buscando los mejores vuelos',
            show: false
        });


        activate();

        /**
         * @name activate
         * @desc Actions to be performed when this controller is instantiated
         * @memberOf preferentBooking.servivuelo.controllers.FlightMultiSearchController
         */
        function activate() {
            Authentication.isLogged();
            FlightSearch.airports().then(airportsSuccessFn, airportsErrorFn);
            FlightSearch.airlines().then(airlineSuccessFn, airlineErrorFn);

        }

        function checkAdult(i) {
            var x = (parseInt(i) == parseInt(vm.numAdult));
            return x;
        }

        /**
         * @name search
         * @desc make a search
         */
        function search(form) {
            if (form.$valid) {
                myModal.$promise.then(myModal.show);

                vm.flights = {Status: ''};
                vm.token=null;
                FlightShared.setAdult(vm.numAdult, vm.resident);
                FlightShared.setChild(vm.numChild, vm.resident);
                FlightShared.setInfant(vm.numInfant, vm.resident);
                var destination = generateAllDresinations();

                var dataObj = {
                    destination: destination,
                    directFlight: vm.directFlight,
                    lowCost: vm.lowCost,
                    numAdult: parseInt(vm.numAdult),
                    numChild: parseInt(vm.numChild),
                    numInfant: parseInt(vm.numInfant),
                    residentAdult: FlightSearch.generateResident(vm.numAdult, vm.resident),
                    residentChild: FlightSearch.generateResident(vm.numChild, vm.resident),
                    residentInfant: FlightSearch.generateResident(vm.numInfant, vm.resident),
                    oneWay: vm.oneWay
                };
                FlightSearch.searchMultiFlight(dataObj).then(searchSuccessFn, searchErrorFn);

            } else {
                console.log(form.$invalid);
            }
        }


        function generateAllDresinations() {
            var destination = [];
            var dest1 = FlightSearch.generateDestionation(vm.flight1, vm.dateFlight1);
            var dest2 = FlightSearch.generateDestionation(vm.flight2, vm.dateFlight2);
            var dest3 = FlightSearch.generateDestionation(vm.flight3, vm.dateFlight3);
            var dest4 = FlightSearch.generateDestionation(vm.flight4, vm.dateFlight4);
            var dest5 = FlightSearch.generateDestionation(vm.flight5, vm.dateFlight5);
            var dest6 = FlightSearch.generateDestionation(vm.flight6, vm.dateFlight6);

            if (!(dest1 === false)) {
                destination.push(dest1)
            }
            if (!(dest2 === false)) {
                destination.push(dest2)
            }
            if (!(dest3 === false)) {
                destination.push(dest3)
            }
            if (!(dest4 === false)) {
                destination.push(dest4)
            }
            if (!(dest5 === false)) {
                destination.push(dest5)
            }
            if (!(dest6 === false)) {
                destination.push(dest6)
            }
            return destination

        }

        function range(num) {
            return FlightShared.range(num)
        }

        function rangeAddition(num, add) {
            return FlightShared.rangeAddition(num, add)
        }


        //// CALLBACKS


        function airportsSuccessFn(data, status, headers, config) {
            vm.iataCodes = data.data;
        }

        function airportsErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }

        function airlineSuccessFn(data, status, headers, config) {
            vm.airlines = data.data;
            FlightShared.setAirline(vm.airlines); //Store the airline into shared service
        }

        function airlineErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }

        function searchSuccessFn(data, status, headers, config) {
            myModal.$promise.then(myModal.hide);
            vm.flights = data.data;
            vm.token = data.data.Token;
            console.log(data);
            FlightShared.setToken(vm.token);//Store token into shared service

        }

        function searchErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }


    }
})();