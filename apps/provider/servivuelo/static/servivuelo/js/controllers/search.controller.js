/**
 * FlightSearchController
 * @namespace preferentBooking.search.controllers
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.servivuelo.controllers')
        .controller('FlightSearchController', FlightSearchController);

    FlightSearchController.$inject = ['$scope', 'Authentication', 'FlightSearch', 'FlightShared', '$modal', '$filter'];

    /**
     * @namespace FlightSearchController
     */
    function FlightSearchController($scope, Authentication, FlightSearch, FlightShared, $modal, $filter) {
        var vm = this;


        vm.search = search;
        vm.range = range;
        vm.rangeAddition = rangeAddition;
        vm.resident = false;
        vm.flights = {Status: ''};
        vm.airportFrom = {};
        vm.airportTo = {};
        vm.numAdult = "1";
        vm.numChild = "0";
        vm.numInfant = "0";
        vm.numAdultResArr = {};
        vm.numChildResArr = {};
        vm.numInfantResArr = {};


        var myModal = $modal({
            title: 'Buscando vuelos',
            content: 'Hola estamos buscando los mejores vuelos',
            show: false
        });


        activate();

        /**
         * @name activate
         * @desc Actions to be performed when this controller is instantiated
         * @memberOf preferentBooking.servivuelo.controllers.FlightSearchController
         */
        function activate() {
            Authentication.isLogged();

            FlightSearch.airports().then(airportsSuccessFn, airportsErrorFn);
            FlightSearch.airlines().then(airlineSuccessFn, airlineErrorFn);

        }


        /**
         * @name search
         * @desc make a search
         */
        function search(form) {
            if (form.$valid) {
                myModal.$promise.then(myModal.show);

                vm.flights = {Status: ''};
                FlightShared.setAdult(vm.numAdult, vm.resident);
                FlightShared.setChild(vm.numChild, vm.resident);
                FlightShared.setInfant(vm.numInfant, vm.resident);
                var destination = generateAllDresinations();

                var dataObj = {
                    destination: destination,
                    directFlight: vm.directFlight,
                    lowCost: vm.lowCost,
                    numAdult: vm.numAdult,
                    numChild: vm.numChild,
                    numInfant: vm.numInfant,
                    residentAdult: FlightSearch.generateResident(vm.numAdult, vm.resident),
                    residentChild: FlightSearch.generateResident(vm.numChild, vm.resident),
                    residentInfant: FlightSearch.generateResident(vm.numInfant, vm.resident),
                    oneWay: vm.oneWay
                };
                console.log(dataObj);
                FlightSearch.searchFlight(dataObj).then(searchSuccessFn, searchErrorFn);
            } else {
                console.log(form.$invalid);
            }


        }


        function generateAllDresinations() {
            var destination = [];

            var dest1 = FlightSearch.generateDestionation(vm.flight1, $scope.fromDate);
            if (!(dest1 === false)) {
                destination.push(dest1)
            }

            if ((destination != []) && !(vm.oneWay)) {
                var to = vm.flight1.from;
                var from = vm.flight1.to;

                var dest2 = FlightSearch.generateDestionation({from: from, to: to}, $scope.toDate);
                if (!(dest2 === false)) {
                    destination.push(dest2)
                }
            }

            return destination

        }





        function range(num) {
            return FlightShared.range(num)
        }

        function rangeAddition(num, add) {
            return FlightShared.rangeAddition(num, add)
        }


        //// CALLBACKS


        function airportsSuccessFn(data, status, headers, config) {
            vm.iataCodes = data.data;
        }

        function airportsErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }

        function airlineSuccessFn(data, status, headers, config) {
            vm.airlines = data.data;
            FlightShared.setAirline(vm.airlines); //Store the airline into shared service
        }

        function airlineErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }

        function searchSuccessFn(data, status, headers, config) {
            myModal.$promise.then(myModal.hide);
            vm.flights = data.data;
            FlightShared.setToken(vm.flights.token);//Store token into shared service

        }

        function searchErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }


    }
})();