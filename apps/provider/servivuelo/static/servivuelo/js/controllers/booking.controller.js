/**
 * FlightSearchController
 * @namespace preferentBooking.search.controllers
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.servivuelo.controllers')
        .controller('FlightBookingController', FlightBookingController);

    FlightBookingController.$inject = ['$scope', 'Authentication','FlightBooking', 'FlightShared', '$location', '$filter'];

    /**
     * @namespace FlightBookingController
     */
    function FlightBookingController($scope,Authentication,FlightBooking, FlightShared, $location, $filter) {
        var vm = this;
        var dateFilter = $filter('date');
        vm.notes = {};
        vm.range = range;
        vm.generateBooking = generateBooking;
        vm.passenger = [];

        activate();

        /**
         * @name activate
         * @desc Actions to be performed when this controller is instantiated
         * @memberOf preferentBooking.servivuelo.controllers.FlightBookingController
         */
        function activate() {
            Authentication.isLogged();

            vm.notes = FlightShared.getRates();
            if ((typeof vm.notes === 'undefined') || (!vm.notes.hasOwnProperty('Notes'))) {
                $location.path('/vuelos')
            } else {
                var notes = vm.notes.Notes;
                if (notes)
                {
                    notes = notes.replace(/(?:\r\n|\r|\n)/g, '<br />');
                } else {
                    notes='No hay observaciones extras';
                }
                vm.aside = {
                    content: notes,
                    title: "Observaciones"
                };
                vm.num_adult = FlightShared.getAdult();
                vm.num_child = FlightShared.getChild();
                vm.num_infant = FlightShared.getInfant();
                vm.destination = FlightShared.getDestination();
                vm.itineraries = FlightShared.getItineraries();
                vm.price_group = FlightShared.getPriceGroup();
                vm.token = FlightShared.getToken();
                console.log(vm.token);
                /*
                FlightBooking.getMunicipal(vm.destination).then(getMunicipalSuccessFn, getMunicipalErrorFn);
                */
            }
        }

         /**
         * @desc prepare traveler object to send to reservation
         * @name formatTravelerObjet
         * @param passen traveler
         * @param type string
         * @returns {{}}
         */

        function formatTravelerObjet(passen, type) {
            var usa = {};
             if (passen.flightToUsa) {

                usa = passen.usa
            }
            var obj = {
                birthDate: dateFilter(passen.birthdate, 'dd/MM/yyyy'),
                documentNumber: FlightShared.slugString(passen.doc_num),
                documentType: FlightShared.slugString(passen.document_type),
                travelerType: type,
                firstName: FlightShared.slugString(passen.name),
                lastName: FlightShared.slugString(passen.surname),
                phone: FlightShared.slugString(passen.phone),
                email: passen.email,
                travelerTitle: passen.type,
                frequentFlyerProgram: '',
                frequentFlyerCardNumber: '',
                flightToUsa:passen.flightToUsa,
                usa: usa

            };
            if (passen.hasOwnProperty('resident_document_type')) {
                obj.isResident = true;
                obj.residentDocumentType = passen.resident_document_type;
                obj.residentCityCode = passen.resident_city_code.code;
                obj.residentCertificateNumber = passen.resident_certificate_number;
            } else {

                obj.isResident = false;
            }
            return obj;
        }


        function generatePassengers() {

            var passengers = [];

            var passengers_obj = vm.passenger;
            if (passengers_obj.hasOwnProperty('adult')) {
                for (var passenger in passengers_obj.adult) {
                    var passen = passengers_obj.adult[passenger];
                    passengers.push(formatTravelerObjet(passen, 'Adult'));
                }
            }

            if (passengers_obj.hasOwnProperty('infant')) {
                for (var passenger in passengers_obj.infant) {
                    var passen = passengers_obj.infant[passenger];
                    passengers.push(formatTravelerObjet(passen, 'Infant'));
                }
            }

            if (passengers_obj.hasOwnProperty('child')) {
                for (var passenger in passengers_obj.infant) {
                    var passen = passengers_obj.child[passenger];
                    passengers.push(formatTravelerObjet(passen, 'Child'));
                }
            }
            return passengers;
        }


        function generateBooking() {

            var passengers = generatePassengers();

            var obj = {
                token: vm.token,
                priceGroup: vm.price_group,
                itinerariesId: vm.itineraries,
                passengers: passengers
            };
            FlightBooking.createBooking(obj).then(generateBookingSuccessFn, generateBookingErrorFn)
        }



        //// COMMON
            /**
         * @name range
         * @param  num integer
         * @dec generate a range
         */
        function range(num) {
            return _.range(num);
        }


        /// CALLBACKS


        /**
         * @name searchSuccessFn
         * @desc Update posts array on view
         */
        function getMunicipalSuccessFn(data, status, headers, config) {
            vm.municipal = data.data;

        }


        /**
         * @name searchErrorFn
         * @desc Show snackbar with error
         */
        function getMunicipalErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }

        /**
         * @name generateBookingSuccessFn
         * @desc Update posts array on view
         */
        function generateBookingSuccessFn(data, status, headers, config) {
            vm.booking = data.data;
            console.log(vm.booking);

        }


        /**
         * @name generateBookingErrorFn
         * @desc Show snackbar with error
         */
        function generateBookingErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }


    }
})();