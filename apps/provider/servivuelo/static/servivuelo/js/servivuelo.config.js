(function () {
    'use strict';

    angular
        .module('preferentBooking.servivuelo.config')
        .config(config);

    config.$inject = ['$datepickerProvider', '$asideProvider'];

    /**
     * @name config
     * @desc Enable HTML5 routing
     */
    function config($datepickerProvider, $asideProvider) {

        angular.extend($datepickerProvider.defaults, {
            dateFormat: 'dd/MM/yyyy',
            startWeek: 1
        });

        angular.extend($asideProvider.defaults, {
            animation: 'am-fadeAndSlideLeft',
            placement: 'left',
            html:true
        });
    }
})();