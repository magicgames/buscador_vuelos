/**
 * Servivuelo
 * @namespace preferentBooking.servivuelo.services
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.servivuelo.services')
        .factory('FlightShared', FlightShared);

    FlightShared.$inject = ['$http', '_', 'Slug'];

    /**
     * @namespace FlightCommon
     * @returns {Factory}
     */
    function FlightShared($http, _, Slug) {
        /**
         * Variables
         */
        var airline;
        var token;
        var price_group;

        var adultObj;
        var childObj;
        var infantObj;
        var destinationObj;
        var itinerariesObj;
        var ratesObj;
        /**
         *
         * @type {{}}
         */
        var FlightShared = {
            range: range,
            rangeAddition: rangeAddition,
            setAirline: setAirline,
            getAirLine: getAirline,
            getAirlineInfo: getAirlineInfo,
            setToken: setToken,
            getToken: getToken,
            setAdult: setAdult,
            getAdult: getAdult,
            setChild: setChild,
            getChild: getChild,
            setInfant: setInfant,
            getInfant: getInfant,
            setDestination: setDestination,
            getDestination: getDestination,
            setItineraries: setItineraries,
            getItineraries: getItineraries,
            setPriceGroup: setPriceGroup,
            getPriceGroup: getPriceGroup,
            setRates: setRates,
            getRates: getRates,
            slugString: slugString,

        };

        return FlightShared;

        ////////////////////

        function setAirline(airl) {
            airline = airl;
        }

        function getAirline() {
            return airline;
        }

        function getAirlineInfo(code) {
            var x= airline[code];
            return x;
        }


        function setToken(tk) {
            token = tk;
        }

        function getToken() {
            return token;
        }

        function setAdult(num, resident) {

            adultObj = {
                num: parseInt(num),
                resident: resident
            }
        }

        function getAdult() {
            return adultObj;
        }

        function setChild(num, resident) {

            childObj = {
                num: parseInt(num),
                resident: resident
            }
        }

        function getChild() {
            return childObj;
        }

        function setInfant(num, resident) {

            infantObj = {
                num: parseInt(num),
                resident: resident
            }
        }

        function getInfant() {
            return infantObj;
        }

        function setDestination(from, to) {

            destinationObj = {
                from: from,
                to: to
            }
        }

        function getDestination() {
            return destinationObj;
        }

        function setItineraries(iti) {
            itinerariesObj = iti
        }

        function getItineraries() {
            return itinerariesObj
        }

        function setPriceGroup(pg) {
            price_group = pg
        }

        function getPriceGroup() {
            return price_group
        }

        function setRates(rates) {
            ratesObj = rates
        }

        function getRates() {
            return ratesObj
        }

        ///// COMMON
        function slugString(input) {
            return Slug.slugify(input);
        }
        /**
         * @name range
         * @param  num integer
         * @dec generate a range
         */
        function range(num) {
            return _.range(num);
        }

        function rangeAddition(num, addition) {
            addition = typeof addition !== 'undefined' ? addition : 0;
            num = parseInt(num) + addition;
            return _.range(num);
        }


    }
})();