/**
 * Servivuelo
 * @namespace preferentBooking.servivuelo.services
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.servivuelo.services')
        .factory('FlightBooking', FlightBooking);

    FlightBooking.$inject = ['$http'];

    /**
     * @namespace FlightBooking
     * @returns {Factory}
     */
    function FlightBooking($http) {


        var FlightBooking = {

            createBooking: createBooking,
            getMunicipal: getMunicipal,
            prepareBooking:prepareBooking,
            confirmBooking:confirmBooking


        };

        return FlightBooking;

        ////////////////////


        /**
         * @name createBooking
         * @desc Create a booking process
         * @param content
         * @returns {HttpPromise}
         * @memberOf preferentBooking.servivuelo.service.FlightBooking
         */
        function createBooking(content) {

            return $http.post("/api/v1/vuelos/booking/create", content);
        }


        /**
         * @desc get codes from resident discount places
         * @name getMunicipal
         * @param content object
         * @returns {HttpPromise}
         */
        function getMunicipal(content) {

            var result = $http.post("/api/v1/vuelos/resident", content);
            return result;


        }

        /**
         * @desc: Prepare booking for tickenting
         * @name prepareBooking
         * @param content
         * @returns {HttpPromise}
         */
        function prepareBooking(content) {
            var result = $http.post("/api/v1/vuelos/booking/prepare", content);
            return result;


        }

        /**
         * @desc: create ticket
         * @name confirmBooking
         * @param content
         * @returns {HttpPromise}
         */
        function confirmBooking(content) {
            var result = $http.post("/api/v1/vuelos/booking/confirm", content);
            return result;


        }


    }
})();