/**
 * Servivuelo
 * @namespace preferentBooking.servivuelo.services
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.servivuelo.services')
        .factory('FlightSearch', FlightSearch);

    FlightSearch.$inject = ['$http','$filter'];

    /**
     * @namespace FlightSearch
     * @returns {Factory}
     */
    function FlightSearch($http,$filter) {
        var dateFilter = $filter('date');
        return {
            airports: airports,
            airlines: airlines,
            searchFlight: searchFlight,
            searchMultiFlight:searchMultiFlight,
            generateRates: generateRates,
            generateResident:generateResident,
            generateDestionation:generateDestionation

        };

        ////////////////////

        /**
         * @name airports
         * @desc Get all Airports
         * @returns {Promise}
         * @memberOf preferentBooking.servivuelo.services.FlightSearch
         */
        function airports() {
            return $http.get("/api/v1/vuelos/iata");
        }
        /**
         * @name airlines
         * @desc Get all Airlines
         * @returns {Promose}
         * @memberOf preferentBooking.servivuelo.service.searchMultiFlight
         */
        function searchMultiFlight(content) {
            return $http.post("/api/v1/vuelos/multisearch", content);
        }

        /**
         * @name airlines
         * @desc Get all Airlines
         * @returns {Promose}
         * @memberOf preferentBooking.servivuelo.service.FlightSearch
         */
        function airlines() {
            var result = $http.get("/api/v1/vuelos/airlines");
            return result
        }

        /**
         * @name searchFlight
         * @desc Create a new Post
         * @param {string} content The content of the new Post
         * @returns {Promise}
         * @memberOf preferentBooking.servivuelo.service.FlightSearch
         */
        function searchFlight(content) {
            return $http.post("/api/v1/vuelos/search", content);
        }



        /**
         * @name getRates
         * @desc Check rates and prepare to create element
         * @param {string} content The content of the new Post
         * @returns {Promise}
         * @memberOf preferentBooking.servivuelo.service.FlightSearch
         */
        function generateRates(content) {
            var result = $http.post("/api/v1/vuelos/rates", content);
            return result;

        }

        function generateResident(obj, resident) {

            var result = [];
            if (resident === 'undefined') {
                resident = false;
            }
            for (var i = 0; i < obj; i++) {
                result[i] = resident;
            }
            return result;

        }

        function generateDestionation(flight, dateFlight) {


            if (flight.hasOwnProperty('from') &&
                flight.hasOwnProperty('to') &&
                flight.from.hasOwnProperty('selected') &&
                flight.to.hasOwnProperty('selected')
            ) {


                if (dateFlight instanceof Date) {
                    return {

                        from: flight.from.selected.code,
                        to: flight.to.selected.code,
                        date: dateFilter(dateFlight, 'yyyy-MM-dd')
                    }
                }

            }

            return false;
        }



    }
})();