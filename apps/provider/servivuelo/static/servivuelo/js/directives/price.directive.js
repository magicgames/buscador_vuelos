(function () {
    'use strict';
    /* @ngInject */
    function infovuelo() {

        var directive = {
            restrict: 'EA',
            scope: {
                user: '=',
                nombre: '='

            },
            templateUrl: '/static/servivuelo/flight_preciodirective.html',
            link: link
        };
        return directive;

        function link(scope, element, attributes) {
            scope.$on(
                "$destroy",
                function handleDestroyEvent() {
                    console.log('Eliminado directiva');
                }
            );
        }
    }

    angular
        .module('preferentBooking.servivuelo.directives')
        .directive('preciovuelo', infovuelo);
})();