(function () {
    'use strict';
    /* @ngInject */
    function resultadovuelo() {

        var directive = {
            restrict: 'EA',
            scope: {
                flight: '=',
                token: '='

            },
            templateUrl: '/static/servivuelo/flight_list.html',
            link: link,
            controller: FlightListController,
            controllerAs: 'vm',
            bindToController: true
        };
        return directive;

        function link(scope, element, attributes) {
            scope.$on(
                "$destroy",
                function handleDestroyEvent() {
                    console.log('Eliminado directiva');
                }
            );
        }
    }

    FlightListController.$inject = ['FlightSearch', 'FlightShared', '$location'];

    function FlightListController(FlightSearch, FlightShared, $location) {
        // Injecting $scope just for comparison
        var vm = this;
        vm.segment = {};
        vm.allAirlines = {};
        vm.maxPrice = 0;
        vm.minPrice = 0;
        vm.evaluatePrice = 0;
        vm.airlineChecked = {};
        vm.airlinesFilter = {};


        vm.checkAirline = checkAirline;
        vm.inprice = inprice;
        vm.inAirLine = inAirLine;
        vm.checkMaxPrice = checkMaxPrice;
        vm.getAirlineName = getAirlineName;
        vm.getAirlineImage = getAirlineImage;
        vm.generateReservation = generateReservation;
        vm.setSegment = setSegment;


        activate();

        /**
         * @name activate
         * @desc Actions to be performed when this controller is instantiated
         * @memberOf preferentBooking.servivuelo.controllers.FlightListController
         */
        function activate() {
            //Obtenemos los destinos
            vm.num_adult = FlightShared.getAdult();
            vm.num_child = FlightShared.getChild();
            vm.num_infant = FlightShared.getInfant();

        }


        function checkAirline(pricegroup) {

            var show = false;

            if (vm.airlinesFilter.hasOwnProperty(pricegroup)) {

                angular.forEach(vm.allAirlines, function (value, key) {
                    if (vm.airlinesFilter[pricegroup].hasOwnProperty(key) &&
                        vm.airlineChecked.hasOwnProperty(key) &&
                        vm.airlineChecked[key] === true) {
                        show = true;
                        return show;
                    }
                });
            }
            return show;
        }

        function inprice(price) {
            var y = Math.ceil(price);
            var x = ((vm.minPrice <= y) && (  y <= vm.evaluatePrice));
            return x
        }

        function inAirLine() {


        }

        function checkMaxPrice(flight) {

            var price = flight.price;
            if (Math.ceil(parseFloat(price)) > vm.maxPrice) {
                vm.maxPrice = Math.ceil(parseFloat(price));
                vm.evaluatePrice = vm.maxPrice
            }

        }

        /**
         * @name getAirlineName
         * @param airlineCode string
         * @returns string
         * @desc return airline name from airline code
         */
        function getAirlineName(airlineCode, priceGroup) {


                var result = FlightShared.getAirlineInfo(airlineCode);
                var code = '';

                try {
                    code = result.Code;
                } catch (e) {
                    console.log(e);
                }
            vm.allAirlines[airlineCode] = code;

                if (!(vm.airlinesFilter.hasOwnProperty(priceGroup))) {
                    vm.airlinesFilter[priceGroup] = {};
                }
                vm.airlinesFilter[priceGroup][airlineCode] = true;

                return code


        }

        /**
         * @name getAirlineLogo
         * @param code string
         * @returns string
         * @desc return airline name from airline code
         */
        function getAirlineImage(code) {
            var result = FlightShared.getAirlineInfo(code);
            var image = '';
            try {
                image = result.Image;
            } catch (e) {

            }


            return image;
        }

        /**
         * @name setSegment
         * @param pricegroup int
         * @param itinerary int
         * @param selection obj
         * @desc Generate airline segments
         */
        function setSegment(pricegroup, itinerary, selection) {
            var seg = itinerary.toString();
            var seg1 = parseInt(seg.charAt(0));


            var obj = {
                'it': itinerary,
                'selection': selection

            };
            if (vm.segment.hasOwnProperty(pricegroup)) {
                var row = vm.segment[pricegroup];

                if (row.hasOwnProperty(seg1)) {
                    vm.segment[pricegroup][seg1] = obj;
                } else {

                    vm.segment[pricegroup][seg1] = [];
                    vm.segment[pricegroup][seg1] = obj;

                }

            } else {

                vm.segment[pricegroup] = [];
                vm.segment[pricegroup][seg1] = [];
                vm.segment[pricegroup][seg1] = obj;

            }
            console.log(vm.segment);
        }


        /**
         * @name generateReservation
         * @param price_group int
         * @param token int
         * @desc Generate a reservation Process
         */
        function generateReservation(price_group, token) {
            var res = vm.segment[price_group];

            var it = [];
            for (var iti in res) {
                if (res.hasOwnProperty(iti)) {
                    var ele = res[iti];
                    it.push(ele['it']);
                }
            }
            FlightShared.setItineraries(it);
            FlightShared.setPriceGroup(price_group);
            var obj = {
                itinerariesId: it,
                priceGroup: price_group,
                token: token,
                numAdult: vm.num_adult.num,
                numChild: vm.num_child.num,
                numInfant: vm.num_infant.num
            };
            console.log(obj);
            FlightSearch.generateRates(obj).then(generateRatesSuccessFn, generateRatesErrorFn);

        }

        /// CALLBACKS
        function generateRatesSuccessFn(data, status, headers, config) {
            vm.rates = data.data;
            FlightShared.setRates(vm.rates);
            $location.path('/vuelos/reservar');


        }

        function generateRatesErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }


    }


    angular
        .module('preferentBooking.servivuelo.directives')
        .directive('resultadovuelo', resultadovuelo);


})();