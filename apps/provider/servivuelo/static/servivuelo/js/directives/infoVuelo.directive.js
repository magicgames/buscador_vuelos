(function () {
    'use strict';
    /* @ngInject */
    function infovuelo() {

        var directive = {
            restrict: 'EA',
            scope: {
                vuelo: '=',
                nombre: '='

            },
            templateUrl: '/static/servivuelo/flight_infovuelo.html',
            link: link
        };
        return directive;

        function link(scope, element, attributes) {
            scope.$on(
                "$destroy",
                function handleDestroyEvent() {
                    console.log('Eliminado directiva');
                }
            );
        }
    }

    angular
        .module('preferentBooking.servivuelo.directives')
        .directive('infovuelo', infovuelo);
})();