(function () {
    'use strict';
    /* @ngInject */
    function confirmarvuelo() {

        var confirmarvuelo = {
            restrict: 'A',
            scope: {

                token: '=',

            },
            templateUrl: '/static/servivuelo/flight_confirm.html',
            link: link,
            controller: FlightConfirmController,
            controllerAs: 'vm',
            bindToController: true
        };
        return confirmarvuelo;

        function link( scope, element, attributes ) {
            scope.$on(
                "$destroy",
                function handleDestroyEvent() {
                    console.log('Eliminado directiva');
                }
            );
        }
    }

    FlightConfirmController.$inject = ['FlightBooking', 'FlightShared', '$location'];

    function FlightConfirmController(FlightBooking, FlightShared, $location) {
        // Injecting $scope just for comparison
        var vm = this;
        vm.difference = 0;
        vm.TotalAmount=0;
        vm.confirmation = false; //Only true if there are no amount difference
        vm.confirmReservation = confirmReservation;
        vm.payReservation = payReservation;
        activate();

        /**
         * @name activate
         * @desc Actions to be performed when this controller is instantiated
         * @memberOf preferentBooking.servivuelo.controllers.FlightListController
         */
        function activate() {
            //Obtenemos los destinos
        }

        function confirmReservation() {

            var obj = {
                token: vm.token,
                difference: vm.difference
            };
            console.log(obj);
            FlightBooking.prepareBooking(obj).then(confirmReservationSuccessFn, confirmReservationErrorFn);

        }

        function payReservation() {

            var obj = {
                token: vm.token
            };
            console.log(obj);
            FlightBooking.confirmBooking(obj).then(payReservationReservationSuccessFn, payReservationReservationErrorFn);

        }

        /// CALLBACKS
        function payReservationReservationSuccessFn(data, status, headers, config) {
            vm.result = data.data;

            console.log(vm.result);

        }

        function payReservationReservationErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }

        function confirmReservationSuccessFn(data, status, headers, config) {
            vm.info = data.data;

            if (vm.info.Status == 'WARNING') {

                vm.difference = parseFloat(vm.info.PrepareBooking.AmountDifference);
                vm.fullAmount = parseFloat(vm.info.PrepareBooking.FullAmount);
                vm.TotalAmount= vm.difference + vm.fullAmount
            } else if (vm.info.Status == 'OK') {
                vm.confirmation = true;
            }
            console.log(vm.info);

        }

        function confirmReservationErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }


    }


    angular
        .module('preferentBooking.servivuelo.directives')
        .directive('confirmarvuelo', confirmarvuelo);


})();