# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from .views import *

urlpatterns = patterns('',
                       url(regex=r'^search$', view=SearchFlightApiView.as_view(), name='buscar_vuelo'),
                       url(regex=r'^multisearch', view=SearchMultiFlightApiView.as_view(), name='buscar_vuelo'),

                       url(regex=r'^rates$', view=CheckRatesApiView.as_view(), name='notas_vuelo'),
                       url(regex=r'^booking/create$', view=BookingCreateApiView.as_view(), name='booking_create'),
                       url(regex=r'^booking/remove$', view=BookingRemoveApiView.as_view(), name='booking_remove'),
                       url(regex=r'^booking/prepare$', view=BookingPrepareApiView.as_view(), name='booking_prepare'),
                       url(regex=r'^booking/edit$', view=BookingEditApiView.as_view(), name='booking_edit'),
                       url(regex=r'^iataservivuelo$', view=ServivueloAirportApiView.as_view(), name='aeropuertos_servivuelo'),
                       url(regex=r'^iata$', view=SelectorAeropuertosApiView.as_view(), name='aeropuertos'),
                       url(regex=r'^airlines$', view=SelectorAerolineasApiView.as_view(), name='areolineas'),
                       url(regex=r'^resident$', view=SelectorResidentApiView.as_view(), name='municipal'),
                       )
