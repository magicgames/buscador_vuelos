# -*- coding: utf-8 -*-
import datetime
from django.conf import settings
from provider.models import IataCode, AccountServivuelo
from provider.utils import strip_accents
from django.utils.translation import ugettext as _


def generate_rates_request(d):
    reason = ''
    request = {}
    num_adult = 0
    price_group = ''
    itineraries_id = {}
    try:
        num_adult = int(d['numAdult'])
        if num_adult == 0:
            raise
    except Exception as e:
        reason = _(u'No hay ningún Adulto')
    try:
        num_child = int(d['numChild'])
    except Exception as e:
        num_child = 0
    try:
        num_infant = int(d['numInfant'])
        if num_infant > num_adult:
            reason = _(u'El número de bebes tiene que ser como máximo el num de adultos')
    except Exception as e:
        num_infant = 0
    try:
        price_group = int(d['priceGroup'])

    except Exception as e:
        reason = _(u'Falta el grupo de precios')
    try:
        itineraries_id = d['itinerariesId']
        if not isinstance(itineraries_id, list):
            raise

    except Exception as e:
        reason = _(u'Faltan los itinerarios O no es valido')

    if reason == '':
        request = {
            'token': d['token'],
            'priceGroup': price_group,
            'itinerariesId': itineraries_id,
            'numAdult': num_adult,
            'numChild': num_child,
            'numInfant': num_infant,
            'email':'dios@cielo.es'
        }
    return {'request': request, 'reason': reason}


def generate_booking_create_request(d):
    reason = ''
    token = ''
    request = {}
    price_group = ''
    itineraries_id = []
    travelers = []
    try:
        token = int(d['token'])
    except Exception as e:
        reason = _(u'Token incorrecto')

        pass

    try:
        price_group = int(d['priceGroup'])
    except Exception as e:
        reason = _(u'Grupo de tarifa incorrecto')
        pass

    try:
        iti_def=[]
        itineraries_id = d['itinerariesId']
        print itineraries_id
        for iti in itineraries_id:
            try:
                if iti is not None:
                    int(iti)
                    iti_def.append(iti)
            except:
                reason = _(u'Error en el vuelo seleccionado')

    except Exception as e:
        reason = _(u'No se han seleccionado vuelos')
        pass

    try:
        travelers = d['passengers']

        if len(travelers) == 0:
            raise
        else:
            i = 1
            for passenger in travelers:
                if not passenger.viewkeys() >= {'firstName', 'lastName', 'documentType', 'documentNumber', 'phone',
                                                'travelerType', 'birthDate'}:
                    reason = _(u'Falta algun campo obligatorio')

                if passenger.get('flightToUsa', False):
                    passenger['usa']['expire_date'] = datetime.datetime.strptime(passenger['usa']['expire_date'],
                                                                                 '%Y-%m-%d').strftime(
                        '%d/%m/%Y'),

                passenger['travelerId'] = i

                if type(passenger['firstName']) is unicode:
                    passenger['firstName'] = strip_accents(passenger['firstName'])
                if type(passenger['lastName']) is unicode:
                    passenger['lastName'] = strip_accents(passenger['lastName'])

                passenger['birthDate'] = datetime.datetime.strptime(passenger['birthDate'], '%Y-%m-%d').strftime(
                    '%d/%m/%Y'),
                i += 1


    except BaseException as e:
        print e.message
        reason = _(u'No hay pasajeros')
        pass
    if reason == '':
        request = {
            'token': token,
            'priceGroup': price_group,
            'itinerariesId': iti_def,
            'travelers': travelers
        }

    return {
        'request': request,
        'reason': reason

    }


def generate_booking_remove_request(d):
    reason = ''
    token = ''
    request = {}
    price_group = ''
    itineraries_id = []
    travelers = []

    try:
        token = int(d['token'])
    except Exception as e:
        reason = _(u'Token incorrecto')
        pass

    if reason == '':
        request = {
            'token': d['token'],

        }

    return {
        'request': request,
        'reason': reason

    }


def generate_aiport_list_request(d):
    return {
        'request': {},
        'reason': ''

    }


def generate_booking_edit_request(d):
    return generate_booking_remove_request(d)


def generate_default_value(request, user):
    if user.is_authenticated():

        try:
            provider = AccountServivuelo.objects.get(user=user)

            if settings.SERVIVUELO_TEST_MODE:

                agencia = provider.provider.agencia_test
                username = provider.provider.user_test
                password = provider.provider.pass_test
            else:
                username = provider.provider.user_production
                password = provider.provider.pass_production
                agencia = provider.provider.agencia_production
            request['request']['username'] = username
            request['request']['password'] = password
            request['request']['id_agencia'] = agencia

        except:
            pass

    return request


def get_iata_choices():
    iata = IataCode.objects.filter()
    return iata


def generate_traveler(type, num_people, resident, people):
    if int(num_people) > 0:
        for x in range(0, int(num_people)):
            try:
                res = False
                if resident[x]:
                    res = True

            except BaseException as e:
                pass
            person = {'type': type, 'is_resident': res}
            people.append(person)
    pass


def get_resident(elemements):
    num_res = 0
    try:
        for x in elemements:
            if elemements[x]:
                num_res += 1
    except BaseException as e:
        print "veo si es residente"
        print e

    return num_res


def generate_search_flight_request(d):
    people = []
    destinations = []
    resident_adult = d['residentAdult']
    resident_child = d['residentChild']
    resident_infant = d['residentInfant']
    result2 = {}

    reason = ''
    try:
        num_adult = int(d['numAdult'])
        if num_adult == 0:
            raise
        generate_traveler('Adult', num_adult, resident_adult, people)
    except Exception as e:
        reason = _(u'No hay ningún Adulto')
        pass

    try:
        num_child = int(d['numChild'])
        generate_traveler('Child', num_child, resident_child, people)
    except Exception as e:
        num_child = 0
        pass

    try:
        num_infant = int(d['numInfant'])
        if num_infant > num_adult:
            reason = _(u'El número de bebes tiene que ser como máximo el num de adultos')

        generate_traveler('Infant', num_infant, resident_infant, people)
    except Exception as e:
        num_infant = 0
        pass

    try:
        destination_obj = d['destination']

    except Exception as e:
        reason = _(u'No has seleccionado ningún destino')

    try:
        if reason != '':
            raise

        for dest in destination_obj:
            x = {
                'departure': dest['from'],
                'arrive': dest['to'],
                'date': datetime.datetime.strptime(dest['date'], '%Y-%m-%d').strftime('%d/%m/%Y'),
                'time': None

            }
            destinations.append(x)

        request = {
            'people': people,
            'destinations': destinations,
            'lowcost': 'true',
            'direct': 'false',
            'numAdult': num_adult,
            'numChild': num_child,
            'numInfant': num_infant,
            'numAdultResident': get_resident(resident_adult),
            'numChildResident': get_resident(resident_child),
            'numInfantResident': get_resident(resident_infant),
        }
    except BaseException as e:
        print e
        print "busqueda"
        reason = e.message
        request = None

    return {
        'request': request,
        'reason': reason
    }
