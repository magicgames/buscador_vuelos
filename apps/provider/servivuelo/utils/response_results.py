# -*- coding: utf-8 -*-


def response_search(result, num_adult=0, num_child=0, num_infant=0, num_adult_resident=0, num_child_resident=0,
                    num_infant_resident=0):
    """
    Procesa el resultado para poder obtener los vuelos
    :param result:
    :return:
    """
    itinerarios = {}
    error = []

    if 'Error' in result:
        if result.Error.Code is None:
            for warning in result.AirAvail.Warnings.Warning:
                x = {
                    'Code': warning['WarningCode'],
                    'Description': warning['Description']

                }
                error.append(x)
        else:
            x = {
                'Code': result.Error.Code,
                'Description': result.Error.Description

            }
            error.append(x)
        return {'Status': 'KO', 'Error': error}

    if result.AirAvail.AirItineraries is None:
        x = {
            'Code': 'NO FLIGHT',
            'Description': "No hay vuelos para esas fechas"

        }
        error.append(x)
        return {'Status': 'KO', 'Error': error}

    for itinerary in result.AirAvail.AirItineraries.AirItinerary:
        scale = None
        if len(itinerary.AirItineraryLegs.AirItineraryLeg) > 1:
            scale = []
            for scale_obj in itinerary.AirItineraryLegs.AirItineraryLeg:
                x = {
                    'Departure': scale_obj['DepartureAirportLocationCode'],
                    'DepartureTime': scale_obj['DepartureDateTime'],
                    'Arrive': scale_obj['ArrivalAirportLocationCode'],
                    'ArriveTime': scale_obj['ArrivalDateTime'],
                    'FlightNumber': scale_obj['FlightNumber'],
                    'OperationCarrier': scale_obj['OperatingCarrierCode'],
                    'MarketingCarrier': scale_obj['MarketingCarrierCode'],


                }
                scale.append(x)

        x = {

            itinerary['ItineraryID']: {
                'Id': itinerary['ItineraryID'],
                'Departure': itinerary['DepartureAirportLocationCode'],
                'DepartureTime': itinerary['DepartureDateTime'],
                'Arrive': itinerary['ArrivalAirportLocationCode'],
                'ArriveTime': itinerary['ArrivalDateTime'],
                'FlightNumber': itinerary.AirItineraryLegs.AirItineraryLeg[0]['FlightNumber'],
                'OperationCarrier': itinerary.AirItineraryLegs.AirItineraryLeg[0]['OperatingCarrierCode'],
                'MarketingCarrier': itinerary.AirItineraryLegs.AirItineraryLeg[0]['MarketingCarrierCode'],
                'Scale': scale

            }

        }
        itinerarios.update(x)

    ### PRECIO con los ID de vuelos que se pueden usar
    """
                Front cabin position (First) :F
                Front cabin position (Business) :C
                Rear cabin position (Economy) :Y
                Rear cabin (Economy, Premium Class) :W
                Rear cabin (Economy, excluding Premium Class) :M
    """
    vuelos = []

    for air_pricing_group in result.AirAvail.AirPricingGroups.AirPricingGroup:

        discount = float(air_pricing_group['DiscountAmount'])
        price_adult = float(air_pricing_group['AdultTicketAmount'])
        price_child = float(air_pricing_group['ChildrenTicketAmount'])
        price_infant = float(air_pricing_group['InfantTicketAmount'])
        tax_adult = float(air_pricing_group['AdultTaxAmount'])
        tax_child = float(air_pricing_group['ChildrenTaxAmount'])
        tax_infant = float(air_pricing_group['InfantTaxAmount'])
        aramix_fee = float(air_pricing_group['AramixFeeAmount'])
        price_per_user = {

            'Adult': {
                'Price': price_adult,
                'Tax': tax_adult,
                'Fee': aramix_fee,
                'Discount': discount

            },
            'Child': {
                'Price': price_child,
                'Tax': tax_child,
                'Fee': aramix_fee,
                'Discount': discount

            },
            'Infant': {
                'Price': price_infant,
                'Tax': price_infant,
                'Fee': aramix_fee,
                'Discount': discount

            }

        }
        price_total_adult = ((price_adult * num_adult) - (price_adult * num_adult_resident * discount)) + (
            (tax_adult + aramix_fee) * num_adult)
        price_total_child = ((price_child * num_child) - (price_child * num_child_resident * discount)) + (
            (tax_child + aramix_fee) * num_child)
        price_total_infant = ((price_infant * num_infant) - (price_infant * num_infant_resident * discount)) + (
            (tax_infant + aramix_fee) * num_infant)

        price = price_total_adult + price_total_child + price_total_infant

        price_group = air_pricing_group['GroupID']
        is_tour_operation = air_pricing_group['IsTourOperator']
        is_lowcost = air_pricing_group['IsLowCost']

        fare_notes = []
        for fare_note in air_pricing_group['FareNotes'].AirFareNote:
            x = {

                'Code': fare_note.NoteCode,
                'Category': fare_note.Category,
                'Description': fare_note.Description,

            }

            fare_notes.append(x)

        iti = {}
        for group_options in air_pricing_group.AirPricingGroupOptions.AirPricingGroupOption:
            for it in group_options.AirPricedItineraries.AirPricedItinerary:

                fare_code = it.AirPricedItineraryLegs.AirPricedItineraryLeg[0]['FareCode']
                fare_type = it.AirPricedItineraryLegs.AirPricedItineraryLeg[0]['FareType']
                num_seats = it.AirPricedItineraryLegs.AirPricedItineraryLeg[0]['AvailableSeats']
                num_bagage = it.AirPricedItineraryLegs.AirPricedItineraryLeg[0]['QuantityBaggageADT']
                weight_bagage = it.AirPricedItineraryLegs.AirPricedItineraryLeg[0]['QuantityWeightADT']
                type_bagage = it.AirPricedItineraryLegs.AirPricedItineraryLeg[0]['QuantityTypeADT']
                measure_bagage = it.AirPricedItineraryLegs.AirPricedItineraryLeg[0]['MeasureUnitADT']
                cabin_type = it.AirPricedItineraryLegs.AirPricedItineraryLeg[0]['CabinType']
                x = {
                    it['ItineraryID']: {
                        'FareCode': fare_code,
                        'FareType': fare_type,
                        'NumSeats': num_seats,
                        'NumBagage': num_bagage,
                        'WeightBagage': weight_bagage,
                        'TypeBagages': type_bagage,
                        'MeasureBagage': measure_bagage,
                        'CabinType': cabin_type
                    }

                }
                segment = int(str(it['ItineraryID'])[0])
                if segment not in iti:
                    iti[segment] = {}

                iti[segment].update(x)
        x = {
            'Price': price,
            'Price_per_user': price_per_user,
            'Price_group': price_group,
            'IsTourOperation': is_tour_operation,
            'IsLowcost': is_lowcost,
            'FareNotes': fare_notes,

            'Flights': iti

        }

        vuelos.append(x)

    for vuelo in vuelos:

        for k in vuelo['Flights'].keys():
            # keys solo tiene los segmentos ( 1..n)
            for v in vuelo['Flights'][k].keys():
                vuelo['Flights'][k][v].update(itinerarios[v])
    res = {'Status': 'OK', 'Flights': vuelos, 'Token': result.RequestID}

    return res


def response_rates(result):
    return {
        'notes': result['RateNotes']
    }


def response_booking_create(result):
    error = []
    warning = []
    messages = []
    if 'Error' in result:
        x = {
            'Code': result.Error.Code,
            'Description': result.Error.Description

        }
        error.append(x)
        return {'Status': 'KO', 'Error': error}

    res = result.AirBooking
    if res.Warnings is not None:
        for warn in res.Warnings:
            x = {
                'Code': warn.Warning.WarningCode,
                'Description': warn.Warning.Description
            }
            warning.append(x)

    if res.Messages is not None:
        for warn in res.Messages:
            x = {
                'Code': None,
                'Description': warn
            }
            warning.append(x)

    booking = {
        'Token': res.BookingID,
        'Reference': res.BookingReference,
        'BookingStatus': res.BookingStatus,
        'AmountDifference': res.AmountDifference,
        'MaxReserationDate': res.TicketIssueDateLimit,
        'CancellationInsurance': res.CancellationInsurance,
        'CancellationInsuranceAmount': res.CancellationInsuranceAmount,
        'TravelersInfoFares': res.TravelersInfoFares,
        'FlightTickets': res.FlightTickets,
        'Warnings': warning,
        'NetAmount': res.TotalAgencyAmount,
        'FullAmount': res.TotalAmount,
        'Messages': messages
    }
    return {'Status': 'OK', 'Booking': booking, 'Token': res.BookingID}


def response_booking_prepare(result):
    error = []
    warning = []
    messages = []
    status = 'OK'
    if 'Error' in result:
        x = {
            'Code': result.Error.Code,
            'Description': result.Error.Description

        }
        error.append(x)
        return {'Status': 'KO', 'Error': error}

    res = result.AirBooking
    print res
    if res.Warnings is not None:
        status = 'WARNING'
        for warn in res.Warnings.Warning:
            x = {
                'Code': warn.WarningCode,
                'Description': warn.Description
            }
            warning.append(x)

    if res.Messages is not None:
        for warn in res.Messages:
            x = {
                'Code': None,
                'Description': warn
            }
            warning.append(x)

    booking = {
        'Token': res.BookingID,
        'Reference': res.BookingReference,
        'BookingStatus': res.BookingStatus,
        'AmountDifference': res.AmountDifference,
        'MaxReserationDate': res.TicketIssueDateLimit,
        'CancellationInsurance': res.CancellationInsurance,
        'CancellationInsuranceAmount': res.CancellationInsuranceAmount,
        'TravelersInfoFares': res.TravelersInfoFares,
        'FlightTickets': res.FlightTickets,
        'Warnings': warning,
        'NetAmount': res.TotalAgencyAmount,
        'FullAmount': res.TotalAmount,
        'Messages': messages
    }
    return {'Status': status, 'PrepareBooking': booking, 'Token': res.BookingID}


def response_booking_confirm(result):
    print result
