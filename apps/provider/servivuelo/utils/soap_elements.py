# -*- coding: utf-8 -*-
class BaseObject(object):
    def __init__(self, client):
        self.client = client


#### BASIC PROCESS
class AuthenticationCredentials(BaseObject):
    def create(self, agency_id, user_id, password):
        credential = self.client.factory.create('AuthenticationCredentials')
        credential.AgencyID = agency_id
        credential.UserID = user_id
        credential.Password = password
        return credential


## TRAVELERS

class AirAvailClassPreferences(BaseObject):
    def create(self):
        credential = self.client.factory.create('AirAvailClassPreferences')
        return credential


class AirTravelerTypes(BaseObject):
    def create(self):
        credential = self.client.factory.create('AirTravelerTypes')
        return credential


class AirTravelerInfo(BaseObject):
    def create(self, traveler_type, is_resident):
        credential = self.client.factory.create('AirTravelerInfo')
        credential.TravelerType = traveler_type
        credential.IsResident = is_resident
        return credential


class ArrayOfAirTravelerInfo(BaseObject):
    def create(self, traveler_info):
        credential = self.client.factory.create('ArrayOfAirTravelerInfo')
        credential.AirTravelerInfo = traveler_info
        return credential


## AIR FLIGHT

class AirFlightSegmentRQ(BaseObject):
    def create(self, departure_location, arrive_location, departure_date, departure_time):
        credential = self.client.factory.create('AirFlightSegmentRQ')
        credential.DepartureAirportLocationCode = departure_location
        credential.ArrivalAirportLocationCode = arrive_location
        credential.DepartureDate = departure_date
        credential.DepartureTime = departure_time
        return credential


class ArrayOfAirFlightSegmentRQ(BaseObject):
    def create(self, flight_segment):
        credential = self.client.factory.create('ArrayOfAirFlightSegmentRQ')
        credential.AirFlightSegmentRQ = flight_segment
        return credential


### SEARCH

class AirAvailRQ(BaseObject):
    def create(self, direct_fligts, include_lowcost, class_pref, travelers, flight_segments):
        credential = self.client.factory.create('AirAvailRQ')
        credential.DirectFlightsOnly = direct_fligts
        credential.IncludeLowCost = include_lowcost
        credential.ClassPref = class_pref
        credential.Travelers = travelers
        credential.FlightSegments = flight_segments
        return credential


## RATES
class AirAvailRateNotesRQ(BaseObject):
    def create(self, request_id, pricing_group_id, itineraries_id, adult_number=0, child_number=0, infant_number=0):
        credential = self.client.factory.create('AirAvailRateNotesRQ')
        credential.AvailRequestID = request_id
        credential.PricingGroupID = pricing_group_id
        for it_int in itineraries_id:
            credential.ItinerariesID.int.append(it_int)

        credential.AdultNumber = adult_number
        credential.ChildrenNumber = child_number
        credential.InfantNumber = infant_number
        return credential


#### BOOKING PROCESS

class AirTravelerTSAData(BaseObject):
    def create(self, first_name, last_name, gender, birth_date, doc_exp_date, doc_issue_country, doc_type, doc_number,
               birth_country, nationality_country, visa_issued_city, visa_number, visa_issue_country, visa_issue_date,
               is_usa_resident, usa_city, usa_state, usa_residence_type, usa_address, usa_zipcode):
        credential = self.client.factory.create('AirTravelerTSAData')
        credential.FirstName = first_name
        credential.LastName = last_name
        """
        Gender
        M: Male,
        F: Female
        MI: Male Infant
        FI: Female Infant
        U: Undisclosed Gender

        """

        credential.Gender = gender  #
        credential.BirthDate = birth_date  # formato dd/mm/yyyy
        credential.DocumentExpirationDate = doc_exp_date
        credential.DocumentIssueCountry = doc_issue_country

        """
        DocumentType
        I: Documento Nacional de Identidad,
        P: Pasaporte,
        IP: Tarjeta de residente o tarjeta pasaporte,
        A: Otra tarjeta de identidad,
        AC: Certificado de miembro de tripulacion,
        F: Cartilla verde americana

        """
        credential.DocumentType = doc_type
        credential.DocumentNumber = doc_number
        credential.BirthCountry = birth_country
        credential.NationalityCountry = nationality_country
        credential.VisaIssueCity = visa_issued_city
        credential.VisaNumber = visa_number
        credential.VisaIssueCountry = visa_issue_country
        credential.VisaIssueDate = visa_issue_date  # formato dd/mm/yyyy
        credential.IsResidentUSA = is_usa_resident
        credential.USA_City = usa_city
        credential.USA_State = usa_state
        credential.USA_ResidenceType = usa_residence_type  # (D: en el destino, R: residencia)
        credential.USA_Address = usa_address
        credential.USA_ZipCode = usa_zipcode

        return credential


class AirTraveler(BaseObject):
    def create(self, traveler_obj, flight_to_usa=False, tsdata=None):

        traveler_type = traveler_obj.get('TravelerType')
        additional_baggages = traveler_obj.get('AdditionalBaggages', "0")
        traveler_title = traveler_obj.get('TravelerTitle')
        document_type = traveler_obj.get('DocumentType')
        traveler_id = traveler_obj.get('TravelerId')
        infant_id = traveler_obj.get('InfantId')
        first_name = traveler_obj.get('FirstName')
        last_name = traveler_obj.get('LastName')
        document_number = traveler_obj.get('DocumentNumber')
        email = traveler_obj.get('Email')
        phone = traveler_obj.get('Phone')
        birth_date = traveler_obj.get('BirthDate')
        is_resident = traveler_obj.get('IsResident')
        resident_document_type = traveler_obj.get('ResidentDocumentType')
        resident_city_code = traveler_obj.get('ResidentCityCode')
        resident_certificate_number = traveler_obj.get('ResidentCertificateNumber')
        flyer_program = traveler_obj.get('FrequentFlyerProgram')
        flyer_card_number = traveler_obj.get('FrequentFlyerCardNumber')

        if flight_to_usa:
            gender = tsdata['gender']
            doc_exp_date = tsdata['doc_exp_date']
            doc_issue_country = tsdata['doc_issue_country']
            doc_type = tsdata['doc_type']
            doc_number = tsdata['doc_number']
            birth_country = tsdata['birth_country']
            nationality_country = tsdata['nationality_country']
            visa_issued_city = tsdata['visa_issued_city']
            visa_number = tsdata['visa_number']
            visa_issue_country = tsdata['visa_issue_country']
            visa_issue_date = tsdata['visa_issue_date']
            is_usa_resident = tsdata['is_usa_resident']
            usa_city = tsdata['usa_city']
            usa_state = tsdata['usa_state']
            usa_residence_type = tsdata['usa_residence_type']
            usa_address = tsdata['usa_address']
            usa_zipcode = tsdata['usa_zipcode']
            tsdata_data = AirTravelerTSAData().create(first_name=first_name, last_name=last_name, gender=gender,
                                                      birth_date=birth_date,
                                                      doc_exp_date=doc_exp_date, doc_issue_country=doc_issue_country,
                                                      doc_type=doc_type,
                                                      doc_number=doc_number, birth_country=birth_country,
                                                      nationality_country=nationality_country,
                                                      visa_issued_city=visa_issued_city,
                                                      visa_number=visa_number, visa_issue_country=visa_issue_country,
                                                      visa_issue_date=visa_issue_date, is_usa_resident=is_usa_resident,
                                                      usa_city=usa_city,
                                                      usa_state=usa_state, usa_residence_type=usa_residence_type,
                                                      usa_address=usa_address,
                                                      usa_zipcode=usa_zipcode)

        else:
            tsdata_data = None

        credential = self.client.factory.create('AirTraveler')
        credential.TravelerType = traveler_type  # AirTravelerTypes ( Adulto, niño , bebe)
        credential.TravelerTitle = traveler_title  # AirTravelerTitles(Mr, Mrs, Ms)

        credential.DocumentType = document_type  # Document Type(DNI, NIE, PAS, OTR)
        credential.TravelerID = traveler_id  # TravelerID
        credential.InfantID = infant_id  # TravelerID of infant assigned to adult
        credential.FirstName = first_name
        credential.LastName = last_name
        credential.DocumentNumber = document_number
        credential.Email = email
        credential.Phone = phone
        credential.BirthDate = birth_date
        credential.FrequentFlyerProgram = flyer_program
        credential.FrequentFlyerCardNumber = flyer_card_number
        credential.AdditionalBaggages = additional_baggages
        credential.IsResident = is_resident

        """

        DN: DNI
        GR: Senadores y diputados ( trabajando temporalmente)
        TR : Para NIE
        MR : Menor de Edad

        """
        credential.ResidentDocumentType = resident_document_type  # ResidentDocumentType(DN,CR,GR,TR,MR,AM)
        credential.ResidentCityCode = resident_city_code
        credential.ResidentCertificateNumber = resident_certificate_number
        credential.TSAData = tsdata_data  # TSAData

        """
        ticket_amount=0,tax_amount, agency_fee_amount, aramix_fee_amount, ob_fee_amount, segment_leg
        credential.TicketAmount = ticket_amount
        credential.TaxAmount = tax_amount
        credential.AgencyFeeAmount = agency_fee_amount
        credential.AramixFeeAmount = aramix_fee_amount
        credential.OBFeeAmount = ob_fee_amount
        credential.LstAirTravelerFlightSegmentLeg = segment_leg  # ArrayOfAirTravelerFlightSegmentLeg
        """

        return credential


class AirBookingRQ(BaseObject):
    def create(self, request_id, pricing_group_id, itineraries_id, travelers, issuer_notes, customer_notes,
               cancellation_insurance):
        credential = self.client.factory.create('AirBookingRQ')
        credential.AvailRequestID = request_id
        credential.PricingGroupID = pricing_group_id
        for it_int in itineraries_id:
            credential.ItinerariesID.int.append(it_int)

        for traveler in travelers:
            credential.AirTravelers.AirTraveler.append(traveler)

        credential.IssuerNotes = issuer_notes
        credential.CustomerNotes = customer_notes
        credential.CancellationInsurance = cancellation_insurance

        return credential


class AirBookingImportRQ(BaseObject):
    def create(self, agency_id, user_id, locator):
        credential = self.client.factory.create('AirBookingImportRQ')
        credential.AgencyID = agency_id
        credential.UserID = user_id
        credential.Locator = locator

        return credential


class AirBookingStatus(BaseObject):
    def create(self):
        credential = self.client.factory.create('AirBookingStatus')
        return credential


class AirBookingMarkToTicketingRQ(BaseObject):
    def create(self, booking_id, payment_form, card_id, insurance_amount=0, observations='', amount_difference=0):
        credential = self.client.factory.create('AirBookingMarkToTicketingRQ')
        credential.BookingID = booking_id
        credential.PaymentForm = payment_form
        credential.CardID = card_id
        credential.InsuranceAmount = insurance_amount
        credential.Observations = observations
        credential.AmountDifference = amount_difference

        return credential

class AirBookingTicketingRQ(BaseObject):
    def create(self, booking_id, card_id, insurance_amount=0, amount_difference=0):
        credential = self.client.factory.create('AirBookingTicketingRQ')
        credential.BookingID = booking_id
        credential.CardID = card_id
        credential.InsuranceAmount = insurance_amount
        credential.AmountDifference = amount_difference

        return credential


### INVOICES

class AirInvoiceAvsisRQ(BaseObject):
    def create(self, agency_id, user_id, password, localizador):
        credential = self.client.factory.create('AirInvoiceAvsisRQ')
        credential.IDAgencia = agency_id
        credential.UserID = user_id
        credential.Password = password
        credential.Localizador = localizador
        return credential
