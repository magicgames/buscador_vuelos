# -*- coding: utf-8 -*-

"""

METODOS IMPLEMENTADOS


AgencyCardsList(AuthenticationCredentials credentials)
AirlineCarriersList(AuthenticationCredentials credentials)
AirDestinationAirportsList(AuthenticationCredentials credentials)
AirInvoiceAvsis(AirInvoiceAvsisRQ credentials)
AirAvailGetRateNotes(AuthenticationCredentials credentials, AirAvailRateNotesRQ airAvailRateNotesRQ)
AirBookingCancel(AuthenticationCredentials credentials, xs:int bookingID)
AirBookingCancelTickets(AuthenticationCredentials credentials, xs:int bookingID)
AirBookingEdit(AuthenticationCredentials credentials, xs:int bookingID)
AirBookingGetRateNotes(AuthenticationCredentials credentials, xs:int bookingID)
AirMunicipalCodeList(AuthenticationCredentials credentials, ns2:ArrayOfstring lstIata)
AirBookingCreate(AuthenticationCredentials credentials, AirBookingRQ airBookingRQ)
AirBookingImport(AuthenticationCredentials credentials, AirBookingImportRQ airBookingImportRQ)
AirBookingMarkToTicketing(AuthenticationCredentials credentials, AirBookingMarkToTicketingRQ airBookingMarkToTicketingRQ)



"""
"""
Métodos del cliente



            AirBookingSearch(AuthenticationCredentials credentials, AirBookingSearchRQ airBookingSearchRQ)
            AirBookingTicketing(AuthenticationCredentials credentials, AirBookingTicketingRQ airBookingTicketingRQ)
            AirBookingUpdateTravelerTSAData(AuthenticationCredentials credentials, AirBookingUpdateTravelerTSADataRQ airBookingUpdateTsaTravelerRQ)



"""

import logging

from suds.client import Client
from suds.plugin import MessagePlugin

logger = logging.getLogger('servivuelo')
from apps.provider.servivuelo.utils.soap_elements import *


class LogPlugin(MessagePlugin):
    def sending(self, context):
        x = str(context.envelope)
        logger.debug(x)

    def received(self, context):
        x = str(context.reply)
        logger.debug(x)


# client = Client(url)

class BasicClientInfo(object):
    def __init__(self, agency_id, user_id, password):
        url = 'http://integrawspublico.aramix.es/v1_0/ServiceAereo.svc?singleWsdl'
        client = Client(url, plugins=[LogPlugin()])

        self.client = client
        self.agency_id = agency_id
        self.user_id = user_id
        self.password = password
        self.credentials = AuthenticationCredentials(self.client).create(agency_id=agency_id, user_id=user_id,
                                                                         password=password)


## DISPONIBILIDAD


class AirAvailCalendarSearch(BasicClientInfo):
    def provider_url(self):
        result = self.client.service.AirAvailCalendarSearch(self.credentials, self.request)
        return result

    def create(self, people, destinations, direct_fligts=False, include_lowcost=True, class_pref=None):
        traveler_type = AirTravelerTypes(self.client).create()  # traveler_type
        traveler_info = []
        flight_segments = []
        for person in people:
            passenger = AirTravelerInfo(self.client).create(traveler_type=person['type'],
                                                            is_resident=person['is_resident'])
            traveler_info.append(passenger)
        travelers = ArrayOfAirTravelerInfo(self.client).create(traveler_info=traveler_info)

        for destination in destinations:
            flightsegment = AirFlightSegmentRQ(self.client).create(destination['departure'], destination['arrive'],
                                                                   destination['date'], destination['time'])
            flight_segments.append(flightsegment)
        flight_segments = ArrayOfAirFlightSegmentRQ(self.client).create(flight_segment=flight_segments)
        self.request = AirAvailRQ(self.client).create(direct_fligts=direct_fligts,
                                                      include_lowcost=include_lowcost,
                                                      class_pref=class_pref, travelers=travelers,
                                                      flight_segments=flight_segments)
        result = self.provider_url()
        return result


class AirAvailSearch(AirAvailCalendarSearch):
    def provider_url(self):
        result = self.client.service.AirAvailSearch(self.credentials, self.request)
        return result


class AirAvailMultiSegmentSearch(AirAvailCalendarSearch):
    def provider_url(self):
        result = self.client.service.AirAvailMultiSegmentSearch(self.credentials, self.request)
        return result


## RESERVAS

class AgencyCardsList(BasicClientInfo):
    def provider_url(self):
        result = self.client.service.AgencyCardsList(self.credentials)
        return result

    def create(self):
        result = self.provider_url()
        return result


class AirAvailGetRateNotes(BasicClientInfo):
    def provider_url(self, air_avail):
        result = self.client.service.AirAvailGetRateNotes(self.credentials, air_avail)

        return result

    def create(self, request_id, pricing_group_id, itineraries_id, adult_number=0, child_number=0, infant_number=0):
        air_avail = AirAvailRateNotesRQ(self.client).create(request_id=request_id, pricing_group_id=pricing_group_id,
                                                            itineraries_id=itineraries_id, adult_number=adult_number,
                                                            child_number=child_number, infant_number=infant_number)
        result = self.provider_url(air_avail)
        return result


class AirBookingCancel(BasicClientInfo):
    def provider_url(self, param):
        result = self.client.service.AirBookingCancel(self.credentials, param)
        return result

    def create(self, param):
        result = self.provider_url(param)
        return result


class AirBookingCancelTickets(BasicClientInfo):
    def provider_url(self, param):
        result = self.client.service.AirBookingCancelTickets(self.credentials, param)
        return result

    def create(self, param):
        result = self.provider_url(param)
        return result


class AirBookingEdit(BasicClientInfo):
    def provider_url(self, param):
        result = self.client.service.AirBookingEdit(self.credentials, param)
        return result

    def create(self, param):
        result = self.provider_url(param)
        return result


class AirBookingGetRateNotes(BasicClientInfo):
    def provider_url(self, param):
        result = self.client.service.AirBookingGetRateNotes(self.credentials, param)
        return result

    def create(self, param):
        result = self.provider_url(param)
        return result


class AirBookingCreate(BasicClientInfo):
    def provider_url(self, param):
        result = self.client.service.AirBookingCreate(self.credentials, param)
        return result

    def create(self, request_id, pricing_group_id, itineraries_id, traveler_info, issuer_notes, customer_notes,
               cancellation_insurance=False, flight_to_usa=False, tsdata=None):
        air_booking_create = AirBookingRQ(self.client)
        travelers = []
        for traveler in traveler_info:
            traveler_obj = AirTraveler(self.client)
            travel = traveler_obj.create(traveler_obj=traveler, flight_to_usa=flight_to_usa, tsdata=tsdata)
            travelers.append(travel)

        air_booking = air_booking_create.create(request_id=request_id, pricing_group_id=pricing_group_id,
                                                itineraries_id=itineraries_id, travelers=travelers,
                                                issuer_notes=issuer_notes,
                                                customer_notes=customer_notes,
                                                cancellation_insurance=cancellation_insurance)

        result = self.provider_url(air_booking)
        return result


class AirBookingMarkToTicketing(BasicClientInfo):
    def provider_url(self, param):
        result = self.client.service.AirBookingMarkToTicketing(self.credentials, param)
        return result

    def create(self, booking_id, payment_form='UYU', card_id='1031', insurance_amount=0, observations='',
               amount_difference=0):
        """

        :param int  booking_id: Id de reserva
        :param string payment_form: por defecto "UYU" que es tarjeta de crédito
        :param int card_id: por defecto 1031 que es la tarjeta de prueba (mirar la de producción)
        :param float insurance_amount: Valor del seguro, de momento es cero
        :param string observations: Cualquier tipo de observación
        :param float amount_difference: La direferencia entre el precio marcado y el de reserva permitido para hacer la reserva
        :return:
        """
        param = AirBookingMarkToTicketingRQ(self.client).create(booking_id=booking_id, payment_form=payment_form,
                                                                card_id=card_id,
                                                                insurance_amount=insurance_amount,
                                                                observations=observations,
                                                                amount_difference=amount_difference)
        result = self.provider_url(param)
        return result


class AirBookingTicketing(BasicClientInfo):
    def provider_url(self, param):
        result = self.client.service.AirBookingTicketing(self.credentials, param)
        return result

    def create(self, booking_id, card_id='1031', insurance_amount=0, amount_difference=0):
        """

        :param int  booking_id: Id de reserva
        :param string payment_form: por defecto "UYU" que es tarjeta de crédito
        :param int card_id: por defecto 1031 que es la tarjeta de prueba (mirar la de producción)
        :param float insurance_amount: Valor del seguro, de momento es cero
        :param string observations: Cualquier tipo de observación
        :param float amount_difference: La direferencia entre el precio marcado y el de reserva permitido para hacer la reserva
        :return:
        """
        param = AirBookingTicketingRQ(self.client).create(booking_id=booking_id, card_id=card_id,
                                                          insurance_amount=insurance_amount,
                                                          amount_difference=amount_difference)
        result = self.provider_url(param)
        return result


class AirBookingImport(BasicClientInfo):
    def provider_url(self, param):
        result = self.client.service.AirBookingImport(self.credentials, param)
        return result

    def create(self, agency_id, user_id, locator):
        param = AirBookingImportRQ().create(agency_id=agency_id, user_id=user_id, locator=locator)
        result = self.provider_url(param)
        return result


## BASIC INFO
class AirInvoiceAvsis(BasicClientInfo):
    def provider_url(self, localizador):
        air_invoice = AirInvoiceAvsisRQ(self.client).create(agency_id=self.agency_id, user_id=self.user_id,
                                                            password=self.password, localizador=localizador)
        result = self.client.service.AgencyCardsList(air_invoice)
        return result

    def create(self, localizador):
        result = self.provider_url()
        return result


class AirlineCarriersList(AgencyCardsList):
    def provider_url(self):
        result = self.client.service.AirlineCarriersList(self.credentials)
        return result


class AirDestinationAirportsList(AgencyCardsList):
    def provider_url(self):
        result = self.client.service.AirDestinationAirportsList(self.credentials)
        return result


###

class AirMunicipalCodeList(BasicClientInfo):
    def provider_url(self, param):
        result = self.client.service.AirMunicipalCodeList(self.credentials, param)
        return result

    def create(self, param):
        result = self.provider_url(param)
        return result

### SEARCH SYSTEM ####
