# -*- coding: utf-8 -*-
import simplejson as json
import datetime
# Create your views here.
from django.http import HttpResponseBadRequest, HttpResponse
from django.views.generic import View
from django.db.models import Q
from django.utils.translation import ugettext as _
from django.conf import settings
from provider.servivuelo.utils.response_results import *
from provider.servivuelo.utils.basic import get_iata_choices
from zato.client import JSONClient
from provider.models import AirLines, MunicipalIata
from .utils.basic import *
from common.views import LoginRequiredView
from django.core.cache import cache


class SearchFlightApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        body = self.request.read()

        d = json.loads(body)

        req = generate_search_flight_request(d)
        req = generate_default_value(req, self.request.user)

        try:
            if req['reason'] != '':
                raise
            client_json = JSONClient(settings.CLIENT_ADDRESS, '/flight/search', settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(req['request'])
            print json.dumps(result.data)
            return HttpResponse(json.dumps(result.data), content_type="application/json")
        except BaseException as e:
            print e
            print req['reason']
            return HttpResponseBadRequest(content=req['reason'])


class SearchMultiFlightApiView(LoginRequiredView, View):

    def post(self, request, *args, **kwargs):

        """
        eliminamos el sistema de cache
        if cache.get('vuelo') is not None:
            return HttpResponse(json.dumps(cache.get('vuelo')), content_type="application/json")
        """

        body = self.request.read()

        d = json.loads(body)
        req = generate_search_flight_request(d)
        req = generate_default_value(req, self.request.user)

        try:
            if req['reason'] != '':
                raise
            client_json = JSONClient(settings.CLIENT_ADDRESS, '/flight/search/multiple', settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(req['request'])
            print json.dumps(result.data);
            return HttpResponse(json.dumps(result.data), content_type="application/json")
        except Exception as e:
            print e
            return HttpResponseBadRequest(content=req['reason'])


class CheckRatesApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        body = self.request.read()
        d = json.loads(body)
        req = generate_rates_request(d)
        req = generate_default_value(req, self.request.user)
        try:
            if req['reason'] != '':
                raise
            client_json = JSONClient(settings.CLIENT_ADDRESS, '/flight/search/rates', settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(req['request'])
            return HttpResponse(json.dumps(result.data), content_type="application/json")

        except Exception as e:
            return HttpResponseBadRequest(content=req['reason'])


class BookingCreateApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        body = self.request.read()
        d = json.loads(body)
        print d
        req = generate_booking_create_request(d)
        req = generate_default_value(req, self.request.user)
        try:
            print req
            if req['reason'] != '':
                raise
            print req['request']
            client_json = JSONClient(settings.CLIENT_ADDRESS, '/flight/booking/create', settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(req['request'])
            print result
            return HttpResponse(json.dumps(result.data), content_type="application/json")
        except BaseException as e:
            print e
            return HttpResponseBadRequest(content=req['reason'])


class BookingRemoveApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        body = self.request.read()
        d = json.loads(body)
        req = generate_booking_remove_request(d)
        req = generate_default_value(req, self.request.user)
        try:
            if req['reason'] != '':
                raise
            client_json = JSONClient(settings.CLIENT_ADDRESS, '/flight/booking/remove', settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(req['request'])
            return HttpResponse(json.dumps(result.data), content_type="application/json")
        except Exception as e:
            return HttpResponseBadRequest(content=req['reason'])


class BookingEditApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        body = self.request.read()
        d = json.loads(body)
        req = generate_booking_edit_request(d)
        req = generate_default_value(req, self.request.user)
        try:
            if req['reason'] != '':
                raise
            client_json = JSONClient(settings.CLIENT_ADDRESS, '/flight/booking/edit', settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(req['request'])
            return HttpResponse(json.dumps(result.data), content_type="application/json")
        except Exception as e:
            print e
            return HttpResponseBadRequest(content=req['reason'])


class BookingPrepareApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        body = self.request.read()
        d = json.loads(body)
        reason = ''
        payment_form = 'CYC'
        card_id = 1031
        insurance_amount = 0
        observations = ''
        amount_difference = float(d.get('difference', 0.00))
        booking_id = 0

        try:
            booking_id = int(d['token'])

        except Exception as e:
            reason = _(u'Token incorrecto')
            pass
        try:
            if reason != '':
                raise

            request = {
                'id_agencia': '101079',
                'username': '1140',
                'password': 'KR54SS-32',

                'token': d['token'],

                'paymentForm': payment_form,
                'observations': observations,
                'insuranceAmount': insurance_amount,
                'amountDifference': amount_difference
            }
            client_json = JSONClient(settings.CLIENT_ADDRESS, '/flight/booking/mark', settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(request)
            return HttpResponse(json.dumps(result.data), content_type="application/json")


        except Exception as e:
            print e
            import sys
            reload(sys)
            sys.setdefaultencoding("utf-8")
            return HttpResponseBadRequest(content=reason)


class ServivueloAirportApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        body = self.request.read()
        d = {}
        req = generate_aiport_list_request(d)
        req = generate_default_value(req, self.request.user)
        try:
            if req['reason'] != '':
                raise
            print json.dumps(req['request'])
            client_json = JSONClient(settings.CLIENT_ADDRESS, '/flight/getairports', settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(req['request'])
            print result
            return HttpResponse(json.dumps(result.data), content_type="application/json")
        except Exception as e:
            print e
            return HttpResponseBadRequest(content=req['reason'])

# AJAX VIEWS FOR SELECTORS
class SelectorAeropuertosApiView(LoginRequiredView, View):
    def get(self, header, alternate=None):
        result = []
        air_ports = (get_iata_choices().values())

        for air_port in air_ports:
            x = {
                'code': air_port['code'],
                'name': air_port['name']

            }
            result.append(x)

        return HttpResponse(json.dumps(result), content_type="application/json")


class SelectorAerolineasApiView(LoginRequiredView, View):
    def get(self, header, alternate=None):
        result = {}
        air_ports = (AirLines.objects.filter().values('carrier', 'alias', 'logo'))

        for air_port in air_ports:
            x = {
                air_port['carrier']: {
                    'Code': air_port['alias'],
                    'Image': air_port['logo']
                }

            }
            result.update(x)

        return HttpResponse(json.dumps(result), content_type="application/json")


class SelectorResidentApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        body = self.request.read()
        result = []
        d = json.loads(body)
        from_airport = d['from']
        to_airport = d['to']
        data = MunicipalIata.objects.filter(Q(iata__pk=from_airport) | Q(iata__pk=to_airport)).distinct('name',
                                                                                                        'code').values(
            'code', 'name')
        for res in data:
            x = {
                'name': res['name'],
                'code': res['code'],
            }
            result.append(x)

        return HttpResponse(json.dumps(result), content_type="application/json")
