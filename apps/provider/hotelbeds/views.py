# -*- coding: utf-8 -*-
import datetime

import simplejson as json
from elasticsearch import Elasticsearch

# Create your views here.
from django.conf import settings
from django.http import HttpResponseBadRequest, HttpResponse
from django.views.generic import View
from zato.client import JSONClient
from django.http import HttpResponse
from utils.basic import *
from common.views import LoginRequiredView


# Create your views here.

####### HOTELS
class SearchHotelApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        body = self.request.read()
        d = json.loads(body)
        request = generate_search_hotel_request(d)
        request = generate_default_value(request, d, self.request.user)
        print json.dumps(request['request'])
        try:
            if request["reason"] != "":
                raise
            client_json = JSONClient(settings.CLIENT_ADDRESS, "/hotel/search", settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(request["request"])
            print json.dumps(result.data)
            return HttpResponse(json.dumps(result.data), content_type="application/json")
        except Exception as e:
            return HttpResponseBadRequest(content=request["reason"])


class BlockHotelApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        body = self.request.read()
        d = json.loads(body)

        request = generate_block_hotel_request(d)
        request = generate_default_value(request, d, self.request.user)
        try:
            if request["reason"] != "":
                raise
            client_json = JSONClient(settings.CLIENT_ADDRESS, "/hotel/booking/create", settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(request["request"])
            print result.data
            return HttpResponse(json.dumps(result.data), content_type="application/json")
        except Exception as e:
            return HttpResponseBadRequest(content=request["reason"])


class PurchaseHotelApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        body = self.request.read()

        d = json.loads(body)
        print d

        request = generate_puchase_hotel_request(d)
        request = generate_default_value(request, d, self.request.user)
        try:
            if request["reason"] != "":
                raise
            client_json = JSONClient(settings.CLIENT_ADDRESS, "/hotel/booking/confirm", settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(request["request"])
            print result.data
            return HttpResponse(json.dumps(result.data), content_type="application/json")
        except Exception as e:
            return HttpResponseBadRequest(content=request["reason"])


class CancelHotelApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        result = {}
        body = self.request.read()
        d = json.loads(body)
        request = generate_cancel_hotel_request(d)
        request = generate_default_value(request, d, self.request.user)
        try:
            if request["reason"] != "":
                raise
            client_json = JSONClient(settings.CLIENT_ADDRESS, "/hotel/booking/cancel", settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(request["request"])
            print result.data
            return HttpResponse(json.dumps(result.data), content_type="application/json")
        except Exception as e:
            return HttpResponseBadRequest(content=request["reason"])


class RemoveHotelApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        result = {}
        body = self.request.read()
        print body
        d = json.loads(body)
        print d
        request = generate_remove_hotel_request(d)
        request = generate_default_value(request, d, self.request.user)
        try:
            if request["reason"] != "":
                raise
            print request["request"]
            client_json = JSONClient(settings.CLIENT_ADDRESS, "/hotel/booking/remove", settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(request["request"])
            print result.data
            return HttpResponse(json.dumps(result.data), content_type="application/json")
        except Exception as e:
            return HttpResponseBadRequest(content=request["reason"])


#### TRANSFER

class SearchTransferApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        result = {}
        body = self.request.read()

        d = json.loads(body)
        request = generate_search_transfer_request(d)

        request = generate_default_value(request, d, self.request.user)
        try:
            if request["reason"] != "":
                raise
            print request
            client_json = JSONClient(settings.CLIENT_ADDRESS, "/transfer/search", settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(request["request"])
            print json.dumps(result.data)
            return HttpResponse(json.dumps(result.data), content_type="application/json")
        except Exception as e:
            return HttpResponseBadRequest(content=request["reason"])


class BlockTransferApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        result = {}
        body = self.request.read()
        print body
        d = json.loads(body)
        request = generate_booking_transfer_request(d)
        request = generate_default_value(request, d, self.request.user)

        print request
        try:
            if request["reason"] != "":
                raise
            client_json = JSONClient(settings.CLIENT_ADDRESS, "/transfer/booking/create", settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(request["request"])
            print json.dumps(result.data)
            return HttpResponse(json.dumps(result.data), content_type="application/json")
        except Exception as e:
            return HttpResponseBadRequest(content=request["reason"])


class PurchaseTransferApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        result = {}
        body = self.request.read()
        print body
        d = json.loads(body)
        request = generate_puchase_transfer_request(d)
        request = generate_default_value(request, d, self.request.user)

        try:
            if request["reason"] != "":
                raise
            client_json = JSONClient(settings.CLIENT_ADDRESS, "/transfer/booking/confirm", settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(request["request"])
            print json.dumps(result.data)
            return HttpResponse(json.dumps(result.data), content_type="application/json")
        except Exception as e:
            return HttpResponseBadRequest(content=request["reason"])


class CancelTransferApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        result = {}
        body = self.request.read()
        d = json.loads(body)
        request = generate_cancel_hotel_request(d)
        request = generate_default_value(request, d, self.request.user)
        try:
            if request["reason"] != "":
                raise
            client_json = JSONClient(settings.CLIENT_ADDRESS, "/transfer/booking/cancel", settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(request["request"])
            print result.data
            return HttpResponse(json.dumps(result.data), content_type="application/json")
        except Exception as e:
            return HttpResponseBadRequest(content=request["reason"])


class RemoveTransferApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        result = {}
        body = self.request.read()
        d = json.loads(body)
        request = generate_remove_transfer_request(d)
        request = generate_default_value(request, d, self.request.user)
        try:
            if request["reason"] != "":
                raise
            client_json = JSONClient(settings.CLIENT_ADDRESS, "/transfer/booking/remove", settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(request["request"])

            return HttpResponse(json.dumps(result.data), content_type="application/json")
        except Exception as e:
            return HttpResponseBadRequest(content=request["reason"])


### TICKETS

class SearchTicketApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        pass


class BlockTicketApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        pass


class PurchaseTicketApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        pass


# SELECTORS

class BaseSelectorDestinationApi(LoginRequiredView, View):
    url = None

    def post(self, request, *args, **kwargs):
        results = []
        body = self.request.read()
        d = json.loads(body)

        try:
            query = d["query"]
            if len(query) < 3:
                raise
        except:
            return HttpResponse(json.dumps(results), content_type="application/json")

        try:
            lang = d["lang"]
        except:
            lang = settings.DEFAULT_HOTEL_SEARCH

        request = {
            "query": query,
            "lang": lang
        }
        client_json = JSONClient(settings.CLIENT_ADDRESS, self.url, settings.CLIENT_CREDENTIALS)
        result = client_json.invoke(request)
        return HttpResponse(json.dumps(result.data), content_type="application/json")


class SelectorDestinationApiView(BaseSelectorDestinationApi):
    url = "/hotel/search/autocomplete"


class SelectorLiteDestinationApiView(BaseSelectorDestinationApi):
    url = "/hotel/search/autocompletelite"


class SelectorTransferDestinationApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        results = []
        body = self.request.read()
        d = json.loads(body)

        try:
            query = d["query"]
            if len(query) < 3:
                raise
        except:
            return HttpResponse(json.dumps(results), content_type="application/json")

        try:
            lang = d["lang"]
        except:
            lang = settings.DEFAULT_HOTEL_SEARCH

        request = {
            "query": query,
            "lang": lang
        }
        client_json = JSONClient(settings.CLIENT_ADDRESS, "/transfer/search/autocomplete", settings.CLIENT_CREDENTIALS)
        result = client_json.invoke(request)
        return HttpResponse(json.dumps(result.data), content_type="application/json")


class SelectorTicketDestinationApiView(LoginRequiredView, View):
    def post(self, request, *args, **kwargs):
        results = []
        body = self.request.read()
        d = json.loads(body)

        try:
            query = d["query"]
            if len(query) < 3:
                raise
        except:
            return HttpResponse(json.dumps(results), content_type="application/json")

        try:
            lang = d["lang"]
        except:
            lang = settings.DEFAULT_HOTEL_SEARCH

        request = {
            "query": query,
            "lang": lang
        }
        client_json = JSONClient(settings.CLIENT_ADDRESS, "/ticket/search/autocomplete", settings.CLIENT_CREDENTIALS)
        result = client_json.invoke(request)
        return HttpResponse(json.dumps(result.data), content_type="application/json")
