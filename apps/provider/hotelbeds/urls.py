# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from .views import *

urlpatterns = patterns('',
                       url(regex=r'^destination$', view=SelectorDestinationApiView.as_view(), name='destination_hotel'),
                       url(regex=r'^destinationlite$', view=SelectorLiteDestinationApiView.as_view(), name='destinationlite_hotel'),


                       url(regex=r'^search$', view=SearchHotelApiView.as_view(), name='search_hotel'),
                       url(regex=r'^booking/create$', view=BlockHotelApiView.as_view(), name='block_hotel'),
                       url(regex=r'^booking/confirm$', view=PurchaseHotelApiView.as_view(), name='purchase_hotel'),
                       url(regex=r'^booking/cancel$', view=CancelHotelApiView.as_view(), name='cancel_hotel'),
                       url(regex=r'^booking/remove$', view=RemoveHotelApiView.as_view(), name='remove_hotel'),
                       url(regex=r'^transfer/destination$', view=SelectorTransferDestinationApiView.as_view(),
                           name='destination_transfer'),
                       url(regex=r'^transfer/search$', view=SearchTransferApiView.as_view(), name='search_transfer'),
                       url(regex=r'^transfer/booking/create$', view=BlockTransferApiView.as_view(),
                           name='block_transfer'),
                       url(regex=r'^transfer/booking/remove$', view=RemoveTransferApiView.as_view(),
                           name='remove_transfer'),
                       url(regex=r'^transfer/booking/cancel$', view=CancelTransferApiView.as_view(),
                           name='remove_transfer'),

                       url(regex=r'^transfer/booking/confirm$', view=PurchaseTransferApiView.as_view(),
                           name='purchase_transfer'),
                       url(regex=r'^ticket/destination$', view=SelectorTicketDestinationApiView.as_view(),
                           name='destination_ticket'),
                       url(regex=r'^ticket/search$', view=SearchTicketApiView.as_view(), name='search_ticket'),
                       url(regex=r'^ticket/booking/create$', view=BlockTicketApiView.as_view(), name='block_ticket'),
                       url(regex=r'^ticket/booking/confirm$', view=PurchaseTicketApiView.as_view(),
                           name='purchase_ticket')
                       )
