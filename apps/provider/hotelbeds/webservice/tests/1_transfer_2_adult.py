from provider.hotelbeds.webservice.transfer import Transfer

occupancy = [
    {
        'adult_count': 2
    }

]

transfer = {

    'dates': {
        'from': {
            'date': '20120313',
            'time': '1115',
        },
        'to': {
            'date': '20120315',
            'time': '1115',
        },

    },

    'from': {

        'type': 'TERMINAL',
        'code': 'XXX'

    },
    'to': {
        'type': 'HOTEL', # 'HOTEL , TERMINAL"
        'code': '94962',
        'transfer_zone':'41'
    },

}

a = Transfer("molamos_session", "molamos_tokemn", "CAS", transfer, occupancy)
