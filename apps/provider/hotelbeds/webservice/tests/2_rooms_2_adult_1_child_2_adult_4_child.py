from provider.hotelbeds.webservice.hotel import Hotel
room = [

    {
        'num_room': 1,
        'adult_count': 2,
        'children': {
            'child_count': 1,
            'age': ['5']
        }
    },

    {
        'num_room': 1,
        'adult_count': 2,
        'children': {
            'child_count': 4,
            'age': ['3', '4', '5', '6']
        }
    }

]


destination = {
    'code': 'PMI',
    'type': 'SIMPLE',
    'zone': '41'

}

a = Hotel()
data = a.hotel_avail('molamos', 'molamos', 'CAS', '20160102', '20160108', destination, room)

room = [


    {
        'num_room': 1,
        'adult_count': 2,
        'children': {
            'child_count': 1,
            'age': ['5']
        },
        'HotelRoom': [
            {
                'board': {
                    'code': 'XXX',
                    'type': 'SIMPLE',
                },
                'roomtype': {
                    'characteristic': 'XXX',
                    'code': 'XXX',
                    'type': 'SIMPLE',
                }
            }
        ]
    },

    {
        'num_room': 1,
        'adult_count': 2,
        'children': {
            'child_count': 4,
            'age': ['3', '4', '5', '6']
        },

        'HotelRoom': [
            {
                'board': {
                    'code': 'XXX',
                    'type': 'SIMPLE',
                },
                'roomtype': {
                    'characteristic': 'XXX',
                    'code': 'XXX',
                    'type': 'SIMPLE',
                }
            }
        ]
    }
]


data = a.hotel_service_add('CAS', 'molamos', '1EaCNW93PDm3VOnezSkzigao', 'REP-TODOS', '1', '20160102', '20160108',
                           '1803', 'PMI', 'SIMPLE', room)



