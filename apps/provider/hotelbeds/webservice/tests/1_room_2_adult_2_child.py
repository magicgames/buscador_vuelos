from provider.hotelbeds.webservice.hotel import Hotel

room = [


    {
        'num_rooms': 1,
        'adult_count': 2,
        'children': {
            'child_count': 2,
            'age': ['2', '3']
        }
    }

]

destination = {
    'code': 'PMI',
    'type': 'SIMPLE',
    'zone': '41'

}

a = Hotel()
data = a.hotel_avail('molamos', 'molamos', 'CAS', '20160102', '20160108', destination, room)

## SERVICE_ADD
room = [

    {
        'num_rooms': 1,
        'adult_count': 2,
        'children': {
            'child_count': 2,
            'age': ['2', '3']
        },
        'HotelRoom': [
            {
                'board': {
                    'code': 'XXX',
                    'type': 'SIMPLE',
                },
                'roomtype': {
                    'characteristic': 'XXX',
                    'code': 'XXX',
                    'type': 'SIMPLE',
                }
            }
        ]
    }

]


data = a.hotel_service_add('CAS', 'molamos', '1EaCNW93PDm3VOnezSkzigao', 'REP-TODOS', '1', '20160102', '20160108',
                           '1803', 'PMI', 'SIMPLE', room)