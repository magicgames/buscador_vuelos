from apps.provider.hotelbeds.webservice.ticket import *

occupancy = [
    {
        'num_ticket': 1,
        'adult_count': 2
    }

]

destination = {
    'code': 'PMI',
    'type': 'SIMPLE',
    'zone': '41'

}

a = Ticket()
data = a.ticket_avail("molamos_session", "molamos_tokemn", "CAS", "20150701", "20150707", destination, occupancy)

modality_token = data['Data']['Response'][0]['Info']['Price'][0]['Code']
ticket_code = data['Data']['Response'][0]['Info']['Basic']['Id']
avail_token = data['Data']['Response'][0]['Info']['Basic']['Token']

data2 = a.ticket_valuation("CAS", "molamos_tokemn", avail_token, "20150701", "20150701", modality_token, ticket_code,
                           occupancy)



