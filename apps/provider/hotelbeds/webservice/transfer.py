# coding=utf-8
import json, hashlib, xmltodict, actions

from django.core.cache import cache

from actions.utils import process_shopping_cart
from actions.request import Request_xml


class Transfer(Request_xml):
    xmlns = None
    xsi = None
    version = None
    root = None

    def __init__(self):
        self.xmlns = "http://www.hotelbeds.com/schemas/2005/06/messages"
        self.xsi = "http://www.w3.org/2001/XMLSchema-instance"
        self.version = "2013/12"
        self.username = "PREFERENTES149637"
        self.password = "PREFERENTES149637"
        self.url = "http://testapi.interface-xml.com/appservices/http/FrontendService"
        pass

    def transfer_avail(self, session_id, token, lang, transfer, occupancy):

        change_destination = False
        final_result = {}

        cache_key = hashlib.sha1(
            session_id + token + lang  + json.dumps(transfer, sort_keys=True) + json.dumps(
                occupancy, sort_keys=True)).hexdigest()

        try:
            data = cache.get(cache_key)
            if data is None:
                raise
        except BaseException:
            ticket_request = actions.TransferRequestAvailAction(self.xmlns, self.xsi, self.version, self.username,self.password, self.url)
            data = ticket_request.send_request(session_id, token, lang, from_date, to_date, transfer, occupancy)
            data = self.make_request(data)
            if int(data['TicketAvailRS']['@totalItems']) == 0:
                change_destination = True
                data = ticket_request.send_request(session_id, token, lang, from_date, to_date, transfer, occupancy)

            cache.set(cache_key, data, timeout=30 * 60)

        ticket_response = actions.TicketResponseAvailAction()
        result = ticket_response.process_response(session_id, token, lang, from_date, to_date, destination, data,
                                                  occupancy)

        final_result = {
            'Change_destination': change_destination,
            'Data': result
        }

        return final_result

        pass

    def transfer_valuation(self):
        pass

    def transfer_service_add(self):
        pass

    def transfer_service_remove(self):
        pass
