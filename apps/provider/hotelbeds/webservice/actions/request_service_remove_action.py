
# coding=utf-8
import requests
from lxml import etree
from django.conf import settings
from utils import credential


class RequestServiceRemoveAction:
    def __init__(self, xmlns, xsi, version, username, password, url):
        self.xmlns = xmlns
        self.xsi = xsi

        self.version = version
        self.username = username
        self.password = password
        self.url = url
        self.root = None
        pass

    def send_request(self, lang, token, purchase_token,code):
        schema_location = "http://www.hotelbeds.com/schemas/2005/06/messages ServiceRemoveRQ.xsd"
        self.root = etree.Element("{" + self.xmlns + "}ServiceRemoveRQ", version=self.version,
                                  attrib={"{" + self.xsi + "}schemaLocation": schema_location}, echoToken=token,
                                  purchaseToken=purchase_token,SPUI=code,
                                  nsmap={'xsi': self.xsi, None: self.xmlns})

        etree.SubElement(self.root, 'Language').text = lang
        credential(self.root, self.username, self.password)
        return etree.tostring(self.root)


