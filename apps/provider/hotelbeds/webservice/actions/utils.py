from lxml import etree


def credential(root, user, password):
    credentials = etree.SubElement(root, "Credentials")
    etree.SubElement(credentials, 'User').text = user
    etree.SubElement(credentials, 'Password').text = password


def default_options(root, lang, destination, num_items=999):
    etree.SubElement(root, "Language").text = lang
    etree.SubElement(root, "PaginationData", pageNumber="1", itemsPerPage=str(num_items))

    if 'Code' in destination:
        try:
            type_destination = destination['Type']
        except KeyError:
            type_destination = "SIMPLE"

        dest_ele = etree.SubElement(root, "Destination", code=destination['Code'], type=type_destination)
        if 'zone' in destination:
            zone = etree.SubElement(dest_ele, 'ZoneList')
            etree.SubElement(zone, 'Zone', type=type_destination, code=destination['Zone'])


def process_shopping_cart(data, service_type):
    try:
        ele = data['Purchase']['ServiceList']
        purchase_token = data['Purchase']['@purchaseToken']
        service_list = data['Purchase']['ServiceList']
        if not isinstance(service_list, list):
            x = service_list
            service_list = list()
            service_list.append(x)
        for services in service_list:
            info={}
            comments = []
            comment_list = services['Service']['ContractList']['Contract']['CommentList']
            if not isinstance(comment_list, list):
                x = comment_list
                comment_list = list()
                comment_list.append(x)

            for comment in comment_list:
                x = {
                    'Type': comment['Comment']['@type'],
                    'Description': comment['Comment']['#text']

                }
                comments.append(x)

            if 'HotelInfo' in services['Service']:
                cancelation = {}

                if 'CancellationPolicies' in services['Service']['AvailableRoom']['HotelRoom']:

                    x = services['Service']['AvailableRoom']['HotelRoom']['CancellationPolicies']
                    cancelation = {

                        'Price': x['CancellationPolicy']['@amount'],
                        'startDate': x['CancellationPolicy']['@dateFrom'],
                        'startTime': x['CancellationPolicy']['@time']

                    }

                info = {

                    'Name': services['Service']['HotelInfo']['Name'],
                    'RoomType': services['Service']['AvailableRoom']['HotelRoom']['RoomType'],
                    'BoardType': services['Service']['AvailableRoom']['HotelRoom']['Board'],
                    'CancelationPolicies': cancelation
                }

            x = {
                'ServiceToken': services['Service']['@SPUI'],
                'ServiceType': service_type,
                'Comments': comments,
                'Info': info
            }

            return x
    except KeyError as e:
        print e
        return {'ERROR': e.message}


def guest_occupation(occupation_list, **kwargs):

    kwargs = kwargs['kwargs']
    print kwargs
    try:
        adult_count = kwargs['adultCount']
    except KeyError as e:
        adult_count = 0

    try:
        child_count = kwargs['childCount']
    except KeyError as e:
        child_count = 0

    try:
        num_room = kwargs['numRoom']

    except KeyError as e:
        num_room = 0

    occupation = etree.SubElement(occupation_list, "HotelOccupancy")
    etree.SubElement(occupation, "RoomCount").text = str(num_room)
    occupation_room = etree.SubElement(occupation, "Occupancy")
    etree.SubElement(occupation_room, "AdultCount").text = str(adult_count)
    etree.SubElement(occupation_room, "ChildCount").text = str(child_count)

    if int(child_count) > 0:
        guest_list = etree.SubElement(occupation_room, "GuestList")
        for x in xrange(num_room):
            for child in kwargs['age']:
                customer = etree.SubElement(guest_list, "Customer", type="CH")
                etree.SubElement(customer, "Age").text = str(child)


def guest_list__occupation(occupation_list, **kwargs):
    kwargs = kwargs['kwargs']

    try:
        adult_count = kwargs['adultCount']
    except KeyError as e:
        adult_count = 0

    try:
        child_count = kwargs['Children']['Child_count']
    except KeyError as e:
        child_count = 0

    try:
        num_room = kwargs['numRoom']

    except KeyError as e:
        num_room = 0

    occupation_room = etree.SubElement(occupation_list, "ServiceOccupancy")
    etree.SubElement(occupation_room, "AdultCount").text = str(adult_count)
    etree.SubElement(occupation_room, "ChildCount").text = str(child_count)

    if int(child_count) > 0:
        guest_list = etree.SubElement(occupation_room, "GuestList")
        for x in xrange(num_room):
            for child in kwargs['Children']['Age']:
                customer = etree.SubElement(guest_list, "Customer", type="CH")
                etree.SubElement(customer, "Age").text = str(child)


def service_add(xmlns, xsi, version, token, lang, username, password, service_type, service_token, contract_name,
                contract_code, from_date, to_date):
    schema_location = "http://www.hotelbeds.com/schemas/2005/06/messages ServiceAddRQ.xsd"
    root = etree.Element("{" + xmlns + "}ServiceAddRQ", version=version,
                         attrib={"{" + xsi + "}schemaLocation": schema_location}, echoToken=token,
                         nsmap={'xsi': xsi, None: xmlns})

    etree.SubElement(root, 'Language').text = lang
    credential(root, username, password)
    service = etree.SubElement(root, 'Service',
                               attrib={"{" + xsi + "}type": service_type, "availToken": service_token})

    contract_list = etree.SubElement(service, 'ContractList')
    contract = etree.SubElement(contract_list, 'Contract')
    etree.SubElement(contract, "Name").text = contract_name
    etree.SubElement(contract, "IncomingOffice", code=contract_code)
    etree.SubElement(service, 'DateFrom', date=from_date)
    etree.SubElement(service, 'DateTo', date=to_date)

    return service, root
