# coding=utf-8
import requests
import collections
from lxml import etree
from django.conf import settings
from utils import credential, guest_occupation, default_options


class TransferRequestAvailAction:
    def __init__(self, xmlns, xsi, version, username, password, url):
        self.xmlns = xmlns
        self.xsi = xsi

        self.version = version
        self.username = username
        self.password = password
        self.url = url
        self.root = None
        pass

    def send_request(self, session_id, token, lang, transfer, occupancy):
        schema_location = "http://www.hotelbeds.com/schemas/2005/06/messages TransferValuedAvailRQ.xsd"
        self.root = etree.Element("{" + self.xmlns + "}TransferValuedAvailRQ", version=self.version,
                                  showDiscountsList="Y",
                                  attrib={"{" + self.xsi + "}schemaLocation": schema_location},
                                  echoToken=token,
                                  sessionId=session_id,
                                  nsmap={'xsi': self.xsi, None: self.xmlns})
        credential(self.root, self.username, self.password)
        etree.SubElement(self.root, "ReturnContents").text = "Y"

        pass

    def __avail_data(self):
        """
        <AvailData type="IN">
        <ServiceDate date="20140310" time="1000"/>
        <Occupancy>
            <AdultCount>2</AdultCount>
            <ChildCount>1</ChildCount>
            <GuestList>
                <Customer type="CH">
                    <Age>6</Age>
                </Customer>
            </GuestList>
        </Occupancy>
        <PickupLocation xsi:type="ProductTransferHotel">
            <Code>13388</Code>
        </PickupLocation>
        <DestinationLocation xsi:type="ProductTransferTerminal">
            <Code>JFK</Code>
            <DateTime date="20140310" time="1000"/>
        </DestinationLocation>
    </Av
    ailData>
        :return:
        """

        pass
