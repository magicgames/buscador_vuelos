# coding=utf-8
import requests
import collections
from lxml import etree

from django.conf import settings
from utils import credential, default_options,guest_list__occupation


class TicketRequestAvailAction:
    def __init__(self, xmlns, xsi, version, username, password, url):
        self.xmlns = xmlns
        self.xsi = xsi

        self.version = version
        self.username = username
        self.password = password
        self.url = url
        self.root = None
        pass

    def send_request(self, session_id, token, lang, from_date, to_date, destination,occupancy):
        assert isinstance(occupancy, collections.Iterable)
        """

        :param session_id:
        :param token:
        :param lang:
        :param from_date:
        :param to_date:
        :param destination:
        :return:
        """

        """
        <TicketAvailRQ echoToken="DummyEchoToken" sessionId="dkjf11sjg321fd5b3n"
        version="2013/12" xmlns="http://www.hotelbeds.com/schemas/2005/06/messages"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.hotelbeds.com/schemas/2005/06/messages TicketAvailRQ.xsd">
        """
        schema_location = "http://www.hotelbeds.com/schemas/2005/06/messages TicketAvailRQ.xsd"
        self.root = etree.Element("{" + self.xmlns + "}TicketAvailRQ", version=self.version,
                                  attrib={"{" + self.xsi + "}schemaLocation": schema_location}, echoToken=token,
                                  sessionId=session_id,
                                  nsmap={'xsi': self.xsi, None: self.xmlns})

        credential(self.root, self.username, self.password)
        default_options(self.root, lang, destination)

        etree.SubElement(self.root, "DateFrom", date=from_date)
        etree.SubElement(self.root, "DateTo", date=to_date)
        self.__ticket_occupancy(occupancy)

        return etree.tostring(self.root)

    def __ticket_occupancy(self,occupancy):
        occupation_list = etree.SubElement(self.root, 'ServiceOccupancy')
        for ticket in occupancy:
                guest_list__occupation(occupation_list, kwargs=ticket)


        pass


