# coding=utf-8
import requests
import collections
from lxml import etree
from django.conf import settings
from utils import credential, guest_occupation, default_options


class HotelRequestAvailAction:
    def __init__(self, xmlns, xsi, version, username, password, url):
        self.xmlns = xmlns
        self.xsi = xsi

        self.version = version
        self.username = username
        self.password = password
        self.url = url
        self.root = None
        pass

    def send_request(self, session_id, token, lang, from_date, to_date, destination, rooms,hotel_ids=None):
        assert isinstance(rooms, collections.Iterable)
        schema_location = "http://www.hotelbeds.com/schemas/2005/06/messages HotelValuedAvailRQ.xsd"
        self.root = etree.Element("{" + self.xmlns + "}HotelValuedAvailRQ", version=self.version,
                                  showDiscountsList="Y",
                                  attrib={"{" + self.xsi + "}schemaLocation": schema_location}, echoToken=token,
                                  sessionId=session_id,
                                  nsmap={'xsi': self.xsi, None: self.xmlns})

        credential(self.root, self.username, self.password)
        default_options(self.root, lang, destination)
        etree.SubElement(self.root, "CheckInDate", date=from_date)
        etree.SubElement(self.root, "CheckOutDate", date=to_date)
        etree.SubElement(self.root, "ShowCancellationPolicies").text = "Y"
        etree.SubElement(self.root, "ShowDirectPayment").text = "Y"

        extra_parameter_list = etree.SubElement(self.root, 'ExtraParamList')
        extended_data = etree.SubElement(extra_parameter_list, 'ExtendedData', type="EXT_DISPLAYER")
        etree.SubElement(extended_data, "Name").text = "DISPLAYER_DEFAULT"
        etree.SubElement(extended_data, "Value").text = "PROMOTION:Y"
        extra_parameter_list2 = etree.SubElement(self.root, 'ExtraParamList')
        extended_data = etree.SubElement(extra_parameter_list2, 'ExtendedData', type="EXT_ADDITIONAL_PARAM")
        etree.SubElement(extended_data, "Name").text = "PARAM_SHOW_OPAQUE_CONTRACT"
        etree.SubElement(extended_data, "Value").text = "Y"
        extra_parameter_list3 = etree.SubElement(self.root, 'ExtraParamList')
        extended_data = etree.SubElement(extra_parameter_list3, 'ExtendedData', type="EXT_ORDER")
        etree.SubElement(extended_data, "Name").text = "ORDER_CONTRACT_PRICE"
        etree.SubElement(extended_data, "Value").text = "DESC"

        self.__room_occupation_list(rooms)

        return etree.tostring(self.root)

    def __room_occupation_list(self, rooms):
        occupation_list = etree.SubElement(self.root, 'OccupancyList')
        for room in rooms:
            guest_occupation(occupation_list, kwargs=room)

    def __extra_filters(self, search_filter):
        pass