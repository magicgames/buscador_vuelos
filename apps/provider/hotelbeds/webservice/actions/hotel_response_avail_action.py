# coding=utf-8


class HotelResponseAvailAction:
    def process_response(self, token, data):
        if not self.check_token(data, token):
            return None
        if int(data['HotelValuedAvailRS']['@totalItems']) == 0:

            return {}

        hotels = data['HotelValuedAvailRS']['ServiceHotel']
        hotel_data = {}
        hotel_ids = []
        response = {}
        """
        Si no es una lista de "hoteles" -- Solo hay 1 el elemento se convierte en una colección.
        Como necesitamos que sea una lista vamos a mirar si es una lista de "Hoteles"
        """
        if not isinstance(hotels, list):
            x = hotels
            hotels = list()
            hotels.append(x)
        geo_pos = {}
        for hotel in hotels:
            hotel_info = hotel['HotelInfo']
            geo_pos = {
                'Latitude': hotel_info['Position']['@latitude'],
                'Longitude': hotel_info['Position']['@longitude']
            }
            basic_room = []
            hotel_rooms = hotel['AvailableRoom']
            if not isinstance(hotel_rooms, list):
                x = hotel_rooms
                hotel_rooms = list()
                hotel_rooms.append(x)

            for room in hotel_rooms:
                room_data = self.get_room_info(room)

                basic_room.append(room_data)
            images = []
            if 'ImageList' in hotel_info:
                for img in hotel_info['ImageList']['Image']:
                    url = img['Url']
                    x = {
                        'Order': img['Order'],
                        'Url': url.replace('small/', '')

                    }
                    images.append(x)

            hotel_result = {
                'Info': {
                    'Name': hotel_info['Name'],
                    'DirectPayment':hotel['DirectPayment'],
                    'Position': geo_pos,
                    'Category': hotel_info['Category']['@code'],
                    'Code': hotel_info['Code'],
                    'HotelToken': hotel['@availToken'],
                    'Room': basic_room,
                    'Images': images,
                },

                'Destination': {
                    'Code': hotel_info['Destination']['@code'],
                    'Type': hotel_info['Destination']['@type'],
                    'Name': hotel_info['Destination']['Name'],
                },
                'Contract': {
                    'Name': hotel['ContractList']['Contract']['Name'],
                    'Code': hotel['ContractList']['Contract']['IncomingOffice']['@code']
                },
                'ExtraInfo': {
                    'Currency': {
                        'Code': hotel['Currency']['@code'],
                        'Name': hotel['Currency']['#text'],
                    },
                    'DirectPayment': hotel['DirectPayment'],
                    'DateFrom': hotel['DateFrom']['@date'],
                    'DateTo': hotel['DateFrom']['@date'],
                },

            }

            hotel_data[int(hotel_info['Code'])] = hotel_result
        response = {
            'Response': hotel_data,
        }

        return response

    def get_room_info(self, data):
        hotel_room = data['HotelRoom']
        ocupancy = {
            'RoomCount': data['HotelOccupancy']['RoomCount'],
            'AdultCount': data['HotelOccupancy']['Occupancy']['AdultCount'],
            'ChildCount': data['HotelOccupancy']['Occupancy']['ChildCount'],
        }

        board_type = {
            'Code': hotel_room['Board']['@code'],
            'Description': hotel_room['Board']['#text'],
            'Type': hotel_room['Board']['@type'],

        }

        room_type = {
            'Code': hotel_room['RoomType']['@code'],
            'Description': hotel_room['RoomType']['#text'],
            'Characteristic': hotel_room['RoomType']['@characteristic'],
            'Type': hotel_room['RoomType']['@type'],
        }
        price = {
            'Amount': hotel_room['Price']['Amount'],
            'SellingPrice': {
                'Mandatory': hotel_room['Price']['SellingPrice']['@mandatory'],
                'Amount': hotel_room['Price']['SellingPrice']['#text'],
            },
            'NetPrice': hotel_room['Price']['NetPrice'],
            'Commission': hotel_room['Price']['Commission']
        }
        room = {
            'AvailableRoom': hotel_room['@availCount'],
            'OnDemand': hotel_room['@onRequest'],
            'BoardType': board_type,
            'Type': room_type,
            'Price': price,
            'Occupancy': ocupancy
        }
        return room

    def check_token(self, data, token):
        result = False
        try:
            if data['HotelValuedAvailRS']['@echoToken'] == token:
                result = True
        except KeyError:
            pass
        return result
