# coding=utf-8
import requests
from lxml import etree
from django.conf import settings
from utils import credential, guest_occupation, service_add
import collections


class TicketRequestServiceAddAction:
    def __init__(self, xmlns, xsi, version, username, password, url, service_type, product_type):
        self.xmlns = xmlns
        self.xsi = xsi

        self.version = version
        self.username = username
        self.password = password
        self.url = url
        self.root = None
        self.service_type = service_type
        self.product_type = product_type
        pass

    def send_request(self, lang, token, service_token, contract_name, contract_code, from_date, to_date, ticket_code,
                     currency, destination_code, destination_type, occupancy):
        assert isinstance(occupancy, collections.Iterable)

        service, root = service_add(self.xmlns, self.xsi, self.version, token, lang, self.username, self.password,
                                    self.service_type, service_token, contract_name, contract_code, from_date, to_date)

        etree.SubElement(service, 'Currency', code=currency)
        self.__service_add_ticket_info(service, self.product_type, ticket_code, destination_code, destination_type)

        return etree.tostring(root)


    def __service_add_ticket_info(self, service, product_type, hotel_id, destination_code, destination_type):
        hotel_info = etree.SubElement(service, 'TicketInfo', attrib={"{" + self.xsi + "}type": product_type})
        etree.SubElement(hotel_info, "Code").text = hotel_id
        etree.SubElement(hotel_info, "Destination", code=destination_code, type=destination_type)