# coding=utf-8
import requests
from lxml import etree
from django.conf import settings
from utils import credential, guest_occupation, service_add
import collections


class HotelRequestServiceAddAction:
    def __init__(self, xmlns, xsi, version, username, password, url, service_type, product_type):
        self.xmlns = xmlns
        self.xsi = xsi

        self.version = version
        self.username = username
        self.password = password
        self.url = url
        self.root = None
        self.service_type = service_type
        self.product_type = product_type
        pass

    def send_request(self, lang, token, service_token, contract_name, contract_code, from_date, to_date, hotel_id,
                     destination_code, destination_type, rooms):
        assert isinstance(rooms, collections.Iterable)

        service, root = service_add(self.xmlns, self.xsi, self.version, token, lang, self.username, self.password,
                                    self.service_type, service_token, contract_name, contract_code, from_date, to_date)

        self._service_add_hotel_info(service, self.product_type, hotel_id, destination_code, destination_type)

        for room in rooms:
            available_room = self.__service_add_available_room_info(service, room)
            self.__service_add_room_info(available_room, room)

        return etree.tostring(root)

    def _service_add_hotel_info(self, service, product_type, hotel_id, destination_code, destination_type):
        hotel_info = etree.SubElement(service, 'HotelInfo', attrib={"{" + self.xsi + "}type": product_type})
        etree.SubElement(hotel_info, "Code").text = hotel_id
        etree.SubElement(hotel_info, "Destination", code=destination_code, type=destination_type)

    def __service_add_available_room_info(self, service, room):
        available_room = etree.SubElement(service, "AvailableRoom")
        guest_occupation(available_room, kwargs=room)
        return available_room

    def __service_add_room_info(self, available_room, hotel_room):
        room_infos = hotel_room['hotelRoom']
        for room_info in room_infos:
            hotel_room = etree.SubElement(available_room, "HotelRoom")
            etree.SubElement(hotel_room, "Board", code=room_info['board']['code'], type=room_info['board']['type'])
            etree.SubElement(hotel_room, "RoomType", code=room_info['roomType']['code'],
                             characteristic=room_info['roomType']['characteristic'],
                             type=room_info['roomType']['type'])


