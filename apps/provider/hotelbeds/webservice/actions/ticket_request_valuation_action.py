# coding=utf-8
import requests
import collections
from lxml import etree
from django.conf import settings
from utils import credential, default_options, guest_list__occupation


class TicketRequestValuationAction:
    def __init__(self, xmlns, xsi, version, username, password, url):
        self.xmlns = xmlns
        self.xsi = xsi

        self.version = version
        self.username = username
        self.password = password
        self.url = url
        self.root = None
        pass

    def send_request(self, lang, token, avail_token, from_date, to_date, modality_code, ticket_code, occupancy):
        assert isinstance(occupancy, collections.Iterable)
        schema_location = "http://www.hotelbeds.com/schemas/2005/06/messages TicketValuationRQ.xsd"
        self.root = etree.Element("{" + self.xmlns + "}TicketValuationRQ", version=self.version,
                                  attrib={"{" + self.xsi + "}schemaLocation": schema_location}, echoToken=token,
                                  nsmap={'xsi': self.xsi, None: self.xmlns})

        credential(self.root, self.username, self.password)
        etree.SubElement(self.root, 'Language').text = lang
        etree.SubElement(self.root, 'AvailToken').text = avail_token
        etree.SubElement(self.root, 'TicketCode').text = ticket_code
        etree.SubElement(self.root, 'ModalityCode').text = modality_code
        etree.SubElement(self.root, "DateFrom", date=from_date)
        etree.SubElement(self.root, "DateTo", date=to_date)
        self.__ticket_occupancy(occupancy)

        return etree.tostring(self.root)

    def __ticket_occupancy(self,occupancy):
        occupation_list = etree.SubElement(self.root, 'ServiceOccupancy')
        for ticket in occupancy:
                guest_list__occupation(occupation_list, kwargs=ticket)


        pass



