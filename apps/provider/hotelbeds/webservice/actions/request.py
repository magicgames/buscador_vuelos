# coding=utf-8
import requests
import xmltodict
import logging
logger = logging.getLogger('hotelbeds')


class Request_xml(object):
    def make_request(self, data):
        additional_headers = dict()
        additional_headers['content-encoding'] = 'gzip'
        result = {}
        try:
            logger.debug(data)
            r = requests.post("http://testapi.interface-xml.com/appservices/http/FrontendService",
                              data={'xml_request': data}, headers=additional_headers)
            logger.debug(r.text)
            result = xmltodict.parse(r.text)


        except requests.RequestException as e:
            logger.error(e)
            pass

        return result
