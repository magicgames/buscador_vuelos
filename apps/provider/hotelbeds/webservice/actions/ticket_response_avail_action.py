# coding=utf-8
import json
from django.conf import settings


class TicketResponseAvailAction:
    def process_response(self, session_id, token, lang, from_date, to_date, destination_code, data, occupancy):
        if not self.check_token(data, token):
            """
            Si no coincide el token con lo que nos envian
            Así nos aseguramos recibir los datos correctos
            """
            return None

        tickets = data['TicketAvailRS']['ServiceTicket']

        ticket_data = []
        response = {}
        """
        Si no es una lista de "Tickets" -- Solo hay 1 el elemento se convierte en una colección.
        Como necesitamos que sea una lista vamos a mirar si es una lista de "Hoteles"
        """
        if not isinstance(tickets, list):
            x = tickets
            tickets = list()
            tickets.append(x)

        for ticket in tickets:

            basic_info = self.get_basic_info(ticket)
            price_info = []
            ticket_available_mod = ticket['AvailableModality']
            if not isinstance(ticket_available_mod, list):
                x = ticket_available_mod
                ticket_available_mod = list()
                ticket_available_mod.append(x)

            for mod in ticket_available_mod:
                room_data = self.get_mod_info(mod)
                price_info.append(room_data)
            hotel_result = {
                'Id': ticket['TicketInfo']['Code'],
                'Info': {
                    'Basic': basic_info,
                    'Price': price_info
                }

            }

            ticket_data.append(hotel_result)

            response = {
                'Request': {
                    'From': from_date,
                    'To': to_date,
                    'Destination': destination_code,
                    'Session': session_id,
                    'Token': token,
                    'Lang': lang,

                    'Occupancy': occupancy
                },


                'Response': ticket_data,


            }

        return response

    def get_basic_info(self, data):
        imagelist = []
        ticket_info = data['TicketInfo']

        # images
        images = ticket_info['ImageList']

        images = images['Image']
        if not isinstance(images, list):
            x = images
            images = list()
            images.append(x)
        for image in images:
            image = {
                'type': image['Type'],
                'order': image['Order'],
                'visual_order': image['VisualizationOrder'],
                'url': image['Url']
            }
            imagelist.append(image)

        basic_info = {

            'Name': ticket_info['Name'],
            'Provider': 'hotel_beds',
            'Id': ticket_info['Code'],
            'Token': data['@availToken'],
            'Description': ticket_info['DescriptionList']['Description']['#text'],
            'Images': imagelist,
            'Destination': {

                'Code': ticket_info['Destination']['@code'],
                'Type': ticket_info['Destination']['@type'],
            },

        }
        return basic_info


    def get_mod_info(self, data):
        price_list = data['PriceList']
        date_list = []
        date_range = data['OperationDateList']
        price = []
        price_list = price_list['Price']
        if not isinstance(price_list, list):
            x = price_list
            price_list = list()
            price_list.append(x)
        for price_ele in price_list:
            ele = {
                'Amount': price_ele['Amount'],
                'Description': price_ele['Description']
            }
            price.append(ele)
        elements = date_range['OperationDate']
        if not isinstance(elements, list):
            x = elements
            elements = list()
            elements.append(x)
        for date_day_row in elements:
            try:
                range_row = {
                    'Day': date_day_row['@date'],
                    'Min_duration': date_day_row['@minimumDuration'],
                    'Max_duration': date_day_row['@maximumDuration'],

                }
                date_list.append(range_row)
            except BaseException as e:
                print date_day_row

        available = {
            'Code': data['@code'],
            'Name': data['Name'],
            'Contract': {
                'Name': data['Contract']['Name'],
                'Code': data['Contract']['IncomingOffice']['@code']

            },
            'Type': data['Type']['@code'],
            'Mode': data['Mode']['@code'],
            'Price_list': price,
            'date': date_list,
            'Child_age': {
                'From': data['ChildAge']['@ageFrom'],
                'To': data['ChildAge']['@ageTo'],

            }

        }

        return available

    def check_token(self, data, token):
        result = False
        try:
            if data['TicketAvailRS']['@echoToken'] == token:
                result = True
        except KeyError as e:
            pass
        return result