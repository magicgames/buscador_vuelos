# coding=utf-8
import json
from django.conf import settings


class TicketResponseValuationAction:

    def process_response(self, token, data):
        if not self.check_token(data, token):
            """
            Si no coincide el token con lo que nos envian
            Así nos aseguramos recibir los datos correctos
            """
            return None

        tickets = data['TicketValuationRS']['ServiceTicket']
        comment_list = tickets['CommentList']
        ticket_info = tickets['TicketInfo']
        pax = tickets['Paxes']
        guest_list = pax['GuestList']['Customer']
        additional_cost_elem = []
        service_detail_elem = []
        cancellation_policy_elem = []
        available_modality_elem = []

        guest_elem = []

        for guest in guest_list:
            x = {
                'Type': guest['@type'],
                'Age': guest['Age'],
            }
            guest_elem.append(x)

        try:
            cancellation_policy_list = tickets['CancellationPolicyList']
            cancellation_policy_list = cancellation_policy_list['Price']
            if not isinstance(cancellation_policy_list, list):
                x = cancellation_policy_list
                cancellation_policy_list = list()
                cancellation_policy_list.append(x)
            for cancellation_policy in cancellation_policy_list:
                x = {

                    'Amount': cancellation_policy['Amount'],
                    'Date_from': cancellation_policy['DateTimeFrom']['@date'],
                    'Date_to': cancellation_policy['DateTimeTo']['@date']
                }

                cancellation_policy_elem.append(x)
        except KeyError:
            pass

        try:
            additional_cost_list = tickets['AdditionalCostList']['AdditionalCost']
            if not isinstance(additional_cost_list, list):
                x = additional_cost_list
                additional_cost_list = list()
                additional_cost_list.append(x)

            for additional_cost in additional_cost_list:
                x = {
                    'Type': additional_cost['@type'],
                    'Amount': additional_cost['Price']['Amount']
                }
                additional_cost_elem.append(x)

        except KeyError:
            pass

        try:
            service_detail_list = tickets['ServiceDetailList']
            if not isinstance(service_detail_list, list):
                x = service_detail_list
                service_detail_list = list()
                service_detail_list.append(x)

            for service_detail in service_detail_list:
                x = {
                    'Code': service_detail['ServiceDetail']['@code'],
                    'Name': service_detail['ServiceDetail']['Name']
                }

                service_detail_elem.append(x)
        except KeyError as e:
            print "entro aqui por seviceDetail"
            print e
            pass

        try:
            available_modality_list = tickets['AvailableModality']
            if not isinstance(available_modality_list, list):
                x = available_modality_list
                available_modality_list = list()
                available_modality_list.append(x)

            for available_modality in available_modality_list:
                x = {
                    'Code': available_modality['@code'],
                    'Name': available_modality['Name'],
                    'Contract': {
                        'Name': available_modality['Contract']['Name'],
                        'Code': available_modality['Contract']['IncomingOffice']['@code'],
                    }
                }

                available_modality_elem.append(x)
        except KeyError:
            pass

        result = {
            'Spui': tickets['@SPUI'],

            'Contract_list': {
                'Name': tickets['ContractList']['Contract']['Name'],
                'Code': tickets['ContractList']['Contract']['IncomingOffice']['@code'],

            },
            'Supplier': {
                'Name': tickets['Supplier']['@name'],
                'Vatnumber': tickets['Supplier']['@vatNumber']

            },
            'Comment_list': {
                'Type': comment_list['Comment']['@type'],
                'Comment': comment_list['Comment']['#text']

            },
            'Service_detail': service_detail_elem,
            'Additional_cost_list': additional_cost_elem,
            'Cancellation_policy': cancellation_policy_elem,
            'Ticket_info': {
                'Code': ticket_info['Code'],
                'Name': ticket_info['Name'],
                'Company_code': ticket_info['CompanyCode'],
                'Ticket_class': ticket_info['TicketClass'],
                'Destination': {
                    'Type': ticket_info['Destination']['@type'],
                    'Code': ticket_info['Destination']['@code'],
                },
                'Date_from': tickets['DateFrom']['@date'],
                'Date_To': tickets['DateTo']['@date'],
            },
            'Available_modality': available_modality_elem,
            'Paxes': {
                'Adult_count': pax['AdultCount'],
                'Child_count': pax['ChildCount'],
                'Guest_list': guest_elem,

            }
        }
        return result

    def check_token(self, data, token):
        result = False
        try:
            if data['TicketValuationRS']['@echoToken'] == token:
                result = True
        except KeyError as e:
            pass
        return result