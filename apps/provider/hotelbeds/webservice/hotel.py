# coding=utf-8
import json
import hashlib
import actions
from django.core.cache import cache
from django.http import HttpResponseBadRequest, HttpResponse

from provider.hotelbeds.models import HotelDescription
from actions.utils import process_shopping_cart
from actions.request import Request_xml


class Hotel(Request_xml):
    xmlns = None
    xsi = None
    version = None
    root = None

    def __init__(self):
        self.xmlns = "http://www.hotelbeds.com/schemas/2005/06/messages"
        self.xsi = "http://www.w3.org/2001/XMLSchema-instance"

        self.version = "2013/12"
        self.username = "PREFERENTES149637"
        self.password = "PREFERENTES149637"
        self.url = "http://testapi.interface-xml.com/appservices/http/FrontendService"

        pass

    def hotel_avail(self, session_id, token, lang, from_date, to_date, destination, rooms):
        change_destination = False
        cache_key = hashlib.sha1(
            session_id + token + lang + from_date + to_date + json.dumps(destination, sort_keys=True) + json.dumps(
                rooms, sort_keys=True)) \
            .hexdigest()

        try:
            data = cache.get(cache_key)
            if data is None:
                raise
        except:
            hotel_request = actions.HotelRequestAvailAction(self.xmlns, self.xsi, self.version, self.username,
                                                            self.password,
                                                            self.url)
            data = hotel_request.send_request(session_id, token, lang, from_date, to_date, destination, rooms)
            data = self.make_request(data)
            # TODO: gestion de errores común para todos los procesos.
            if 'ErrorList' in data['HotelValuedAvailRS']:
                error_list = []
                errors = data['HotelValuedAvailRS']['ErrorList']['Error']
                if not isinstance(errors, list):
                    x = errors
                    errors = list()
                    errors.append(x)

                for error in errors:
                    x = {

                        'Code': error['Code'],
                        'Description': error['DetailedMessage']

                    }
                    error_list.append(x)

                return {

                    'Error': error_list,
                    'Status': 'KO',
                    'SecurityToken': token
                }

            if int(data['HotelValuedAvailRS']['@totalItems']) == 0:
                change_destination = True
                destination.pop('zone', None)
                data = hotel_request.send_request(session_id, token, lang, from_date, to_date, destination, rooms)
                data = self.make_request(data)

            cache.set(cache_key, data, timeout=30 * 60)

        result = actions.HotelResponseAvailAction().process_resretuponse(token, data)
        if len(result) == 0:
            final_result = {

                'ChangeDestination': change_destination,
                'Response': {},
                'TotalItem': 0,
                'Status': 'OK',
                'SecurityToken': token
            }
            return final_result

        hotel_keys = result['Response'].keys()

        descriptions = HotelDescription.objects.filter(hotel__id__in=hotel_keys, language__code=lang). \
            select_related('hotel').values('hotel', 'description', 'hotel__avg_review', 'hotel__num_review')

        for description in descriptions:
            review = {
                'NumReview': description['hotel__num_review'],
                'AvgReview': description['hotel__avg_review']

            }
            result['Response'][description['hotel']]['Info']['Description'] = description['description']
            result['Response'][description['hotel']]['Info']['Review'] = review
        final_result = {

            'ChangeDestination': change_destination,
            'Response': result['Response'],
            'TotalItem': data['HotelValuedAvailRS']['@totalItems'],
            'TimeToExpire': data['HotelValuedAvailRS']['@timeToExpiration'],
            'Status': 'OK',
            'SecurityToken': token
        }
        return final_result

    def hotel_service_add(self, lang, token, service_token, contract_name, contract_code, from_date, to_date, hotel_id,
                          destination_code, destination_type, rooms):
        service_type = "ServiceHotel"
        product_type = "ProductHotel"
        service_request = actions.HotelRequestServiceAddAction(self.xmlns, self.xsi, self.version, self.username,
                                                               self.password, self.url, service_type, product_type)

        data = service_request.send_request(lang, token, service_token, contract_name, contract_code, from_date,
                                            to_date, hotel_id, destination_code, destination_type, rooms)
        data = self.make_request(data)

        data = data['ServiceAddRS']

        if 'ErrorList' in data:
            error_list = []
            errors = data['ErrorList']['Error']
            if not isinstance(errors, list):
                x = errors
                errors = list()
                errors.append(x)

            for error in errors:
                x = {

                    'Code': error['Code'],
                    'Description': error['DetailedMessage']

                }
                error_list.append(x)

            return {

                'Error': error_list,
                'Status': 'KO',
                'SecurityToken': token
            }

        data = self.process_data(data, service_type)
        return data

    def hotel_service_remove(self, lang, token, purchase_token, code):
        service_remove_request = actions.RequestServiceRemoveAction(self.xmlns, self.xsi, self.version, self.username,
                                                                    self.password, self.url)
        data = service_remove_request.send_request(lang, token, purchase_token, code)
        data = self.make_request(data)

        data = data['ServiceRemoveRS']
        data = self.process_data(data)
        return data

    def process_data(self, data, service_type):
        data = process_shopping_cart(data, service_type)
        return data
