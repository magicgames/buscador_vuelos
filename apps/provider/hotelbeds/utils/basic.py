# coding=utf-8
import datetime
from provider.models import AccountHotelBeds
from django.conf import settings


def generate_remove_hotel_request(d):
    lang = d.get("lang", "CAS")
    reason = ''
    request = {}
    try:
        service_token = d['serviceToken']
        purchase_token = d['purchaseToken']
        request = {
            "serviceToken": service_token,
            "purchaseToken": purchase_token,
            "lang": lang,
        }
    except BaseException as e:
        reason = 'Faltan parámetros'

    return {'request': request, 'reason': reason}


def generate_cancel_hotel_request(d):
    lang = d.get("lang", "CAS")
    reason = ''
    request = {}
    try:
        file_number = d['fileNumber']
        code = d['code']
        security_token = d['securityToken']
        request = {
            "fileNumber": file_number,
            "code": code,
            "lang": lang,
        }
    except BaseException as e:
        reason = 'Faltan parámetros'

    return {'request': request, 'reason': reason}


def generate_block_hotel_request(d):
    reason = ''
    request = {}
    room = []
    lang = d.get("lang", "CAS")
    service_token = d.get("token")
    from_date = datetime.datetime.strptime(d["fromDate"], "%Y-%m-%d").strftime("%Y%m%d")
    to_date = datetime.datetime.strptime(d["toDate"], "%Y-%m-%d").strftime("%Y%m%d")
    hotel_id = d.get("id")

    try:
        contract_name = d["contract"]["name"]
        contract_code = d["contract"]["code"]
        destination_code = d["destination"]["code"]
        destination_type = d["destination"]["type"]
        i = 0
        for selected in d["selectedRoom"]:
            search_room = d["searchRoom"][i]
            x = {
                "numRoom": search_room["numRoom"],
                "adultCount": search_room["adultCount"],
                "hotelRoom": {
                    "board": {
                        "code": selected["boardType"]["code"],
                        "type": selected["boardType"]["type"]
                    },
                    "roomType": {
                        "code": selected["type"]["code"],
                        "type": selected["type"]["type"],
                        "characteristic": selected["type"]["characteristic"],
                    }
                }
            }

            if ("childCount" in search_room) and (int(search_room["childCount"]) > 0):
                x["childCount"] = search_room["childCount"]
                x["age"] = search_room["age"]
            else:
                x["childCount"] = 0
            room.append(x)
            i += 1
        request = {
            "serviceToken": service_token,
            "fromDate": from_date,
            "toDate": to_date,
            "lang": lang,
            "hotelId": hotel_id,
            "contractName": contract_name,
            "contractCode": contract_code,
            "destinationCode": destination_code,
            "destinationType": destination_type,
            "rooms": room
        }
    except BaseException as e:
        reason = 'Faltan parámetros'
    return {
        'request': request, 'reason': reason
    }


def generate_puchase_hotel_request(d):
    lang = d.get("lang", "CAS")
    reason = ''
    request = {}
    try:
        purchase_token = d['purchaseToken']
        service_token = d['serviceToken']
        holder = d['holder']
        guest = d['guest']
        transaction_reference = d['transactionReference']
        comment = d.get('comment')
        request = {
            "serviceToken": service_token,
            "purchaseToken": purchase_token,
            "holder": holder,
            "guest": guest,
            "comment": comment,
            "lang": lang,
            "transactionReference": transaction_reference
        }
    except KeyError as e:
        reason = "Faltan parámetros"

    return {
        'request': request, 'reason': reason

    }


def generate_search_hotel_request(d):
    reason = ''
    hotel_id = []
    lang = d.get("lang", "CAS")

    try:
        rooms = d['room']
        destination = d['destination']

        if 'hotelId' in destination:
            if isinstance(destination['hotelId'], list):
                for id in destination['hotelId']:
                    try:
                        id = int(id)
                        if id != 0:
                            hotel_id.append(str(id))
                    except BaseException as e:
                        pass
            else:
                try:
                    id = int(destination['hotelId'])
                    hotel_id.append(str(id))
                except BaseException as e:
                    pass
            if len(hotel_id) > 0:
                destination['hotelId'] = hotel_id
            else:
                del destination['hotelId']

        from_date = datetime.datetime.strptime(d['fromDate'], '%Y-%m-%d').strftime('%Y%m%d')
        to_date = datetime.datetime.strptime(d['toDate'], '%Y-%m-%d').strftime('%Y%m%d')
        session = d['session']
        request = {

            'lang': lang,
            'fromDate': from_date,
            'toDate': to_date,
            'destination': destination,
            'rooms': rooms,
            'session': session
        }
    except BaseException as e:

        request = {}
        reason = "Faltan parametros " + e

    return {
        'request': request,
        'reason': reason,

    }


def generate_remove_transfer_request(d):
    reason = ''
    lang = d.get("lang", "CAS")
    request = {}
    # GENERAMOS IN TRANSFER

    if 'in' in d:
        try:
            d['in']['serviceToken'] = d['in']['serviceToken']
            d['in']['purchaseToken'] =d['in']['purchaseToken']
            d['in']['lang']=lang
            request['in']=d['in']

        except BaseException as e:
            reason = e.message + " En IN "
            pass

    if 'out' in d:
        try:
            d['out']['serviceToken'] = d['out']['serviceToken']
            d['out']['purchaseToken'] =d['out']['purchaseToken']
            d['out']['lang']=lang
            request['out']=d['out']

        except BaseException as e:
            reason = e.message + " En OUT "
            pass


    return {
        'request': request,
        'reason': reason,

    }


def generate_search_transfer_request(d):
    reason = ''
    from_date = datetime.datetime.strptime(d['fromDate'], '%Y-%m-%d').strftime('%Y%m%d')
    to_date = datetime.datetime.strptime(d['toDate'], '%Y-%m-%d').strftime('%Y%m%d')
    from_time = datetime.datetime.strptime(d['fromTime'], '%H%M%S').strftime('%H%M')
    to_time = datetime.datetime.strptime(d['toTime'], '%H%M%S').strftime('%H%M')
    destination = d['destination']
    occupation = d['occupation']
    occupation['numElem'] = 1
    lang = d.get("lang", "CAS")

    session = d['session']
    request = {
        'lang': lang,
        'fromDate': from_date,
        'toDate': to_date,
        'fromTime': from_time,
        'toTime': to_time,
        'destination': destination,
        'occupation': occupation,
        'session': session
    }

    return {
        'request': request,
        'reason': reason,

    }

    pass


def generate_booking_transfer_request(d):
    reason = ''
    lang = d.get("lang", "CAS")
    request = {}
    # GENERAMOS IN TRANSFER

    try:

        code_in = d['in']['pickupLocation']
        destination_in = d['in']['destination']
        d['in']['pickupLocation'] = {
            'code': code_in,
            'type': 'ProductTransferTerminal'

        }
        d['in']['destination'] = {
            'code': destination_in,
            'type': 'ProductTransferHotel'

        }

        d['in']['date'] = datetime.datetime.strptime(d['date']['in']['date'], '%Y-%m-%d').strftime('%Y%m%d')
        d['in']['time'] = datetime.datetime.strptime(d['date']['in']['time'], '%H%M%S').strftime('%H%M')
        d['in']['occupation'] = d['occupation']
        d['in']['lang'] = lang
    except:
        pass

    try:

        # GENERAMOS OUT TRANSFER
        code_out = d['out']['pickupLocation']
        destination_out = d['out']['destination']
        d['out']['pickupLocation'] = {
            'code': code_out,
            'type': 'ProductTransferHotel'

        }
        d['out']['destination'] = {
            'code': destination_out,
            'type': 'ProductTransferTerminal'

        }
        d['out']['date'] = datetime.datetime.strptime(d['date']['out']['date'], '%Y-%m-%d').strftime('%Y%m%d')
        d['out']['time'] = datetime.datetime.strptime(d['date']['out']['time'], '%H%M%S').strftime('%H%M')
        d['out']['occupation'] = d['occupation']
        d['out']['lang'] = lang
    except:
        pass
    if 'in' in d:
        request['in'] = d['in']
    if 'out' in d:
        request['out'] = d['out']

    return {
        'request': request,
        'reason': reason,

    }


def generate_puchase_transfer_request(d):
    lang = d.get("lang", "CAS")
    reason = ''
    request = {}

    try:
        purchase_token = d['blockTransfer']['in']['purchaseToken']
    except:
        purchase_token = d['blockTransfer']['out']['purchaseToken']

    try:
        holder = d['holder']
        guest = d['guest']
        transaction_reference = d['transactionReference']
        block_object = d['blockTransfer']
        comment = d.get('comment')
    except:
        reason = "Faltan parámetros"

    if reason == '':

        try:

            block_object['in']['date'] = datetime.datetime.strptime(block_object['in']['date'], '%Y-%m-%d').strftime(
                '%Y%m%d')
            block_object['in']['time'] = datetime.datetime.strptime(block_object['in']['time'], '%H%M%S').strftime(
                '%H%M')
        except:
            pass
        try:
            block_object['out']['date'] = datetime.datetime.strptime(block_object['out']['date'], '%Y-%m-%d').strftime(
                '%Y%m%d')
            block_object['out']['time'] = datetime.datetime.strptime(block_object['out']['time'], '%H%M%S').strftime(
                '%H%M')
        except:
            pass

        request = {
            "blockObject": block_object,
            "purchaseToken": purchase_token,
            "holder": holder,
            "guest": guest,
            "comment": comment,
            "lang": lang,
            "transactionReference": transaction_reference
        }

    return {
        'request': request, 'reason': reason

    }


# Generate Common

def generate_default_value(request, d, user):
    if user.is_authenticated():

        try:
            provider = AccountHotelBeds.objects.get(user=user)

            if settings.HOTELBEDS_TEST_MODE:
                username = provider.provider.user_test
                password = provider.provider.pass_test
            else:
                username = provider.provider.user_production
                password = provider.provider.pass_production

            security_token = d.get("securityToken")
            request['request']['username'] = username
            request['request']['password'] = password
            request['request']['securityToken'] = security_token
        except BaseException as e:

            print e
            pass

    return request
