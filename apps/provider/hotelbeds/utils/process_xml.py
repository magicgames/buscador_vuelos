# coding=utf-8


def get_basic_info(data):
    hotel_info = data['HotelInfo']
    geo_pos = {
        'Latitude': hotel_info['Position']['@latitude'],
        'Longitude': hotel_info['Position']['@longitude']
    }
    basic_info = {
        'Contract': {
            'Name': data['ContractList']['Contract']['Name'],
            'Code': data['ContractList']['Contract']['IncomingOffice']['@code']
        },
        'Name': hotel_info['Name'],
        'Provider': 'hotel_beds',
        'Id': hotel_info['Code'],
        'Token': data['@availToken'],
        'Category': hotel_info['Category']['@code'],
        'destination': {

            'code': hotel_info['Destination']['@code'],
            'type': hotel_info['Destination']['@type'],
        },
        'Position': geo_pos,
    }
    return basic_info


def get_room_info(data):
    hotel_room = data['HotelRoom']
    occupation = {
        'Room_count': data['HotelOccupancy']['RoomCount'],
        'Adult_count': data['HotelOccupancy']['Occupancy']['AdultCount'],
        'Children_count': data['HotelOccupancy']['Occupancy']['ChildCount']
    }
    board_type = {
        'Code': hotel_room['Board']['@code'],
        'Description': hotel_room['Board']['#text']
    }
    room_type = {
        'Code': hotel_room['RoomType']['@code'],
        'Description': hotel_room['RoomType']['#text'],
        'Characteristic': hotel_room['RoomType']['@characteristic'],
        'Type': hotel_room['RoomType']['@code'],
    }
    selling_price = {
        'Mandatory': hotel_room['Price']['SellingPrice']['@mandatory'],
        'Amount': hotel_room['Price']['SellingPrice']['#text'],
    }
    price = {
        'Amount': hotel_room['Price']['Amount'],
        'Selling_price': selling_price,
        'Net_Price': hotel_room['Price']['NetPrice'],
        'Commission': hotel_room['Price']['Commission']
    }
    room = {
        'Token': hotel_room['@SHRUI'],
        'Free_rooms': hotel_room['@availCount'],
        'On_demand': hotel_room['@onRequest'],
        'Board_type': board_type,
        'Type': room_type,
        'Price': price,
        'Occupation': occupation
    }
    return room