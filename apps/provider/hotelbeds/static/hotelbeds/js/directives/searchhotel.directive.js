(function () {
    'use strict';
    /* @ngInject */
    function resultadohotel() {

        var directive = {
            restrict: 'A',
            scope: {
                hotel: '=',
            },
            templateUrl: '/static/hotelbeds/hotel_list.html',
            link: link,
            controller: HotelListController,
            controllerAs: 'vm',
            bindToController: true
        };
        return directive;

        function link( scope, element, attributes ) {
            scope.$on(
                "$destroy",
                function handleDestroyEvent() {
                    console.log('Eliminado directiva');
                }
            );
        }
    }

    HotelListController.$inject = ['HotelBooking', 'HotelShared', '$location'];

    function HotelListController(HotelBooking, HotelShared, $location) {

        var vm = this;
        vm.room = [];
        vm.roomChecked = [];
        vm.num_room = 0;
        vm.blockHotel = blockHotel;
        vm.roomKeys = roomKeys;

        activate();

        /**
         *
         */
        function activate() {
            vm.objSearch = HotelShared.getSearchObject();
        }

        /**
         * @desc block hotel to start reservation
         * @name blockHotel
         * @param hotel
         */
        function blockHotel(hotel) {
            var rooms, masterrooms;
            masterrooms = vm.room[hotel.info.code];
            rooms = [];
            for (var room in masterrooms) {
                rooms.push(masterrooms[room]);
            }
            HotelShared.setDirectPayment(hotel.info.directPayment);

                var obj = {
                searchRoom: vm.objSearch.room,
                session: vm.objSearch.session,
                token: hotel.info.hotelToken,
                contract: hotel.info.extraInfo.contract,
                id: hotel.info.code,
                securityToken: vm.objSearch.securityToken,
                destination: hotel.info.extraInfo.destination,
                selectedRoom: rooms,
                fromDate: vm.objSearch.fromDate,
                toDate: vm.objSearch.toDate
            };

            HotelBooking.createBooking(obj).then(blockHotelSuccessFn, blockHotelErrorFn)
        }

        /**
         * @desc Function to store the rooms
         * @param index
         * @param room
         * @param hotelId
         */
        function roomKeys(index, room, hotelId) {

            var ocupancy = room.occupancy;
            var hash = ocupancy.roomCount + '_' + ocupancy.adultCount + '_' + ocupancy.childCount;
            if (vm.roomChecked[index]) {
                if (vm.num_room + parseInt(room.occupancy.roomCount) > parseInt(vm.objSearch.numRooms)) {

                    alert("No puedes seleccionar mas habitaciones de las que has buscado");
                    delete vm.roomChecked[index];
                } else {
                    vm.num_room = vm.num_room + parseInt(room.occupancy.roomCount);
                    if (!(vm.room.hasOwnProperty(hotelId))) {
                        vm.room[hotelId] = []
                    }
                    ;
                    vm.room[hotelId][hash] = room;
                }

            } else {
                vm.num_room = vm.num_room - parseInt(room.occupancy.roomCount);
                delete vm.room[hash]
            }
        }


        // CALLBACKS
        /**
         * @name searchSuccessFn
         * @desc Update posts array on view
         */
        function blockHotelSuccessFn(data, status, headers, config) {
            vm.hotels = data.data;
            var searchObject = HotelShared.getSearchObject();
            vm.room = searchObject.room;
            HotelShared.setBlockHotel(vm.hotels);
            $location.path('/hoteles/reservar');
        }

        /**
         * @name searchErrorFn
         * @desc Show snackbar with error
         */
        function blockHotelErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }


    }


    angular
        .module('preferentBooking.hotelbeds.directives')
        .directive('resultadohotel', resultadohotel);


})();