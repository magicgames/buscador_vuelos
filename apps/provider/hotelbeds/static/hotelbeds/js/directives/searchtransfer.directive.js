(function () {
    'use strict';
    /* @ngInject */
    function resultadotransfer() {

        var directive = {
            restrict: 'EA',
            scope: {
                transfer: '=',
            },
            templateUrl: '/static/hotelbeds/transfer_list.html',
            link: link,


            controller: TransferListController,

            controllerAs: 'vm',
            bindToController: true
        };
        return directive;

        function link(scope, element, attributes) {

            scope.$on(
                "$destroy",
                function handleDestroyEvent() {
                    console.log('Eliminado directiva');
                }
            );
        }
    }

    TransferListController.$inject = ['TransferBooking', 'TransferShared', 'CommonService', '$location'];

    function TransferListController(TransferBooking, TransferShared, CommonService, $location) {
        var vm = this;

        vm.transferObj = {};
        vm.blockTransfer = blockTransfer;


        function blockTransfer() {
            var searchObj = TransferShared.getSearchObject();
            var token = CommonService.generateToken();
            var obj = {
                securityToken: token,
                in: vm.transferObj.in,
                out: vm.transferObj.out,
                occupation: searchObj.occupation,
                destination: searchObj.destination,
                date: {
                    in: {
                        date: searchObj.fromDate,
                        time: searchObj.fromTime
                    },
                    out: {
                        date: searchObj.toDate,
                        time: searchObj.toTime
                    }

                }
            };
            TransferBooking.createBooking(obj).then(blockTransferSuccessFn, blockTransferErrorFn)


        }

        // CALLBACKS
        /**
         * @name searchSuccessFn
         * @desc Update posts array on view
         */
        function blockTransferSuccessFn(data, status, headers, config) {
            TransferShared.setBlockObject(data.data);
            $location.path('/transfer/reservar');
        }

        /**
         * @name searchErrorFn
         * @desc Show snackbar with error
         */
        function blockTransferErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }
    }

    angular
        .module('preferentBooking.hotelbeds.directives')
        .directive('resultadotransfer', resultadotransfer);


})();