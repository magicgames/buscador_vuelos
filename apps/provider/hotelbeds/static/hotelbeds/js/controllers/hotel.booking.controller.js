/**
 * HotelSearchController
 * @namespace preferentBooking.search.controllers
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.hotelbeds.controllers')
        .controller('HotelBookingController', HotelBookingController);

    HotelBookingController.$inject = ['$scope', 'CommonService', 'HotelBooking', 'HotelShared', 'Authentication', '$modal', '$filter'];

    /**
     * @namespace HotelBookingController
     */
    function HotelBookingController($scope, CommonService, HotelBooking, HotelShared, Authentication, $modal, $filter) {
        var vm = this;
        vm.comment = '';
        vm.guest = [];
        vm.holder = {};
        vm.searchObject = {};
        vm.card={};
        vm.directPayment = 'N';
        vm.range = range;
        vm.range_addition = range_addition;
        vm.generateAdultType = generateAdultType;
        vm.generateChildType = generateChildType;
        vm.generateBooking = generateBooking;
        vm.directPayment = 'N';
        activate();

        /**
         * @name activate
         * @desc Actions to be performed when this controller is instantiated
         * @memberOf preferentBooking.servivuelo.controllers.HotelBookingController
         */
        function activate() {

            Authentication.isLogged();
            vm.searchObject = HotelShared.getSearchObject();
            console.log(vm.searchObject);
            vm.room = vm.searchObject.room;
            vm.blockHotel = HotelShared.getBlockHotel();
            vm.directPayment = HotelShared.getDirectPayment();

        }

        function generateBooking() {
            var dataObj;
            vm.holder['customerId'] = 'Hldr';
            vm.holder['type'] = 'AD';
            vm.holder['card']={};
            if (vm.card.hasOwnProperty('type')) {
                vm.holder['card']= {
                    cardType:vm.card.type.type,
                    cardNumber:vm.card.cardNumber,
                    cardHolder:vm.holder.name+' '+vm.holder.lastName,
                    expirationDate:vm.card.month+''+vm.card.year,
                    cardCvc: vm.card.cvc
                }
            }
            dataObj = {
                guest: vm.guest,
                holder: vm.holder,
                comment: vm.comment,
                securityToken: CommonService.generateToken(),
                purchaseToken: vm.blockHotel.purchaseToken,
                serviceToken: vm.blockHotel.serviceToken,
                transactionReference: CommonService.generateToken(15)
            };

            HotelBooking.confirmBooking(dataObj).then(bookingSuccessFn, bookingErrorFn);

        }

        function bookingSuccessFn(data, status, headers, config) {
            vm.hotels = data.data;
            console.log(vm.hotels);
            myModal.$promise.then(myModal.hide);
        }

        /**
         * @name searchErrorFn
         * @desc Show snackbar with error
         */
        function bookingErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }

        /**
         * @name range
         * @param  num integer
         * @dec generate a range
         */
        function range(num) {
            return CommonService.range(num);
        }

        function range_addition(num, addition) {
            return CommonService.range(num, addition);
        }

        function generateAdultType(id) {
            if (!(vm.guest.hasOwnProperty(id))) {
                vm.guest[id] = {}
            }
            vm.guest[id].type = 'AD';
            vm.guest[id].customerId = id + 1;
        }

        function generateChildType(id, age) {
            if (!(vm.guest.hasOwnProperty(id))) {
                vm.guest[id] = {}
            }
            vm.guest[id].type = 'CH';
            vm.guest[id].age = age;
            vm.guest[id].customerId = id + 1;

        }

        function prepareRoom(rooms) {


        }


    }
})();