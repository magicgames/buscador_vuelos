/**
 * HotelSearchController
 * @namespace preferentBooking.search.controllers
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.hotelbeds.controllers')
        .controller('HotelSearchController', HotelSearchController);

    HotelSearchController.$inject = ['$scope', 'CommonService', 'HotelSearch', 'HotelShared', 'Authentication', '$modal', '$filter'];

    /**
     * @namespace HotelSearchController
     */
    function HotelSearchController($scope, CommonService, HotelSearch, HotelShared, Authentication, $modal, $filter) {
        var vm = this;
        vm.numRoom = 1;
        vm.adultCount = {};
        vm.childCount = {};
        vm.childAge = {};
        vm.destinations={};
        vm.search = search;
        vm.findDestination = findDestination;
        vm.generateChild = generateChild;

        vm.range = range;
        vm.range_addition = range_addition;

        vm.groupDestination = groupDestination;

        var myModal = $modal({
            title: 'Buscando hoteles',
            content: 'Hola estamos buscando los mejores hoteles',
            show: false
        });

        activate();

        /**
         * @name activate
         * @desc Actions to be performed when this controller is instantiated
         * @memberOf preferentBooking.servivuelo.controllers.HotelSearchController
         */
        function activate() {
            Authentication.isLogged();
        }

        function groupDestination(item) {

            if (item.searchType == 'Hotel') {
                return 'Hotel';
            }

            if (item.searchType == 'Zone') {
                return 'Zona';
            }
            if (item.searchType == 'Destino') {
                return 'Destino';
            }

        }

        function generateChild(x) {
            if (x === 'undefinded') {
                return 0
            }
            return range(x);
        }

        function findDestination(content) {
            if (content.length > 3) {
                var dataObj = {query: content};
                console.log(dataObj);
                HotelSearch.destination(dataObj).then(destinationSuccessFn, destinationErrorFn)
            }
        }

        /**
         * @name search
         * @desc make a search
         */
        function search() {
            /*
             myModal.$promise.then(myModal.show);
             */
            var dateFilter = $filter('date');
            var session = CommonService.generateToken(15);
            var room = HotelSearch.generateRoom(vm.numRoom, vm.adultCount, vm.childCount, vm.childAge);
            var token = CommonService.generateToken();

            var dest = vm.destination.selected;
            var dataObj = {
                numRooms: vm.numRoom,
                destination: dest,
                fromDate: dateFilter($scope.fromDate, 'yyyy-MM-dd'),
                toDate: dateFilter($scope.toDate, 'yyyy-MM-dd'),
                room: room,
                session: session,
                securityToken: token
            };
            console.log(dataObj);
            vm.hotels = {};
            HotelShared.setSearchObject(dataObj);
            HotelSearch.searchHotel(dataObj).then(searchSuccessFn, searchErrorFn);
        }

        /**
         * @name range
         * @param  num integer
         * @dec generate a range
         */
        function range(num) {

            return CommonService.range(num);
        }

        function range_addition(num, addition) {
            return CommonService.range(num, addition);
        }

        /**
         * @name searchSuccessFn
         * @desc Update posts array on view
         */
        function searchSuccessFn(data, status, headers, config) {
            vm.hotels = data.data;
            console.log(data.data);
            myModal.$promise.then(myModal.hide);
        }

        /**
         * @name searchErrorFn
         * @desc Show snackbar with error
         */
        function searchErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }


        /**
         * @name destinationSuccessFn
         * @desc Update posts array on view
         */
        function destinationSuccessFn(data, status, headers, config) {
            console.log(data.data);
            vm.destinations = data.data;

        }


        /**
         * @name destinationErrorFn
         * @desc Show snackbar with error
         */
        function destinationErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }


    }
})();