/**
 * HotelSearchController
 * @namespace preferentBooking.search.controllers
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.hotelbeds.controllers')
        .controller('TransferSearchController', TransferSearchController);

    TransferSearchController.$inject = ['$scope', 'CommonService', 'TransferSearch', 'TransferShared', 'Authentication', '$modal', '$filter'];

    /**
     * @namespace HotelSearchController
     */
    function TransferSearchController($scope, CommonService, TransferSearch, TransferShared, Authentication, $modal, $filter) {
        var vm = this;
        vm.numRoom = 1;
        vm.adultCount = 2;
        vm.childCount = 0;
        vm.childAge = {};
        vm.search = search;
        vm.findDestination = findDestination;
        vm.generateChild = generateChild;

        vm.range = range;
        vm.range_addition = range_addition;

        vm.groupDestination = groupDestination;

        var myModal = $modal({
            title: 'Buscando hoteles',
            content: 'Hola estamos buscando los mejores hoteles',
            show: false
        });

        activate();

        /**
         * @name activate
         * @desc Actions to be performed when this controller is instantiated
         * @memberOf preferentBooking.servivuelo.controllers.HotelSearchController
         */
        function activate() {
             Authentication.isLogged();

        }

        function groupDestination(item) {

            if (item.searchType == 'Hotel') {
                return 'Hotel';
            }

            if (item.searchType == 'Zone') {
                return 'Zona';
            }
            if (item.searchType == 'Destino') {
                return 'Destino';
            }

        }

        function generateChild(x) {
            if (x === 'undefinded') {
                return 0
            }
            return range(x);
        }

        function findDestination(content) {
            if (content.length > 3) {
                var dataObj = {query: content};
                TransferSearch.transferDestination(dataObj).then(destinationSuccessFn, destinationErrorFn)
            }
        }

        /**
         * @name search
         * @desc make a search
         */
        function search() {
            /*
             myModal.$promise.then(myModal.show);
             */
            var dateFilter = $filter('date');
            var session = CommonService.generateToken(15);
            var token = CommonService.generateToken();
            var occupation = {
                adultCount: parseInt(vm.adultCount),
                childCount: parseInt(vm.childCount)

            };

            if (parseInt(occupation['childCount']) > 0) {
                occupation['age'] = vm.childAge
            }
            var dest = vm.destination.selected;
            var dataObj = {
                destination: dest,
                occupation: occupation,
                fromDate: dateFilter($scope.fromDate, 'yyyy-MM-dd'),
                fromTime: dateFilter($scope.fromTime, 'HHmmss'),
                toTime: dateFilter($scope.toTime, 'HHmmss'),
                toDate: dateFilter($scope.toDate, 'yyyy-MM-dd'),
                session: session,
                securityToken: token
            };
            vm.hotels = {};
            console.log(dataObj);
            TransferShared.setSearchObject(dataObj);
            TransferSearch.searchTransfer(dataObj).then(searchSuccessFn, searchErrorFn);
        }

        /**
         * @name range
         * @param  num integer
         * @dec generate a range
         */
        function range(num) {

            return CommonService.range(num);
        }

        function range_addition(num, addition) {
            return CommonService.range(num, addition);
        }

        /**
         * @name searchSuccessFn
         * @desc Update posts array on view
         */
        function searchSuccessFn(data, status, headers, config) {
            vm.transfers = data.data;
            console.log(vm.transfers);
        }

        /**
         * @name searchErrorFn
         * @desc Show snackbar with error
         */
        function searchErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }


        /**
         * @name destinationSuccessFn
         * @desc Update posts array on view
         */
        function destinationSuccessFn(data, status, headers, config) {
            vm.destinations = data.data;

        }


        /**
         * @name destinationErrorFn
         * @desc Show snackbar with error
         */
        function destinationErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }


    }
})();