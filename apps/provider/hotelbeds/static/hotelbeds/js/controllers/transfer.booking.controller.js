/**
 * HotelSearchController
 * @namespace preferentBooking.search.controllers
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.hotelbeds.controllers')
        .controller('TransferBookingController', TransferBookingController);

    TransferBookingController.$inject = ['$scope', 'CommonService', 'TransferBooking', 'TransferShared', 'Authentication', '$modal', '$filter'];

    /**
     * @namespace HotelBookingController
     */
    function TransferBookingController($scope, CommonService, TransferBooking, TransferShared, Authentication, $modal, $filter) {
        var vm = this;
        vm.holder = {};
        vm.guest = [];
        vm.comment = '';
        vm.range = range;
        vm.showFlightIn=false;
        vm.showFlightOut=false;
        vm.range_addition = range_addition;
        vm.generateAdultType = generateAdultType;
        vm.generateChildType = generateChildType;
        vm.generateBooking = generateBooking;
        activate();

        /**
         * @name activate
         * @desc Actions to be performed when this controller is instantiated
         * @memberOf preferentBooking.servivuelo.controllers.TransferBookingController
         */
        function activate() {

            Authentication.isLogged();
            vm.searchObject = TransferShared.getSearchObject();
            vm.blockTransfer = TransferShared.getBlockObject();

            vm.occupation = vm.searchObject.occupation;
            if (vm.blockTransfer.hasOwnProperty('in')) {
                vm.showFlightIn=true;
                vm.blockTransfer.in.terminalCode = vm.searchObject.destination.terminalCode;
                vm.blockTransfer.in.date = vm.searchObject.fromDate;
                vm.blockTransfer.in.time = vm.searchObject.fromTime;
            }
            if (vm.blockTransfer.hasOwnProperty('out')) {
                vm.showFlightOut=true;
                vm.blockTransfer.out.terminalCode = vm.searchObject.destination.terminalCode;
                vm.blockTransfer.out.date = vm.searchObject.toDate;
                vm.blockTransfer.out.time = vm.searchObject.toTime;
            }


        }

        function generateBooking() {
            var dataObj;
            vm.holder['customerId'] = 'Hldr';
            vm.holder['type'] = 'AD';
            vm.holder['card'] = {};
            dataObj = {
                guest: vm.guest,
                holder: vm.holder,
                comment: vm.comment,
                securityToken: CommonService.generateToken(),
                blockTransfer: vm.blockTransfer,
                transactionReference: CommonService.generateToken(15)
            };
            console.log(dataObj);

            TransferBooking.confirmBooking(dataObj).then(bookingSuccessFn, bookingErrorFn);

        }

        function bookingSuccessFn(data, status, headers, config) {
            vm.hotels = data.data;
            console.log(vm.hotels);
        }

        /**
         * @name searchErrorFn
         * @desc Show snackbar with error
         */
        function bookingErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }

        /**
         * @name range
         * @param  num integer
         * @dec generate a range
         */
        function range(num) {
            return CommonService.range(num);
        }

        function range_addition(num, addition) {
            return CommonService.range(num, addition);
        }

        function generateAdultType(id) {
            if (!(vm.guest.hasOwnProperty(id))) {
                vm.guest[id] = {}
            }
            vm.guest[id].type = 'AD';
            vm.guest[id].customerId = id + 1;
        }

        function generateChildType(id, age) {
            if (!(vm.guest.hasOwnProperty(id))) {
                vm.guest[id] = {}
            }
            vm.guest[id].type = 'CH';
            vm.guest[id].age = age;
            vm.guest[id].customerId = id + 1;

        }

        function prepareRoom(rooms) {


        }


    }
})();