(function () {
    'use strict';
    angular
        .module('preferentBooking.hotelbeds.directives', []);
    angular
        .module('preferentBooking.hotelbeds.controllers', ['ngSanitize', 'mgcrea.ngStrap', 'ui.select', 'preferentBooking.authentication', 'preferentBooking.layout']);

    angular
        .module('preferentBooking.hotelbeds.services', ['ngCookies', 'underScore']);
    angular
        .module('preferentBooking.hotelbeds.config', []);
    angular
        .module('preferentBooking.hotelbeds', [
            'preferentBooking.hotelbeds.controllers',
            'preferentBooking.hotelbeds.services',
            'preferentBooking.hotelbeds.config',
            'preferentBooking.hotelbeds.directives'
        ])

})();