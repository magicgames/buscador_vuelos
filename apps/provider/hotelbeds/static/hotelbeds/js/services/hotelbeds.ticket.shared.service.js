/**
 * Servivuelo
 * @namespace preferentBooking.hotelbeds.services
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.hotelbeds.services')
        .factory('TicketShared', TicketShared);

    TicketShared.$inject = ['$http', '_', 'Slug'];

    /**
     * @namespace HotelShared
     * @returns {Factory}
     */
    function TicketShared($http, _, Slug) {
        /**
         * Variables
         */
        var searchObj;
        var blockObj;
        /**
         *
         * @type {{}}
         */
        var TicketShared = {
            setSearchObject: setSearchObject,
            getSearchObject: getSearchObject,
            setBlockObject: setBlockObject,
            getBlockObject: getBlockObject

        };

        return TicketShared;

        ////////////////////


        function setSearchObject(obj) {
            searchObj = obj;

        }

        function getSearchObject() {
            return searchObj;

        }

        function setBlockObject(obj) {
            blockObj=obj
        }
         function getBlockObject() {
           return  blockObj
        }

    }
})();