/**
 * hotelbeds
 * @namespace preferentBooking.hotelbeds.services
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.hotelbeds.services')
        .factory('TicketSearch', TicketSearch);

    TicketSearch.$inject = ['$http'];

    /**
     * @namespace Flights
     * @returns {Hotel}
     */
    function TicketSearch($http) {
        var TicketSearch = {
            ticketDestination:ticketDestination,
            searchTicket:searchTicket


        };

        return TicketSearch;

        ////////////////////



        function ticketDestination(content) {
                return $http.post("/api/v1/hoteles/ticket/destination", content);
        }


        function searchTicket(content) {
            return $http.post("/api/v1/hoteles/ticket/search", content);
        }


    }
})();