/**
 * hotelbeds
 * @namespace preferentBooking.hotelbeds.services
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.hotelbeds.services')
        .factory('TransferSearch', TransferSearch);

    TransferSearch.$inject = ['$http'];

    /**
     * @namespace Flights
     * @returns {Hotel}
     */
    function TransferSearch($http) {
        var TransferSearch = {
            transferDestination:transferDestination,
            searchTransfer:searchTransfer


        };

        return TransferSearch;

        ////////////////////



        function transferDestination(content) {
                return $http.post("/api/v1/hoteles/transfer/destination", content);
        }


        function searchTransfer(content) {
            return $http.post("/api/v1/hoteles/transfer/search", content);
        }


    }
})();