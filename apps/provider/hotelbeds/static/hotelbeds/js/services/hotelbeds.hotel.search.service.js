/**
 * hotelbeds
 * @namespace preferentBooking.hotelbeds.services
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.hotelbeds.services')
        .factory('HotelSearch', HotelSearch);

    HotelSearch.$inject = ['$http'];

    /**
     * @namespace Flights
     * @returns {Hotel}
     */
    function HotelSearch($http) {
        var HotelSearch = {
            destination: destination,
            transferDestination: transferDestination,
            searchHotel: searchHotel,
            searchTransfer: searchTransfer,

            generateRoom: generateRoom
        };

        return HotelSearch;

        ////////////////////

        /**
         * @desc Return destination autocomplete or False
         * @name destination
         * @param  string content
         * @returns {*}
         */
        function destination(content) {

            console.log(content);
            return $http.post("/api/v1/hoteles/destination", content);

        }

        function transferDestination(content) {
            return $http.post("/api/v1/hoteles/transfer/destination", content);
        }

        function searchHotel(content) {
            return $http.post("/api/v1/hoteles/search", content);
        }

        function searchTransfer(content) {
            return $http.post("/api/v1/hoteles/transfer/search", content);
        }


        function generateRoom(numRoom, adultCount, childCount, childAge) {

            var room = [];
            var room_intermediate = [];
            for (var i = 0; i < numRoom; i++) {
                var x = {
                    'numRoom': 1,
                    'adultCount': parseInt(adultCount[i + 1])
                };
                if (childCount.hasOwnProperty(i + 1)) {

                    x.childCount = parseInt(childCount[i + 1]);
                    if (childAge.hasOwnProperty(i + 1)) {
                        var ages = childAge[i + 1];
                        x.age = [];
                        for (var v in  ages) {
                            x.age.push(ages[v])
                        }
                    }
                } else {
                    x.childCount = 0;
                }
                room.push(x)
            }
            for (var i = 0; i <= room.length; i++) {
                var reindex = false;
                var elem = room.shift();
                for (var j = 0; j < room.length; j++) {
                    var new_room = room[j];

                    if (!((elem['adultCount'] == new_room['adultCount'])
                        &&
                        (elem['childCount'] == new_room['childCount']))) {
                    } else {
                        elem['numRoom'] = elem['numRoom'] + 1;
                        try {
                            var age_new = new_room['age'];
                            var elem_age;
                            try {
                                elem_age = elem['age'];

                            } catch (err) {
                                elem_age = []

                            } finally {

                                for (z = 0; z < age_new.length; z++) {
                                    elem_age.push(age_new[z]);
                                }
                                elem['age'] = elem_age;
                            }

                        } catch (err) {
                        }
                        finally {
                            delete room[j];
                            reindex = true;
                        }
                    }
                }
                room_intermediate.push(elem);
                if (reindex) {
                    var ri = [];
                    for (var z in room) {

                        if (room[z] !== 'undefinded') {
                            ri.push(room[z]);
                        }
                    }
                    room = ri;
                    reindex = false;

                }
                i = 0;
            }
            return room_intermediate;
        }
    }
})();