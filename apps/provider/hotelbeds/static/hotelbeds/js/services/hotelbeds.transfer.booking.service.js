/**
 * hotelbeds
 * @namespace preferentBooking.hotelbeds.services
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.hotelbeds.services')
        .factory('TransferBooking', TransferBooking);

    TransferBooking.$inject = ['$http'];

    /**
     * @namespace TransferBooking
     * @returns {Factory}
     */
    function TransferBooking($http) {


        var TransferBooking = {

            createBooking: createBooking,
            confirmBooking:confirmBooking


        };

        return TransferBooking;

        ////////////////////


        /**
         * @name createBooking
         * @desc Create a booking process
         * @param content
         * @returns {HttpPromise}
         * @memberOf preferentBooking.hotelbeds.services.TransferBooking
         */
        function createBooking(content) {
            return $http.post("/api/v1/hoteles/transfer/booking/create", content);
        }


        /**
         * @desc: create ticket
         * @name confirmBooking
         * @param content
         * @returns {HttpPromise}
         */
        function confirmBooking(content) {
            var result = $http.post("/api/v1/hoteles/transfer/booking/confirm", content);
            return result;


        }


    }
})();