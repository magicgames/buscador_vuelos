/**
 * Servivuelo
 * @namespace preferentBooking.hotelbeds.services
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.hotelbeds.services')
        .factory('HotelShared', HotelShared);

    HotelShared.$inject = ['$http', '_', 'Slug'];

    /**
     * @namespace HotelShared
     * @returns {Factory}
     */
    function HotelShared($http, _, Slug) {
        /**
         * Variables
         */
        var searchObj= null;
        var blockObj= null;
        var directPayment=null;
        /**
         *
         * @type {{}}
         */
        var HotelShared = {
            setSearchObject: setSearchObject,
            getSearchObject: getSearchObject,
            setBlockHotel: setBlockHotel,
            getBlockHotel: getBlockHotel,
            setDirectPayment:setDirectPayment,
            getDirectPayment:getDirectPayment

        };

        return HotelShared;

        ////////////////////


        function setSearchObject(obj) {
            searchObj = obj;

        }

        function getSearchObject() {
            return searchObj;

        }

        function setBlockHotel(obj) {

            blockObj = obj;
        }

        function getBlockHotel() {
            return blockObj;
        }

        function setDirectPayment(payment) {
            directPayment=payment
        }

        function getDirectPayment() {
            return directPayment
        }

    }
})();