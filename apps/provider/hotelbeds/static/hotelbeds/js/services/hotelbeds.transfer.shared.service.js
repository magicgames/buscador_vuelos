/**
 * Servivuelo
 * @namespace preferentBooking.hotelbeds.services
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.hotelbeds.services')
        .factory('TransferShared', TransferShared);

    TransferShared.$inject = ['$http', '_', 'Slug'];

    /**
     * @namespace HotelShared
     * @returns {Factory}
     */
    function TransferShared($http, _, Slug) {
        /**
         * Variables
         */
        var searchObj;
        var blockObj;
        /**
         *
         * @type {{}}
         */
        var TransferShared = {
            setSearchObject: setSearchObject,
            getSearchObject: getSearchObject,
            setBlockObject: setBlockObject,
            getBlockObject: getBlockObject

        };

        return TransferShared;

        ////////////////////


        function setSearchObject(obj) {
            searchObj = obj;

        }

        function getSearchObject() {
            return searchObj;

        }

        function setBlockObject(obj) {
            blockObj=obj
        }
         function getBlockObject() {
           return  blockObj
        }

    }
})();