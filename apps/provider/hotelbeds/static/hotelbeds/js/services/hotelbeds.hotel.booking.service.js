/**
 * hotelbeds
 * @namespace preferentBooking.hotelbeds.services
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.hotelbeds.services')
        .factory('HotelBooking', HotelBooking);

    HotelBooking.$inject = ['$http'];

    /**
     * @namespace HotelBooking
     * @returns {Factory}
     */
    function HotelBooking($http) {


        var HotelBooking = {

            createBooking: createBooking,
            confirmBooking:confirmBooking


        };

        return HotelBooking;

        ////////////////////


        /**
         * @name createBooking
         * @desc Create a booking process
         * @param content
         * @returns {HttpPromise}
         * @memberOf preferentBooking.hotelbeds.services.HotelBooking
         */
        function createBooking(content) {

            return $http.post("/api/v1/hoteles/booking/create", content);
        }




        /**
         * @desc: create ticket
         * @name confirmBooking
         * @param content
         * @returns {HttpPromise}
         */
        function confirmBooking(content) {
            var result = $http.post("/api/v1/hoteles/booking/confirm", content);
            return result;


        }


    }
})();