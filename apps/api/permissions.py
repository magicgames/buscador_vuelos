

from rest_framework import permissions
class AllowChatOnly(permissions.BasePermission):
    """
    Allow any access.
    This isn't strictly required, since you could use an empty
    permission_classes list, but it's useful because it makes the intention
    more explicit.
    """
    def has_permission(self, request, view):
        req_id = request.session.get('req_id',None)
        if req_id is not  None:
            return True
        return False

