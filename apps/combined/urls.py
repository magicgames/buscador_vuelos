# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from .views import *

urlpatterns = patterns('',
                       url(regex=r'^search$', view=SearchHotelFlightApiView.as_view(), name='search_hotel'),
                       )