(function () {
    'use strict';

    angular
        .module('preferentBooking.combined.config')
        .config(config);

    config.$inject = ['$datepickerProvider'];

    /**
     * @name config
     * @desc Enable HTML5 routing
     */
    function config($datepickerProvider) {

        angular.extend($datepickerProvider.defaults, {
            dateFormat: 'dd/MM/yyyy',
            startWeek: 1
        });
    }
})();