(function () {
    'use strict';


    angular
        .module('preferentBooking.combined.controllers', [
            'ngSanitize',
            'mgcrea.ngStrap',
            'ui.select',
            'preferentBooking.authentication',
            'preferentBooking.layout',
            'preferentBooking.hotelbeds.services',
            'preferentBooking.servivuelo.services',
            'preferentBooking.authentication',
            'preferentBooking.servivuelo.directives',
            'preferentBooking.hotelbeds.directives'
        ]

    );
     angular
        .module('preferentBooking.combined.services', []);
    angular
        .module('preferentBooking.combined.config', []);



    angular
        .module('preferentBooking.combined', [
            'preferentBooking.combined.controllers',
            'preferentBooking.combined.services',
            'preferentBooking.combined.config'
        ]);

})();


