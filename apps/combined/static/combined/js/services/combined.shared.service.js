/**
 * Servivuelo
 * @namespace preferentBooking.combined.services
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.combined.services')
        .factory('CombinedShared', CombinedShared);

    CombinedShared.$inject = ['$http', '_', 'Slug'];

    /**
     * @namespace HotelShared
     * @returns {Factory}
     */
    function CombinedShared($http, _, Slug) {
        /**
         * Variables
         */
       var searchObj;
        /**
         *
         * @type {{}}
         */
        var CombinedShared = {
            setSearchObject:setSearchObject,
            getSearchObject:getSearchObject

        };

        return CombinedShared;

        ////////////////////


        function setSearchObject(obj) {
            searchObj=obj;

        }

         function getSearchObject() {
            return searchObj;

        }

    }
})();