/**
 * hotelbeds
 * @namespace preferentBooking.hotelbeds.services
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.combined.services')
        .factory('CombinedSearch', CombinedSearch);

    CombinedSearch.$inject = ['$http','FlightSearch'];

    /**
     * @namespace Flights
     * @returns {Hotel}
     */
    function CombinedSearch($http,FlightSearch) {
        var CombinedSearch = {
            searchHotelFlight: searchHotelFlight
        };

        return CombinedSearch;



        function searchHotelFlight(content) {

            return $http.post("/api/v1/combined/search", content);
        }

    }
})();