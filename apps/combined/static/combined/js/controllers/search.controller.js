/**
 * HotelSearchController
 * @namespace preferentBooking.search.controllers
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.combined.controllers')
        .controller('HotelFlightSearchController', HotelFlightSearchController);

    HotelFlightSearchController.$inject = [
        '$scope',
        'CombinedSearch',
        'CombinedShared',
        'CommonService',
        'FlightSearch',
        'HotelSearch',
        'Authentication',
        '$modal',
        '$filter'
    ];

    /**
     * @namespace HotelSearchController
     */
    function HotelFlightSearchController($scope,
                                         CombinedSearch,
                                         CombinedShared,
                                         CommonService,
                                         FlightSearch,
                                         HotelSearch,
                                         Authentication,
                                         $modal,
                                         $filter) {
        var vm = this;
        vm.numRoom = 1;
        vm.adultCount = {};
        vm.childCount = {};
        vm.childAge = {};
        vm.flights = {Status: ''};
        vm.airportFrom = {};
        vm.airportTo = {};
        vm.defaultAdultCount = 2;
        vm.defaultChildCount = 0;
        vm.resident=false;


        vm.search = search;
        vm.findDestination = findDestination;
        vm.generateChild = generateChild;

        vm.range = range;
        vm.range_addition = range_addition;


        vm.groupDestination = groupDestination;

        var myModal = $modal({
            title: 'Buscando hoteles',
            content: 'Hola estamos buscando los mejores hoteles',
            show: false
        });

        activate();

        /**
         * @name activate
         * @desc Actions to be performed when this controller is instantiated
         * @memberOf preferentBooking.servivuelo.controllers.HotelSearchController
         */
        function activate() {
            Authentication.isLogged();
            FlightSearch.airports().then(airportsSuccessFn, airportsErrorFn);
            FlightSearch.airlines().then(airlineSuccessFn, airlineErrorFn);

        }

        function groupDestination(item) {

            if (item.type == 'hotel') {
                return 'Hoteles';
            }
            if (item.type == 'Destino') {
                return 'Destino';
            }

        }

        function generateChild(x) {
            if (x === 'undefinded') {
                return 0
            }
            return range(x);
        }

        function findDestination(content) {
            if (content.length > 3) {
                var dataObj = {query: content};
                HotelSearch.destination(dataObj).then(destinationSuccessFn, destinationErrorFn)
            }

        }
        function groupDestination(item) {

            if (item.searchType == 'Hotel') {
                return 'Hotel';
            }

            if (item.searchType == 'Zone') {
                return 'Zona';
            }
            if (item.searchType == 'Destino') {
                return 'Destino';
            }

        }
        /**
         * @name search
         * @desc make a search
         */
        function search() {
            /*
             myModal.$promise.then(myModal.show);
             */
            var dateFilter = $filter('date');
            var room = HotelSearch.generateRoom(vm.numRoom, vm.adultCount, vm.childCount, vm.childAge);
            var fromDate = dateFilter($scope.fromDate, 'yyyy-MM-dd');
            var toDate = dateFilter($scope.toDate, 'yyyy-MM-dd');
            var from = vm.airportFrom.selected.code;
            var toAirport = vm.destination.selected.terminalCode;
            var hotelSearchSession = CommonService.generateToken(15);
            var hotelSearchToken = CommonService.generateToken();
            var numAdult = generateNum(vm.adultCount);
            var numChild = generateNumChild(vm.childCount, vm.childAge);
            var numInfant = generateNumInfant(vm.childCount, vm.childAge);
            var dataObj = {
                hotel: {
                    fromDate: fromDate,
                    toDate: toDate,
                    room: room,
                    numRooms: vm.numRoom,
                    destination: vm.destination.selected,
                    session: hotelSearchSession,
                    securityToken: hotelSearchToken

                },
                flight: {
                    fromDate: fromDate,
                    toDate: toDate,
                    from: from,
                    to: toAirport,
                    numAdult: numAdult,
                    residentAdult: FlightSearch.generateResident(numAdult, vm.resident),
                    numChild: numChild,
                    residentChild: FlightSearch.generateResident(numChild, vm.resident),
                    numInfant: numInfant,
                    residentInfant: FlightSearch.generateResident(numInfant, vm.resident),
                    lowCost: vm.lowCost,
                    oneWay: vm.oneWay,
                    directFlight: vm.directFlight
                }
            };
            CombinedShared.setSearchObject(dataObj);
            CombinedSearch.searchHotelFlight(dataObj).then(searchSuccessFn, searchErrorFn);

        }



        function generateNum(obj) {
            var result = 0;
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    result = result + parseInt(obj[key])
                }
            }
            return result;
        }

        function generateNumChild(obj, age) {
            return generateSpecial(obj, age, 2, 18)
        }

        function generateNumInfant(obj, age) {
            return generateSpecial(obj, age, 0, 2)
        }

        function generateSpecial(obj, age, min, max) {
            var result = 0;

            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    if (age.hasOwnProperty(key)) {
                        var ageObj = age[key];
                        for (var key2 in ageObj) {
                            if (ageObj.hasOwnProperty(key2)) {
                                var age_res = parseInt(ageObj[key2]);
                                if ((age_res >= min) && (age_res < max)) {
                                    result++;
                                }
                            }

                        }
                    }

                }


            }
            return result;
        }

        /**
         * @name range
         * @param  num integer
         * @dec generate a range
         */
        function range(num) {

            return CommonService.range(num);
        }

        function range_addition(num, addition) {
            return CommonService.range(num, addition);
        }

        /**
         * @name searchSuccessFn
         * @desc Update posts array on view
         */
        function searchSuccessFn(data, status, headers, config) {

            var result = data.data;
            if (result.hasOwnProperty('flight')) {
                vm.flights = result['flight'];

            }
            if (result.hasOwnProperty('hotel')) {
                vm.hotels = result['hotel'];

            }

            console.log(result);
        }

        /**
         * @name searchErrorFn
         * @desc Show snackbar with error
         */
        function searchErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }


        /**
         * @name destinationSuccessFn
         * @desc Update posts array on view
         */
        function destinationSuccessFn(data, status, headers, config) {
            vm.destinations = data.data;

        }


        /**
         * @name destinationErrorFn
         * @desc Show snackbar with error
         */
        function destinationErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }

        function airportsSuccessFn(data, status, headers, config) {
            vm.iataCodes = data.data;
        }

        function airportsErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }

        function airlineSuccessFn(data, status, headers, config) {
            vm.airlines = data.data;
            // FlightShared.setAirline(vm.airlines); //Store the airline into shared service
        }

        function airlineErrorFn(data, status, headers, config) {
            console.log("error " + data.error)
        }
    }
})();