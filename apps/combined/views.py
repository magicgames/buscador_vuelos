# -*- coding: utf-8 -*-
import simplejson as json
# Create your views here.
from django.conf import settings
from django.views.generic import View
from zato.client import JSONClient
from provider.hotelbeds.utils.basic import generate_search_hotel_request, generate_default_value as hotel_generate_default_value
from provider.servivuelo.utils.basic import generate_search_flight_request, generate_default_value as vuelo_generate_default_value
from django.http import HttpResponseBadRequest, HttpResponse
from common.views import LoginRequiredView


# Create your views here.
class SearchHotelFlightApiView(LoginRequiredView,View):
    def post(self, request, *args, **kwargs):
        result = {}
        body = self.request.read()
        d = json.loads(body)
        request_hotel = generate_search_hotel_request(d['hotel'])
        request_hotel = hotel_generate_default_value(request_hotel,d['hotel'],self.request.user)
        request_flight = generate_search_flight_request(d['flight'])
        request_flight = vuelo_generate_default_value(request_flight, self.request.user)

        try:
            if (request_hotel['reason'] != '') or (request_flight['reason'] != ''):
                raise
            request = {
                'flight': request_flight['request'],
                'hotel': request_hotel['request']

            }
            print json.dumps(request)
            client_json = JSONClient(settings.CLIENT_ADDRESS, '/combined/hotel/flight/search', settings.CLIENT_CREDENTIALS)
            result = client_json.invoke(request)
            print result
            return HttpResponse(json.dumps(result.data), content_type="application/json")
        except Exception as e:

            if (request_hotel['reason'] != '') and (request_flight['reason'] != ''):
                response = request_hotel['reason'] + ' ' + request_flight['reason']
            elif request_hotel['reason'] != '':
                response = request_hotel['reason']
            else:
                response = request_flight['reason']

            return HttpResponseBadRequest(content=response)
