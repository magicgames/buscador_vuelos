(function () {
  'use strict';

  angular
    .module('preferentBooking.authentication', [
      'preferentBooking.authentication.controllers',
      'preferentBooking.authentication.services'
    ]);

  angular
    .module('preferentBooking.authentication.controllers', []);

  angular
    .module('preferentBooking.authentication.services', ['ngCookies']);
})();