# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from .views import *

account_list = AccountViewSet.as_view({

    'post': 'create'
})

urlpatterns = patterns('',
                       url(regex=r'^register$', view=account_list, name='register'),
                       url(regex=r'^login$', view=LoginView.as_view(), name='login'),
                       url(regex=r'^logout', view=LogoutView.as_view(), name='logout'),
                       )

