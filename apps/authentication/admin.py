from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from models import *
# Register your models here.

class AccountAdmin(UserAdmin):

    list_display = ('username',
                    'email',
                    'first_name',
                    'last_name',
                    'is_staff',
                    'date_joined'
                    )

    list_filter = ('username','email')

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username', 'password1', 'password2'),
        }),
    )


admin.site.register(Account, AccountAdmin)

