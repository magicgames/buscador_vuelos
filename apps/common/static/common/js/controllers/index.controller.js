/**
 * NavbarController
 * @namespace preferentBooking.layout.controllers
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.layout.controllers')
        .controller('CommonIndexController', CommonIndexController);

    CommonIndexController.$inject = ['$scope', '$location', 'Authentication'];

    /**
     * @namespace NavbarController
     */
    function CommonIndexController($scope, $location, Authentication) {
        var vm = this;

        activate();
        /**
         * @name logout
         * @desc Log the user out
         * @memberOf preferentBooking.layout.controllers.CommonIndexController
         */

        function activate() {
          Authentication.isLogged();

        }

    }
})();