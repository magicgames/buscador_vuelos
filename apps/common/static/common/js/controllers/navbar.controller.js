/**
* NavbarController
* @namespace preferentBooking.layout.controllers
*/
(function () {
  'use strict';

  angular
    .module('preferentBooking.layout.controllers')
    .controller('NavbarController', NavbarController);

  NavbarController.$inject = ['$scope', 'Authentication'];

  /**
  * @namespace NavbarController
  */
  function NavbarController($scope, Authentication) {
    var vm = this;

    vm.logout = logout;
    vm.isUser = isUser;

    /**
    * @name logout
    * @desc Log the user out
    * @memberOf preferentBooking.layout.controllers.NavbarController
    */
    function logout() {
      Authentication.logout();
    }

    function isUser() {

      return Authentication.isAuthenticated()
    }
  }
})();