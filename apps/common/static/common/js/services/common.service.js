/**
 * Servivuelo
 * @namespace preferentBooking.hotelbeds.services
 */
(function () {
    'use strict';

    angular
        .module('preferentBooking.layout.services')
        .factory('CommonService', CommonService);

    CommonService.$inject = ['$http', '_', 'Slug'];

    /**
     * @namespace HotelShared
     * @returns {Factory}
     */
    function CommonService($http, _, Slug) {
        /**
         * Variables
         */
       var searchObj;
        /**
         *
         * @type {{}}
         */
        var CommonService = {
            range: range,
            rangeAddition: rangeAddition,
            generateToken:generateToken,

        };

        return CommonService;

        ////////////////////

        /**
         * @name range
         * @param  num integer
         * @dec generate a range
         */
        function range(num) {
            if (num ==='undefined') return [];
            return _.range(num);
        }

        function rangeAddition(num, addition) {
            addition = typeof addition !== 'undefined' ? parseInt(addition) : 0;
            num = parseInt(num) + addition;
            return _.range(num);
        }

        function generateToken(numElem) {
            if (typeof(numElem)==='undefined') numElem = 10;
            return new Array(numElem+1).join((Math.random().toString(36)+'00000000000000000').slice(2, 18)).slice(0, numElem);

        }



    }
})();