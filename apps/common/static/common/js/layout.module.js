(function () {
    'use strict';

    angular
        .module('preferentBooking.layout', [
            'preferentBooking.layout.controllers',
            'preferentBooking.layout.services'
        ]);

    angular
        .module('preferentBooking.layout.controllers', []);
     angular
        .module('preferentBooking.layout.services', []);
})();