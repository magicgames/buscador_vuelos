(function () {
    'use strict';

    angular
        .module('preferentBooking.routes')
        .config(config);

    config.$inject = ['$routeProvider'];

    /**
     * @name config
     * @desc Define valid application routes
     */
    function config($routeProvider) {

        $routeProvider.when('/register', {
            controller: 'RegisterController',
            controllerAs: 'vm',
            templateUrl: '/static/index.html'

        })
            .when('/login', {
                controller: 'LoginController',
                controllerAs: 'vm',
                templateUrl: '/static/authentication/login.html'
            })
            .when('/vuelos', {
                controller: 'FlightSearchController',
                controllerAs: 'vm',
                templateUrl: '/static/servivuelo/flight_search.html'
            })
            .when('/vuelosMulti', {
                controller: 'FlightMultiSearchController',
                controllerAs: 'vm',
                templateUrl: '/static/servivuelo/flight_multisearch.html'
            })
            .when('/vuelos/reservar', {
                controller: 'FlightBookingController',
                controllerAs: 'vm',
                templateUrl: '/static/servivuelo/flight_booking.html'
            })

            .when('/hoteles', {
                controller: 'HotelSearchController',
                controllerAs: 'vm',
                templateUrl: '/static/hotelbeds/hotel_search.html'
            }).when('/hoteles/reservar', {
                controller: 'HotelBookingController',
                controllerAs: 'vm',
                templateUrl: '/static/hotelbeds/hotel_booking.html'
            })

            .when('/vuelo_hotel', {
                controller: 'HotelFlightSearchController',
                controllerAs: 'vm',
                templateUrl: '/static/combined/hotel_flight_search.html'
            })
            .when('/transfer', {
                controller: 'TransferSearchController',
                controllerAs: 'vm',
                templateUrl: '/static/hotelbeds/transfer_search.html'
            })
            .when('/transfer/reservar', {
                controller: 'TransferBookingController',
                controllerAs: 'vm',
                templateUrl: '/static/hotelbeds/transfer_booking.html'
            })
            .when('/ticket', {
                controller: 'TicketSearchController',
                controllerAs: 'vm',
                templateUrl: '/static/hotelbeds/ticket_search.html'
            })
            .when('/', {
                controller: 'CommonIndexController',
                controllerAs: 'vm',
                templateUrl: '/static/index.html'
            })
            .otherwise('/');
    }
})();