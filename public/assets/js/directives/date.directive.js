(function () {
    'use strict';


    /* @ngInject */
    function pbDate() {
        // Usage:
        // <a data-cc-widget-close></a>
        // Creates:
        // <a data-cc-widget-close="" href="#" class="wclose">
        //     <i class="fa fa-remove"></i>
        // </a>
        var directive = {
            link: link,
            templateUrl: '/static/date.html',
            restrict: 'EA',
            replace: true
        };
        return directive;

        function link($scope, element, attrs) {
            var model = element.attr('ng-model');
            return $compile($('input', element).attr('ng-model', model))($scope);
        }
    }

    angular
        .module('preferentBooking.directives')
        .directive('pbDate', pbDate);


})();

