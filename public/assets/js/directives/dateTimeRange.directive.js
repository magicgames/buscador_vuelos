(function () {
    'use strict';


    /* @ngInject */
    function dateTimeRange() {
        // Usage:
        // <a data-cc-widget-close></a>
        // Creates:
        // <a data-cc-widget-close="" href="#" class="wclose">
        //     <i class="fa fa-remove"></i>
        // </a>
        var directive = {
            link: link,
            templateUrl:'/static/date-time-range.html',
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {

        }
    }

    angular
        .module('preferentBooking.directives')
        .directive('pbDateTimeRange', dateTimeRange);


})();

