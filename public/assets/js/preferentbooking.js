(function () {
    'use strict';

    angular.module('preferentBooking', [

        /*
         Globales
         */
        'rzModule',
        'mgcrea.ngStrap',
        'preferentBooking.directives',

        /*
         APP Propia
         */
        'preferentBooking.routes',
        'preferentBooking.config',
        'preferentBooking.authentication',
        'preferentBooking.servivuelo',
        'preferentBooking.hotelbeds',

        'preferentBooking.layout',
        'preferentBooking.combined'
    ]);
    angular
        .module('preferentBooking.routes', ['ngRoute']);
    angular
        .module('preferentBooking.config', []);
    angular
        .module('preferentBooking.directives', ['mgcrea.ngStrap']);


    angular
        .module('preferentBooking')
        .run(run);

    run.$inject = ['$http','$injector'];

    /**
     * @name run
     * @desc Update xsrf $http headers to align with Django's defaults
     */
    function run($http,$injector) {
        $http.defaults.xsrfHeaderName = 'X-CSRFToken';
        $http.defaults.xsrfCookieName = 'csrftoken';

    }


})();