Requisitos para instalar en ubuntu
===========

* postfix
* python-springpython
* python-dev python-setuptools
* libffi-dev libpq-dev
* libxml2-dev libxslt-dev python-dev zlib1g-dev
* libtiff5-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk
*[Como instalar virutalenv](http://hosseinkaz.blogspot.com.es/2012/06/how-to-install-virtualenv.html)


Para poder compilar Zato se tiene que hacer de la siguiente manera

pip install zato-client --allow-all-external --allow-unverified springpython


Configurar ufw
=============

ufw default deny incoming
ufw default allow outgoing
ufw allow 22222/tcp
ufw allow 80/tcp
ufw allow 443/tcp
ufw allow 25/tcp
ufw allow from 148.251.152.52 proto tcp to any port 6556
ufw allow from 84.127.237.218 proto tcp to any port 8183
ufw allow from 148.251.152.52 proto tcp to any port 8183