# -*- coding: utf-8 -*-
import string

from django.db.models import Count
from django.conf import settings
from elasticsearch import Elasticsearch
from elasticsearch.helpers import streaming_bulk

from provider.hotelbeds.models import Hotel, DestinationId, Destination, Zone, CountryId


def setup_elastic(client, index_name):
    client.indices.create(index=index_name,
                          body={"analysis": {
                              "filter": {
                                  "nGram_filter": {
                                      "type": "edgeNGram", "min_gram": 2, "max_gram": 20,
                                      "token_chars": ["letter", "digit", "punctuation", "symbol"]
                                  }
                              },
                              "analyzer": {
                                  "nGram_analyzer": {
                                      "type": "custom",
                                      "tokenizer": "whitespace",
                                      "filter": ["lowercase", "asciifolding", "nGram_filter"]
                                  },
                                  "whitespace_analyzer": {"type": "custom", "tokenizer": "whitespace",
                                                          "filter": ["lowercase", "asciifolding"]
                                                          }
                              }
                          }
                          },  # ignore already existing index
                          ignore=400
                          )
    pass


def create_hotel_index(client, index_name):
    client.indices.put_mapping(
        index=index_name,
        doc_type='hotel',
        body={
            'hotel': {
                '_all': {
                    "index_analyzer": "nGram_analyzer",
                    "search_analyzer": "whitespace_analyzer"
                },

                'properties': {

                    'hotel_name': {
                        'type': 'string',
                        'index': 'not_analyzed',
                        'boost': 3,
                    },
                    'zone_name': {
                        'type': 'string',
                        "index_analyzer": "nGram_analyzer",
                        "search_analyzer": "whitespace_analyzer",
                        "boost":2
                    },
                    'id': {
                        'type': 'integer',
                        'include_in_all': 'false'

                    },
                    'location': {'type': 'geo_point'},
                    'destination': {
                        'type': 'object',
                        'include_in_all': 'true',
                        'properties': {
                            'ale': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'bul': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'cas': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'cat': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'che': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'chi': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'dan': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'eng': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'fin': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'fra': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'gre': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'hol': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'hun': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'ind': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'ita': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'jpn': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'kor': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'mal': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'nor': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'pol': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'por': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'rus': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'sue': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'tai': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'tur': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                        }
                    },
                    'country': {
                        'type': 'object',
                        'include_in_all': 'true',
                        'properties': {
                            'ale': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'bul': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'cas': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'cat': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'che': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'chi': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'dan': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'eng': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'fin': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'fra': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'gre': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'hol': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'hun': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'ind': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'ita': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'jpn': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'kor': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'mal': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'nor': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'pol': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'por': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'rus': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'sue': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'tai': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'tur': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                        }

                    }
                }
            }}

    )


def create_destination_index(client, index_name):
    client.indices.put_mapping(
        index=index_name,
        doc_type='destination',
        body={
            'destination': {
                '_all': {
                    "index_analyzer": "nGram_analyzer",
                    "search_analyzer": "whitespace_analyzer"
                },

                'properties': {
                    'code': {
                        'type': 'string',
                        'include_in_all': 'false'
                    },
                    'num_hotels': {
                        'type': 'integer',
                        'include_in_all': 'false'
                    },
                    'destination': {
                        'type': 'object',
                        'include_in_all': 'true',
                        'properties': {
                            'ale': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'bul': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'cas': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'cat': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'che': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'chi': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'dan': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'eng': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'fin': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'fra': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'gre': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'hol': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'hun': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'ind': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'ita': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'jpn': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'kor': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'mal': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'nor': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'pol': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'por': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'rus': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'sue': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'tai': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'tur': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                        }
                    },
                    'country': {
                        'type': 'object',
                        'include_in_all': 'true',
                        'properties': {
                            'ale': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'bul': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'cas': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'cat': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'che': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'chi': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'dan': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'eng': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'fin': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'fra': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'gre': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'hol': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'hun': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'ind': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'ita': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'jpn': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'kor': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'mal': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'nor': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'pol': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'por': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'rus': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'sue': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'tai': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'tur': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                        }
                    }

                }
            }
        },

    )


def create_zone_index(client, index_name):
    client.indices.put_mapping(
        index=index_name,
        doc_type='zone',
        body={
            'zone': {
                '_all': {
                    "index_analyzer": "nGram_analyzer",
                    "search_analyzer": "whitespace_analyzer"
                },

                'properties': {

                    'name': {
                        'type': 'string',
                        "index_analyzer": "nGram_analyzer",
                        "search_analyzer": "whitespace_analyzer"
                    },
                    'id': {
                        'type': 'integer',
                        'include_in_all': 'false'

                    },
                    'num_hotels': {
                        'type': 'integer',
                        'include_in_all': 'false'
                    },
                    'destination_code': {
                        'type': 'string',
                        'include_in_all': 'false'
                    },
                    'code': {
                        'type': 'string',
                        'include_in_all': 'false'
                    },

                    'destination_name': {
                        'type': 'object',
                        'include_in_all': 'true',
                        'properties': {
                            'ale': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'bul': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'cas': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'cat': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'che': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'chi': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'dan': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'eng': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'fin': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'fra': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'gre': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'hol': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'hun': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'ind': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'ita': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'jpn': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'kor': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'mal': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'nor': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'pol': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'por': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'rus': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'sue': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'tai': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'tur': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                        }
                    },

                    'county': {
                        'type': 'object',
                        'include_in_all': 'true',
                        'properties': {
                            'ale': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'bul': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'cas': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'cat': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'che': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'chi': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'dan': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'eng': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'fin': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'fra': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'gre': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'hol': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'hun': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'ind': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'ita': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'jpn': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                            'kor': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'mal': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'nor': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'pol': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'por': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'rus': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'sue': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'tai': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },
                            'tur': {
                                'type': 'string',
                                "index_analyzer": "nGram_analyzer",
                                "search_analyzer": "whitespace_analyzer"
                            },

                        }
                    }

                }
            }
        },

    )


######  HOTEL ####

def bulk_create_hotel():
    hotels = Hotel.objects.all()
    for hotel in hotels:

        zone = Zone.objects.get(destination_code__code=hotel.destination_code, code=hotel.zone_code)
        destination_ids = DestinationId.objects.filter(destination_code=zone.destination_code)
        country_ids = CountryId.objects.filter(country_code=zone.destination_code.country_code)
        destination_dict = {}
        country_dict = {}
        for destination_id in destination_ids:
            lang_code = string.lower(destination_id.language.code)
            x = {lang_code: destination_id.name}
            destination_dict.update(x)

        for country_id in country_ids:
            lang_code = string.lower(country_id.language.code)
            x = {lang_code: country_id.name}
            country_dict.update(x)

        try:
            lat = float(hotel.latitude)
        except:
            lat = 0

        try:
            lon = float(hotel.longitude)
        except:
            lon = 0
        location = {
            'lat': lat,
            'lon': lon

        }
        yield {
            '_id': hotel.id,
            'hotel_name': hotel.name,
            'location': location,
            'zone_name': zone.name,
            'destination': destination_dict,
            'country': country_dict

        }


def load_hotel(client, index):
    for ok, result in streaming_bulk(
            client,
            bulk_create_hotel(),
            index=index,
            doc_type='hotel',
            chunk_size=1000  # keep the batch sizes small for appearances only
    ):
        action, result = result.popitem()
        doc_id = '/%s/hotel/%s' % (index, result['_id'])
        # process the information from ES whether the document has been
        # successfully indexed
        if not ok:
            print('Failed to %s document %s: %r' % (action, doc_id, result))
        else:
           # print(doc_id)
            pass


#### DESTINATION ###


def bulk_create_destination():
    hotel_count = Hotel.objects.values('destination_code').annotate(Count("id")).order_by()
    no_lang = []
    destinations = Destination.objects.all()
    hotel_a = {}
    destination_a = {}

    for hotel in hotel_count:
        x = {hotel['destination_code']: hotel['id__count']}
        hotel_a.update(x)

    for destination in destinations:
        destination_ids = DestinationId.objects.filter(destination_code=destination.code)
        country_ids = CountryId.objects.filter(country_code=destination.country_code)
        destination_dict = {}
        country_dict = {}
        for destination_id in destination_ids:

            lang_code = string.lower(destination_id.language.code)
            if lang_code not in no_lang:
                x = {lang_code: destination_id.name}
                destination_dict.update(x)

        for country_id in country_ids:
            lang_code = string.lower(country_id.language.code)
            x = {lang_code: country_id.name}
            country_dict.update(x)
        try:
            num = hotel_a[destination.code]
        except:
            num = 0

        x = {
            '_id': destination.code,
            'code': destination.code,
            'num_hotels': num,
            'destination': destination_dict,
            'country': country_dict
        }

        yield x


def load_destination(client, index):
    for ok, result in streaming_bulk(
            client,
            bulk_create_destination(),
            index=index,
            doc_type='destination',
            chunk_size=500  # keep the batch sizes small for appearances only
    ):
        action, result = result.popitem()
        doc_id = '/%s/destination/%s' % (index, result['_id'])
        # process the information from ES whether the document has been
        # successfully indexed
        if not ok:
            print('Failed to %s document %s: %r' % (action, doc_id, result))
        else:
            pass

            # instantiate es client, connects to localhost:9200 by default


### ZONE ####




def load_zone(client, index):
    for ok, result in streaming_bulk(
            client,
            bulk_create_zone(),
            index=index,
            doc_type='zone',
            chunk_size=500  # keep the batch sizes small for appearances only
    ):
        action, result = result.popitem()
        doc_id = '/%s/zone/%s' % (index, result['_id'])
        # process the information from ES whether the document has been
        # successfully indexed
        if not ok:
            print('Failed to %s document %s: %r' % (action, doc_id, result))
        else:
            pass

            # instantiate es client, connects to localhost:9200 by default


def bulk_create_zone():
    # select count(*),destination_code,zone_code from hotelbeds_hotel group by destination_code, zone_code
    hotel_count = Hotel.objects.values('destination_code', 'zone_code').annotate(Count("id")).order_by()
    zones = Zone.objects.all()
    hotel_a = {}
    for hotel in hotel_count:
        if hotel['destination_code'] in hotel_a:
            x = {hotel['zone_code']: hotel['id__count']}
            hotel_a[hotel['destination_code']].update(x)
        else:
            x = {hotel['destination_code']: {hotel['zone_code']: hotel['id__count']}}
            hotel_a.update(x)

    for zone in zones:
        name = zone.name
        destination = zone.destination_code
        destination_ids = DestinationId.objects.filter(destination_code=destination.code)
        country_ids = CountryId.objects.filter(country_code=destination.country_code)
        destination_dict = {}
        country_dict = {}
        for destination_id in destination_ids:
            lang_code = string.lower(destination_id.language.code)
            x = {lang_code: destination_id.name}
            destination_dict.update(x)

        for country_id in country_ids:
            lang_code = string.lower(country_id.language.code)
            x = {lang_code: country_id.name}
            country_dict.update(x)

        try:
            num = hotel_a[destination.code][zone.code]
        except:
            num = 0

        x = {
            '_id': destination.code + zone.code,
            'code': zone.code,
            'num_hotels': num,
            'name': name,
            'destination_name': destination_dict,
            'destination_code': destination.code,
            'country': country_dict
        }

        yield x


#### RUN

def run():
    es = Elasticsearch()
    index = settings.ELASTIC_SEARCH_IDEX
    setup_elastic(es, index)
    create_hotel_index(es, index)
    create_destination_index(es, index)
    create_zone_index(es, index)
    load_hotel(es, index)
    # load_destination(es, index)
    # load_zone(es, index)
