"""
WSGI config for buscador project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

if 'DJANGO_SETTINGS_MODULE' in os.environ:
    pass

else:
    os.environ["DJANGO_SETTINGS_MODULE"] = "buscador.settings.production"

application = get_wsgi_application()
