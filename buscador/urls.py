from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from common.views import *

urlpatterns=[]
if settings.DEBUG:
    from django.conf.urls.static import static


    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


urlpatterns = [
    # Examples:
    # url(r'^$', 'buscador.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),


    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),

    url(r'^api/v1/common/', include('api.urls', namespace='api', app_name='api')),
    url(r'^api/v1/vuelos/', include('provider.servivuelo.urls', namespace='servivuelo', app_name='servivuelo')),
    url(r'^api/v1/hoteles/', include('provider.hotelbeds.urls', namespace='hotelbeds', app_name='hotelbeds')),
    url(r'^api/v1/combined/', include('combined.urls', namespace='combined', app_name='combined')),
    url(r'^api/v1/auth/', include('authentication.urls', namespace='authentication', app_name='authentication')),
    url(r'^admin/', include(admin.site.urls)),
    url('^.*$', IndexView.as_view(), name='index'),

]

