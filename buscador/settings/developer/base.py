# coding=utf-8
from __future__ import absolute_import
from buscador.settings.base import *
from os.path import join, normpath
"""
Settings base para el sistema de desarrollo

"""

"""

Ejemplo de configuración de base de datos

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'preferentbooking_db',
        'USER': 'preferentbooking_usr',
        'PASSWORD': 'pepepotamo',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
"""


ALLOWED_HOSTS = ['127.0.0.1', 'api.preferentbooking.com']
LANGUAGE_CODE = 'es'

LOCAL_DEVEL_APPS = (
    'rosetta',
#    'debug_toolbar',
    'template_profiler_panel',
    'django_extensions',

)
"""
DEBUG_TOOLBAR_PANELS = [
    'ddt_request_history.panels.request_history.RequestHistoryPanel',
    'template_profiler_panel.panels.template.TemplateProfilerPanel',
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
    'debug_toolbar.panels.profiling.ProfilingPanel',
]
"""
INSTALLED_APPS = INSTALLED_APPS + LOCAL_DEVEL_APPS
"""
MIDDLEWARE_CLASSES = MIDDLEWARE_CLASSES + (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': 'ddt_request_history.panels.request_history.allow_ajax',
}
"""

"""
Ejemplo de configuración de Sentry. Cada desarrollador tendrá su DSN
para que así sepa que errores está haciendo durante el desarrollo

RAVEN_CONFIG = {
    'dsn': 'http://687334886fee40faa1e098d44b4e7c9d:1d1ee7cff3f04ea6aa8120976ad57e73@sentry.teamrockmotion.com/3',
}

"""