from __future__ import absolute_import
from .base import *
DEBUG = True
TEMPLATE_DEBUG = DEBUG
ALLOWED_HOSTS = ['api.preferentbooking.com','127.0.0.1']

with open('/etc/secret_key.txt') as f:
    SECRET_KEY = f.read().strip()

EMAIL_BACKEND = 'django_sendmail_backend.backends.EmailBackend'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'preferentbooking_search_db',
        'USER': 'preferentbooking_usr',
        'PASSWORD': 'piuadsghvp9s8egvp9dsauhgpv9ash',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

RAVEN_CONFIG = {
    'dsn': 'http://da80459f3ade4a37bdbdcdbf41ff0313:6978993f14b148ee9ff33f8e16052509@sentry.teamrockmotion.com/13',
}

INSTALLED_APPS +=('djsupervisor',)


CLIENT_ADDRESS = 'http://localhost:11223'

CLIENT_CREDENTIALS = ('preferentbooking', 'iadsuhgfoidugcoidsgoidasygoviudsaygpiadsg')
HOTELBEDS_TEST_MODE = True
SERVIVUELO_TEST_MODE = True