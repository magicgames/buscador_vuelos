# Local development dependencies go here
-r base.txt

Werkzeug
-e git+https://github.com/django-debug-toolbar/django-debug-toolbar#egg=django-debug-toolbar

pyOpenSSL
Sphinx
coverage
django-rosetta
bpython
-e git+https://github.com/djsutho/django-debug-toolbar-request-history.git#egg=django-debug-toolbar-request-history
django-debug-toolbar-template-profiler



# Extensiones para manage.py
django-extensions

